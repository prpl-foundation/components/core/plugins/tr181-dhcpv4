/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include "dhcp_method.h"
#include "dhcp_priv.h"

#include <amxd/amxd_transaction.h>
#include <amxc/amxc_macros.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <amxm/amxm.h>

#define ME "method"

amxd_status_t _setStaticClients(amxd_object_t* object,
                                UNUSED amxd_function_t* func,
                                amxc_var_t* args,
                                amxc_var_t* ret) {
    amxd_trans_t trans;
    amxc_var_t* mac_list = NULL;
    bool return_var = false;
    amxd_status_t status = amxd_status_unknown_error;

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    when_null_trace(object, exit, ERROR, "No pool object given");
    when_null_trace(args, exit, ERROR, "No arguments given");

    mac_list = GET_ARG(args, "MACs");

    when_null_trace(mac_list, exit, ERROR, "No MACs given");
    when_true_trace(amxc_var_type_of(mac_list) != AMXC_VAR_ID_LIST, exit, ERROR, "argument not in the right format (list)");

    amxd_trans_select_object(&trans, object);
    amxd_trans_select_pathf(&trans, ".StaticAddress.");

    amxc_var_for_each(mac, mac_list) {
        const char* mac_str = GET_CHAR(mac, NULL);
        when_str_empty_trace(mac_str, exit, ERROR, "given mac is empty");
        amxd_trans_add_inst(&trans, 0, NULL);
        amxd_trans_set_value(cstring_t, &trans, "Chaddr", mac_str);
        amxd_trans_set_value(bool, &trans, "Enable", true);
        amxd_trans_select_pathf(&trans, ".^");
    }

    when_failed_trace((status = amxd_trans_apply(&trans, dhcp_get_dm())), exit, ERROR, "Cannot add static ips");
    return_var = true;
exit:
    amxd_trans_clean(&trans);
    amxc_var_set(bool, ret, return_var);
    return status;
}

amxd_status_t _rmStaticClients(amxd_object_t* object,
                               UNUSED amxd_function_t* func,
                               amxc_var_t* args,
                               amxc_var_t* ret) {
    amxd_trans_t trans;
    amxc_var_t* mac_list = NULL;
    bool return_var = false;
    amxd_status_t status = amxd_status_unknown_error;

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    when_null_trace(object, exit, ERROR, "No pool object given");
    when_null_trace(args, exit, ERROR, "No arguments given");

    mac_list = GET_ARG(args, "MACs");

    when_null_trace(mac_list, exit, ERROR, "No MACs given");
    when_true_trace(amxc_var_type_of(mac_list) != AMXC_VAR_ID_LIST, exit, ERROR, "argument not in the right format (list)");

    amxd_trans_select_object(&trans, object);
    amxd_trans_select_pathf(&trans, ".StaticAddress.");

    amxc_var_for_each(mac, mac_list) {
        const char* mac_str = GET_CHAR(mac, NULL);
        amxd_object_t* static_ip = NULL;
        char* alias = NULL;
        when_str_empty_trace(mac_str, exit, ERROR, "given mac is empty");

        static_ip = amxd_object_findf(object, ".StaticAddress.[Chaddr=='%s']", mac_str);
        if(static_ip == NULL) {
            SAH_TRACEZ_INFO(ME, "MAC %s given is not associate with a static ip", mac_str);
            continue;
        }
        alias = amxd_object_get_value(cstring_t, static_ip, "Alias", NULL);
        amxd_trans_del_inst(&trans, 0, alias);
        free(alias);
    }
    when_failed_trace((status = amxd_trans_apply(&trans, dhcp_get_dm())), exit, ERROR, "Cannot remove static ips");
    return_var = true;
exit:
    amxd_trans_clean(&trans);
    amxc_var_set(bool, ret, return_var);
    return status;
}

amxd_status_t _cleanClientLeases(amxd_object_t* object,
                                 UNUSED amxd_function_t* func,
                                 amxc_var_t* args,
                                 amxc_var_t* ret) {
    amxd_trans_t trans;
    amxc_var_t* mac_list = NULL;
    bool return_var = false;
    amxd_status_t status = amxd_status_unknown_error;
    bool free_list = false;
    char* controller = NULL;
    amxd_object_t* server_obj = NULL;

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    when_null_trace(object, exit, ERROR, "No pool object given");
    when_null_trace(args, exit, ERROR, "No MACs given");

    mac_list = GET_ARG(args, "MACs");

    amxd_trans_select_object(&trans, object);
    amxd_trans_select_pathf(&trans, ".Client.");

    when_true_trace(mac_list != NULL && amxc_var_type_of(mac_list) != AMXC_VAR_ID_LIST, exit, ERROR, "argument not in the right format (NULL or list)");

    if(mac_list == NULL) {
        free_list = true;
        amxc_var_new(&mac_list);
        amxc_var_set_type(mac_list, AMXC_VAR_ID_LIST);
        amxd_object_for_each(instance, it, amxd_object_findf(object, ".Client.")) {
            amxd_object_t* client = amxc_container_of(it, amxd_object_t, it);
            char* mac = amxd_object_get_value(cstring_t, client, "Chaddr", NULL);
            amxc_var_t* tmp = amxc_var_add_new(mac_list);
            if(str_empty(mac)) {
                free(mac);
                SAH_TRACEZ_ERROR(ME, "Error in DM of the Client");
                goto exit;
            }
            amxc_var_push(cstring_t, tmp, mac);
            amxd_trans_del_inst(&trans, amxd_object_get_index(client), NULL);
        }
    } else {
        amxc_var_for_each(mac, mac_list) {
            amxd_object_t* client = NULL;
            const char* mac_str = GET_CHAR(mac, NULL);
            when_str_empty_trace(mac_str, exit, ERROR, "given mac is empty");
            client = amxd_object_findf(object, ".Client.[Chaddr=='%s']", mac_str);
            if(client == NULL) {
                SAH_TRACEZ_INFO(ME, "MAC %s given is not associate with a client", mac_str);
                continue;
            }
            amxd_trans_del_inst(&trans, amxd_object_get_index(client), NULL);
        }
    }

    when_failed_trace((status = amxd_trans_apply(&trans, dhcp_get_dm())), exit, ERROR, "Cannot add static ips");

    server_obj = amxd_object_get_parent(amxd_object_get_parent(object));
    when_null_trace(server_obj, exit, ERROR, "No server object found");

    controller = amxd_object_get_value(cstring_t, server_obj, "Controller", NULL);
    when_null_trace(controller, exit, ERROR, "Cannot get controller from pool.");

    status = amxm_execute_function(controller,
                                   controller,
                                   "clean-leases",
                                   mac_list,
                                   ret);
    when_failed_trace(status, exit, ERROR, "Cannot communicate deletion of leases");

    return_var = true;
exit:
    if(free_list) {
        amxc_var_delete(&mac_list);
    }
    free(controller);
    amxd_trans_clean(&trans);
    amxc_var_set(bool, ret, return_var);
    return status;
}