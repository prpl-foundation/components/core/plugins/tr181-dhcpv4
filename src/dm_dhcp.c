/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <string.h>
#include <stdlib.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "dhcp_priv.h"
#include "dm_dhcp.h"
#include "dhcp_info.h"
#include "firewall_utility.h"
#include "dhcp_event.h"

#include <amxm/amxm.h>
#include <amxb/amxb_operators.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_event.h>
#include <amxd/amxd_transaction.h>

#define ME "dml"

/**
 * @brief
 * Removes all inactive TR181 DHCPv4.Server.Pool.{i}.Client.{i}.IPv4Address instances.
 *
 * @return 0 on success
 */
int dm_dhcp_delete_expired_leases(void) {
    amxc_ts_t now;
    amxc_llist_t paths;
    amxd_object_t* pools = amxd_dm_findf(dhcp_get_dm(), "DHCPv4Server.Pool.");
    int r = -1;
    when_null_trace(pools, exit, ERROR, "DHCPv4.Server.Pool data model object not found");
    amxc_ts_now(&now);
    amxc_llist_init(&paths);
    r = amxd_object_resolve_pathf(pools, &paths, ".*.Client.*.IPv4Address.*");
    when_failed_trace(r, clean, ERROR, "Problem getting clients from data model");
    r = -1;
    amxc_llist_for_each(it, (&paths)) {
        amxd_object_t* obj = NULL;
        amxc_ts_t* expires = NULL;
        const char* path = amxc_string_get(amxc_string_from_llist_it(it), 0);
        when_null_trace(path, clean, ERROR, "Problem getting client path from data model");
        obj = amxd_dm_findf(dhcp_get_dm(), "%s", path);
        when_null_trace(obj, clean, ERROR, "Client data model object not found");
        expires = amxd_object_get_value(amxc_ts_t, obj, "LeaseTimeRemaining", NULL);
        if(expires == NULL) {
            SAH_TRACEZ_ERROR(ME, "LeaseTimeRemaining not found in TR-181 data model");
        } else {
            if(amxc_ts_compare(&now, expires) > 0) {
                SAH_TRACEZ_INFO(ME, "Deleting expired DHCP lease '%s'...", path);
                amxd_object_emit_del_inst(obj);
                amxd_object_delete(&obj);
            }
            free(expires);
        }
    }
    r = 0;
clean:
    amxc_llist_clean(&paths, amxc_string_list_it_free);
exit:
    return r;
}

/**
 * @brief
 * Sets the active parameter of a DHCP server pool client in the TR-181 data model.
 *
 * @param path DHCP server pool client TR-181 path
 * @param active True if the DHCP server pool client is active
 * @return 0 on success
 */
int dm_dhcp_set_pool_client_active(const char* path, bool active) {
    amxd_object_t* object = NULL;
    amxd_trans_t trans;
    int r = -1;

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    when_null_trace(path, exit, ERROR, "Invalid path argument");
    object = amxd_dm_findf(dhcp_get_dm(), "%s", path);
    when_null_trace(object, exit, ERROR, "Client data model object not found");

    amxd_trans_select_object(&trans, object);
    amxd_trans_set_value(bool, &trans, "Active", active);
    r = amxd_trans_apply(&trans, dhcp_get_dm());
    when_failed_trace(r, exit, ERROR, "Problem applying data model transaction");
exit:
    amxd_trans_clean(&trans);
    return r;
}

/**
 * @brief
 * Removes all inactive TR181 DHCPv4.Server.Pool.{i}.Client instances.
 *
 * @return 0 on success
 */
int dm_dhcp_delete_inactive_clients(void) {
    amxd_object_t* pools = NULL;
    amxc_llist_t paths;
    int r = -1;
    pools = amxd_dm_findf(dhcp_get_dm(), "DHCPv4Server.Pool.");
    when_null_trace(pools, exit, ERROR, "DHCPv4.Server.Pool data model object not found");
    amxc_llist_init(&paths);
    r = amxd_object_resolve_pathf(pools, &paths, ".*.Client.[Active==false]");
    when_failed_trace(r, clean, ERROR, "Problem getting inactive client paths from data model");
    r = -1;
    amxc_llist_for_each(it, (&paths)) {
        amxd_object_t* obj = NULL;
        const char* path = amxc_string_get(amxc_string_from_llist_it(it), 0);
        when_null_trace(path, clean, ERROR, "Problem getting client path from data model");
        obj = amxd_dm_findf(dhcp_get_dm(), "%s", path);
        when_null_trace(obj, exit, ERROR, "Client data model object not found");
        SAH_TRACEZ_INFO(ME, "Deleting inactive DHCP client %s", path);
        amxd_object_emit_del_inst(obj);
        amxd_object_delete(&obj);
    }
    r = 0;
clean:
    amxc_llist_clean(&paths, amxc_string_list_it_free);
exit:
    return r;
}

/**
 * @brief
 * Set the pool's Enable parameter to the value store in the private data.
 *
 * @param pools object with all the pools of the server.
 * @param pool the pool to set the Enable value.
 * @param priv boolean pointer to the value to be store in the pool's Enable parameter
 * @return 0 on success
 */
static int dm_dhcp_set_pool_enable(UNUSED amxd_object_t* pools,
                                   amxd_object_t* pool,
                                   void* priv) {
    amxd_trans_t trans;
    bool* server_enabled = NULL;
    int r = -1;

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    when_null_trace(pool, exit, ERROR, "Invalid pool argument");
    when_null_trace(priv, exit, ERROR, "Invalid enable argument");

    amxd_trans_select_object(&trans, pool);
    server_enabled = (bool*) priv;

    amxd_trans_set_value(bool, &trans, "Enable", *server_enabled);

    r = amxd_trans_apply(&trans, dhcp_get_dm());
    when_failed_trace(r, exit, ERROR, "Problem applying data model transaction");

exit:
    amxd_trans_clean(&trans);
    return r;
}

/**
 * @brief
 * Set all pools' Enable parameter of the server to the same boolean value.
 *
 * @param enable the boolean value to use for setting the Enable parameters.
 */
void dm_dhcp_set_all_pools_enable(bool enable) {
    amxd_object_t* pools = amxd_dm_findf(dhcp_get_dm(), "DHCPv4Server.Pool.");
    when_null_trace(pools, exit, ERROR, "DHCPv4.Server.Pool data model object not found");
    amxd_object_for_all(pools, ".*", dm_dhcp_set_pool_enable, &enable);
exit:
    return;
}

/**
 * @brief
 * Set all static address' Enable parameter of the pool to false.
 */
void dm_dhcp_pool_disable_static(amxd_object_t* pool) {
    amxd_object_t* static_ips = amxd_object_findf(pool, "StaticAddress.");
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    when_null_trace(static_ips, exit, ERROR, "StaticAddress data model object not found");
    amxd_object_for_each(instance, it, static_ips) {
        amxd_object_t* obj = amxc_llist_it_get_data(it, amxd_object_t, it);
        amxd_trans_select_object(&trans, obj);
        amxd_trans_set_value(bool, &trans, "Enable", false);
    }
    when_failed_trace(amxd_trans_apply(&trans, dhcp_get_dm()), exit, ERROR, "Cannot change Enable of static address");
exit:
    amxd_trans_clean(&trans);
    return;
}

/**
 * @brief
 * Fill the variant data for a static address to be pass to the controller.
 *
 * @param pool the pool object that the static ip address is associate to.
 * @param addr the static ip address object to be pass to the controller.
 * @param data the data variant to be fill in.
 */
static void create_controller_addr_data(amxd_object_t* pool, amxd_object_t* addr, amxc_var_t* data) {
    amxc_var_t* tmp_var = NULL;

    when_null_trace(pool, exit, ERROR, "No pool for static ip");
    when_null_trace(addr, exit, ERROR, "No static ip given");
    when_null_trace(data, exit, ERROR, "No data to fill given");

    amxc_var_set_type(data, AMXC_VAR_ID_HTABLE);

    tmp_var = amxc_var_add_key(amxc_htable_t, data, "pool", NULL);
    amxd_object_get_params(pool, tmp_var, amxd_dm_access_protected);
    tmp_var = amxc_var_add_key(amxc_htable_t, data, "addr", NULL);
    amxd_object_get_params(addr, tmp_var, amxd_dm_access_protected);
exit:
    return;
}

/**
 * @brief
 * Fill the variant data for a pool to be pass to the controller.
 *
 * @param info the private pool info.
 * @param data the data to be filled in.
 */
static void create_controller_pool_data(dhcp_pool_info_t* info, amxc_var_t* data) {
    amxc_var_t var_tag;
    amxc_var_t* tmp_var = NULL;
    char* allow = NULL;

    amxc_var_init(&var_tag);

    when_null_trace(info, exit, ERROR, "No private data for pool found.");
    when_null_trace(info->obj, exit, ERROR, "No pool object found.");
    when_null_trace(data, exit, ERROR, "No data to fill initialised.");

    amxc_var_set_type(data, AMXC_VAR_ID_HTABLE);

    allow = amxd_object_get_value(cstring_t, info->obj, "AllowedDevices", NULL);
    when_str_empty_trace(allow, exit, ERROR, "No allow devices criterion");

    amxd_object_get_params(info->obj, data, amxd_dm_access_protected);
    amxc_var_set(cstring_t, &var_tag, info->intf_alias);
    amxc_var_set_key(data, "IntfName", &var_tag, AMXC_VAR_FLAG_COPY);

    tmp_var = amxc_var_add_key(amxc_llist_t, data, "Option", NULL);

    amxd_object_for_each(instance, it, amxd_object_findf(info->obj, ".Option.")) {
        amxd_object_t* option = amxc_container_of(it, amxd_object_t, it);
        amxc_var_t* option_list = NULL;
        amxc_string_t force_param;
        amxo_parser_t* parser = dhcp_get_parser();
        bool force = 0;

        amxc_string_init(&force_param, 0);
        option_list = amxc_var_add(amxc_htable_t, tmp_var, NULL);

        amxd_object_get_params(option, option_list, amxd_dm_access_public);
        amxc_string_setf(&force_param, "%sForce", GET_CHAR(&parser->config, "prefix_"));
        force = amxd_object_get_value(bool, option, amxc_string_get(&force_param, 0), NULL);
        amxc_var_add_key(bool, option_list, "Force", force);

        amxc_string_clean(&force_param);
    }

    if(strcmp(allow, "All") != 0) {
        tmp_var = amxc_var_add_key(amxc_htable_t, data, "MACs", NULL);
        amxd_object_for_each(instance, it, amxd_object_findf(info->obj, ".Client.")) {
            amxd_object_t* client = amxc_container_of(it, amxd_object_t, it);
            char* mac = amxd_object_get_value(cstring_t, client, "Chaddr", NULL);

            if(!str_empty(mac)) {
                amxc_var_add_key(bool, tmp_var, mac, 1);
            }
            free(mac);
        }
        amxd_object_for_each(instance, it, amxd_object_findf(info->obj, ".StaticAddress.")) {
            amxd_object_t* addr = amxc_container_of(it, amxd_object_t, it);
            char* mac = amxd_object_get_value(cstring_t, addr, "Chaddr", NULL);

            if((!str_empty(mac)) && (!GET_BOOL(tmp_var, mac))) {
                amxc_var_add_key(bool, tmp_var, mac, 1);
            }
            free(mac);
        }
    }
exit:
    free(allow);
    amxc_var_clean(&var_tag);
    return;
}

/**
 * @brief
 * Sets the TR-181 DHCPv4.Server.Pool.{i}.Status parameter value and configure the server on the system.
 *
 * @param pool DHCPv4.Server.Pool instance object
 */
int dm_dhcp_update_pool_status(amxd_object_t* pool) {
    int status = 1;
    dhcp_pool_info_t* info = NULL;
    amxd_trans_t trans;
    amxc_var_t ret;
    amxc_var_t data;
    char* controller = NULL;
    amxd_object_t* server_obj = NULL;

    SAH_TRACEZ_INFO(ME, "Update Pool Status function called.");

    amxc_var_init(&ret);
    amxc_var_init(&data);
    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    when_null_trace(pool, exit, ERROR, "Invalid pool argument");
    when_null_trace(pool->priv, exit, ERROR, "Cannot get private data from pool object");
    info = (dhcp_pool_info_t*) pool->priv;

    if((info->flags.bit.fw_up) && !(info->flags.bit.enable)) {
        firewall_close_dhcp_port(info);
        info->flags.bit.fw_up = 0;
    }

    if(!(info->flags.bit.fw_up) && (info->flags.bit.enable)) {
        firewall_open_dhcp_port(info);
        info->flags.bit.fw_up = 1;
    }

    server_obj = amxd_object_get_parent(amxd_object_get_parent(info->obj));
    when_null_trace(server_obj, exit, ERROR, "No server object found");

    controller = amxd_object_get_value(cstring_t, server_obj, "Controller", NULL);
    when_null_trace(controller, exit, ERROR, "Cannot get controller from pool.");

    if(info->flags.bit.intf_alias) {
        create_controller_pool_data(info, &data);
        SAH_TRACE_INFO("Calling %s function.", info->flags.bit.enable ? "enable-dhcp-pool" : "disable-dhcp-pool");
        status = amxm_execute_function(controller,
                                       controller,
                                       info->flags.bit.enable ? "enable-dhcp-pool" : "disable-dhcp-pool",
                                       &data,
                                       &ret);
        info->flags.bit.config_ok = status == amxd_status_ok;
    }

    amxd_trans_select_object(&trans, pool);
    if(!info->flags.bit.enable) {
        amxd_trans_set_value(cstring_t, &trans, "Status", "Disabled");
    } else if(info->flags.bit.addr_ok &&
              info->flags.bit.intf_ok &&
              info->flags.bit.intf_alias &&
              info->flags.bit.enable &&
              info->flags.bit.config_ok &&
              info->flags.bit.fw_up) {
        amxd_trans_set_value(cstring_t, &trans, "Status", "Enabled");
    } else {
        amxd_trans_set_value(cstring_t, &trans, "Status", "Error_Misconfigured");
    }

    when_failed_trace(amxd_trans_apply(&trans, dhcp_get_dm()), exit, ERROR, "Cannot sync DHCPv4Server.Pool object.");
exit:
    free(controller);
    amxd_trans_clean(&trans);
    amxc_var_clean(&data);
    amxc_var_clean(&ret);
    return 0;
}

/**
 * @brief
 * Update a lease in the tr181 data model.
 *
 * @param client the client object to add the lease to.
 * @param lease the variant that contain all information about the lease and the client.
 * @param default_lease_time the default lease time to use if no lease time is given by the lease data.
 */
static void dm_dhcp_lease_update(amxd_object_t* client, amxc_var_t* lease, UNUSED int default_lease_time) {
    amxd_trans_t trans;
    amxc_ts_t expires;
    amxc_ts_t* tmp = NULL;
    amxc_var_t* time = NULL;
    amxc_ts_t now;
    amxd_object_t* lease_obj = NULL;
    const char* ipv4_addr = NULL;

    amxc_ts_now(&now);

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_set_attr(&trans, amxd_tattr_change_prot, true);

    // for now we only want 1 IPv4 address by client, other address will be keep in back-end cache but not showed
    // there should never be an other index than 1
    lease_obj = amxd_object_findf(client, ".IPv4Address.1.");
    if(lease_obj != NULL) {
        amxd_trans_select_object(&trans, lease_obj);
    } else {
        amxd_trans_select_object(&trans, client);
        amxd_trans_select_pathf(&trans, ".IPv4Address.");
        amxd_trans_add_inst(&trans, 1, NULL);
    }

    amxd_trans_set_value(amxc_ts_t, &trans, "LastUpdate", &now);

    time = GET_ARG(lease, "LeaseTimeRemaining");
    if((time == NULL) || ((tmp = amxc_var_dyncast(amxc_ts_t, time)) == NULL)) {
        expires = (amxc_ts_t) {.sec = now.sec + default_lease_time};
        amxd_trans_set_value(amxc_ts_t, &trans, "LeaseTimeRemaining", &expires);
    } else {
        amxd_trans_set_value(amxc_ts_t, &trans, "LeaseTimeRemaining", tmp);
        free(tmp);
    }

    ipv4_addr = GET_CHAR(lease, "IPv4Address");
    when_null_trace(ipv4_addr, exit, ERROR, "No ipv4 given from the lease, not updating the client");

    amxd_trans_set_value(cstring_t, &trans, "IPAddress", ipv4_addr);
    when_failed_trace(amxd_trans_apply(&trans, dhcp_get_dm()), exit, ERROR, "Cannot update lease");
exit:
    amxd_trans_clean(&trans);
    return;
}

/**
 * @brief
 * Create a client in the tr181 data model.
 *
 * @param pool the pool object to add the client to.
 * @param lease the variant that contain all information about the lease and the client.
 * @param client_chaddr the client MAC address.
 *
 * @return The newly created client on success or NULL if creation failed.
 */
static amxd_object_t* dm_dhcp_client_create(amxd_object_t* pool, amxc_var_t* lease, const char* client_chaddr) {
    amxd_trans_t trans;
    amxc_string_t client_alias;
    amxd_object_t* client = NULL;

    amxc_string_init(&client_alias, 0);
    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    when_null_trace(pool, exit, ERROR, "No pool given to add the client");
    when_null_trace(client_chaddr, exit, ERROR, "No chaddr for client given");

    amxc_string_setf(&client_alias, "client-%s", client_chaddr);

    amxd_trans_select_object(&trans, pool);
    amxd_trans_select_pathf(&trans, ".Client.");
    amxd_trans_add_inst(&trans, 0, NULL);

    amxd_trans_set_value(cstring_t, &trans, "Alias", amxc_string_get(&client_alias, 0));
    amxd_trans_set_value(cstring_t, &trans, "Chaddr", client_chaddr);

    amxd_trans_select_pathf(&trans, ".Option.");
    amxc_var_for_each(option, GET_ARG(lease, "Option")) {
        const char* tag = amxc_var_key(option);
        amxd_trans_add_inst(&trans, 0, NULL);
        amxd_trans_set_value(cstring_t, &trans, "Tag", tag);
        amxd_trans_set_value(cstring_t, &trans, "Value", GET_CHAR(option, NULL));
        amxd_trans_select_pathf(&trans, ".^");
    }

    when_failed_trace(amxd_trans_apply(&trans, dhcp_get_dm()), exit, ERROR, "Cannot add client");
    client = amxd_object_findf(pool, "Client.client-%s.", client_chaddr);
exit:
    amxd_trans_clean(&trans);
    amxc_string_clean(&client_alias);
    return client;
}

/**
 * @brief
 * Update a client in the tr181 data model.
 *
 * @param client Client whose lease got update.
 * @param lease the variant that contain all information about the lease and the client.
 *
 * @return true on success
 */
static bool dm_dhcp_client_update(amxd_object_t* client, amxc_var_t* lease) {
    bool rv = false;
    amxd_status_t res = amxd_status_unknown_error;
    amxd_object_t* dm_options = amxd_object_findf(client, "Option.");
    amxc_var_t* lease_options = GET_ARG(lease, "Option");
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    when_null_trace(dm_options, exit, ERROR, "Failed to find client options");
    when_null_trace(lease_options, exit, ERROR, "Failed to find lease options");

    res = amxd_trans_select_object(&trans, client);
    when_failed_trace(res, exit, ERROR, "Failed to select Client instance: %d", res);
    amxd_trans_set_value(bool, &trans, "Active", true);

    // Remove no longer relevant options from DM
    res = amxd_trans_select_object(&trans, dm_options);
    when_failed_trace(res, exit, ERROR, "Failed to select Client Option object: %d", res);
    amxd_object_for_each(instance, it, dm_options) {
        amxc_var_t* lease_option = NULL;
        amxd_object_t* dm_option = amxc_container_of(it, amxd_object_t, it);
        char* tag = amxd_object_get_value(cstring_t, dm_option, "Tag", NULL);
        when_null_trace(tag, del_loop_exit, ERROR, "client option had no tag");

        lease_option = GET_ARG(lease_options, tag);
        when_not_null(lease_option, del_loop_exit);

        when_failed_trace(amxd_trans_del_inst(&trans, amxd_object_get_index(dm_option), NULL),
                          del_loop_exit, ERROR, "Failed to delete client option %d", amxd_object_get_index(dm_option));
del_loop_exit:
        free(tag);
    }

    // Add or update lease options into DM
    amxc_var_for_each(lease_option, lease_options) {
        const char* tag = amxc_var_key(lease_option);
        amxd_object_t* dm_option = amxd_object_findf(dm_options, ".[Tag == %s].", tag);
        if(dm_option == NULL) {
            res = amxd_trans_select_object(&trans, dm_options);
            when_failed_trace(res, exit, ERROR, "Failed to select Client Option object: %d", res);

            res = amxd_trans_add_inst(&trans, 0, NULL);
            when_failed_trace(res, exit, ERROR, "Failed to add Client Option object: %d", res);

            amxd_trans_set_value(cstring_t, &trans, "Tag", tag);
        } else {
            res = amxd_trans_select_object(&trans, dm_option);
            when_failed_trace(res, exit, ERROR, "Failed to select Client Option object: %d", res);
        }
        amxd_trans_set_value(cstring_t, &trans, "Value", GET_CHAR(lease_option, NULL));
    }

    res = amxd_trans_apply(&trans, dhcp_get_dm());
    when_failed_trace(res, exit, ERROR, "Failed(%d) to update client", res);

    rv = true;
exit:
    amxd_trans_clean(&trans);
    return rv;
}

/**
 * @brief
 * Add or update a lease in the tr181 data model. If the client is not available in the data model, then it is created.
 *
 * @param function_name teh function name called.
 * @param args the variant data that contains client and lease information.
 * @param ret return variant.
 * @return 0 on success
 */
int dm_dhcp_set_lease(UNUSED const char* function_name,
                      amxc_var_t* args,
                      UNUSED amxc_var_t* ret) {
    amxd_object_t* pool = NULL;
    amxd_object_t* client = NULL;
    const char* pool_alias = NULL;
    const char* client_chaddr = NULL;
    int rv = 1;

    when_null_trace(args, exit, ERROR, "No lease to add");

    pool_alias = GET_CHAR(args, "Alias");
    when_null_trace(pool_alias, exit, ERROR, "No pool alias found");

    pool = amxd_dm_findf(dhcp_get_dm(), "DHCPv4Server.Pool.%s.", pool_alias);
    when_null_trace(pool, exit, ERROR, "Lease pool does not exist");

    client_chaddr = GET_CHAR(args, "Chaddr");
    when_null_trace(client_chaddr, exit, ERROR, "No chaddr client given");
    client = amxd_object_findf(pool, "Client.client-%s.", client_chaddr);
    if(client == NULL) {
        client = dm_dhcp_client_create(pool, args, client_chaddr);
        when_null_trace(client, exit, ERROR, "Cannot create client %s in pool", client_chaddr);
    } else {
        when_false_trace(dm_dhcp_client_update(client, args), exit, ERROR, "Cannot update client: %s", client_chaddr);
    }

    dm_dhcp_lease_update(client, args, amxd_object_get_value(uint32_t, pool, "LeaseTime", NULL));
    dhcp_server_send_lease_event(DHCP_LEASE_NEW, args);
    rv = 0;
exit:
    return rv;
}

/**
 * @brief
 * Add or update a lease in the tr181 data model. If the client is also not in the data model, then it is created.
 *
 * @param function_name the function name called.
 * @param args the variant data that contains client and lease information.
 * @param ret return variant.
 * @return 0 on success
 */
int dm_dhcp_remove_lease(UNUSED const char* function_name,
                         amxc_var_t* args,
                         UNUSED amxc_var_t* ret) {
    amxd_object_t* pool = NULL;
    amxd_object_t* client = NULL;
    amxd_object_t* lease = NULL;
    const char* pool_alias = NULL;
    const char* client_chaddr = NULL;
    const char* ipv4_addr = NULL;
    int rv = 1;

    when_null_trace(args, exit, ERROR, "No lease to remove");
    pool_alias = GET_CHAR(args, "Alias");
    when_null_trace(pool_alias, exit, ERROR, "No pool alias found");

    pool = amxd_dm_findf(dhcp_get_dm(), "DHCPv4Server.Pool.%s.", pool_alias);
    when_null_trace(pool, exit, ERROR, "Lease pool does not exist");

    client_chaddr = GET_CHAR(args, "Chaddr");
    when_null_trace(client_chaddr, exit, ERROR, "No chaddr client given");
    client = amxd_object_findf(pool, "Client.client-%s.", client_chaddr);
    when_null_trace(client, exit, ERROR, "Cannot get client in pool");

    ipv4_addr = GET_CHAR(args, "IPv4Address");
    when_null_trace(ipv4_addr, exit, ERROR, "No ipv4 given from the lease");
    lease = amxd_object_findf(client, "IPv4Address.[IPAddress=='%s']", ipv4_addr);
    when_null_trace(lease, exit, ERROR, "Cannot get lease in client : %s", ipv4_addr);

    dhcp_server_send_lease_event(DHCP_LEASE_RELEASE, args);
    amxd_object_emit_del_inst(lease);
    amxd_object_delete(&lease);
    rv = 0;
exit:
    return rv;
}


/**
 * @brief
 * Set and send the configuration to the controller to enable a static ip address.
 *
 * @param pool the pool object that the static ip address is link to.
 * @param addr the static ip address object to enable in the configuration.
 * @return 0 on success
 */
int dm_dhcp_static_address_enable(amxd_object_t* pool, amxd_object_t* addr) {
    int status = 0;
    char* controller = NULL;
    amxc_var_t data;
    amxc_var_t ret;
    amxd_object_t* server_obj = NULL;

    amxc_var_init(&ret);
    amxc_var_init(&data);

    when_null_trace(pool, exit, ERROR, "No pool given for the static ip");
    when_null_trace(addr, exit, ERROR, "No static ip given");

    server_obj = amxd_object_get_parent(amxd_object_get_parent(pool));
    when_null_trace(server_obj, exit, ERROR, "No server object found");

    controller = amxd_object_get_value(cstring_t, server_obj, "Controller", NULL);
    when_null_trace(controller, exit, ERROR, "Cannot get controller from pool.");

    create_controller_addr_data(pool, addr, &data);

    status = amxm_execute_function(controller,
                                   controller,
                                   "enable-static-ip",
                                   &data,
                                   &ret);
exit:
    amxc_var_clean(&data);
    amxc_var_clean(&ret);
    free(controller);
    return status;
}

/**
 * @brief
 * Call the controller to disable  a static ip address.
 *
 * @param event_data the data comming from a remove of the static ip
 * @param pool the pool object that the static ip address is linked to.
 * @param addr the static ip address object to disable in the configuration.
 * @return 0 on success
 */
int dm_dhcp_static_address_disable(const amxc_var_t* const event_data, amxd_object_t* pool, amxd_object_t* addr) {
    int status = 1;
    char* controller = NULL;
    amxc_var_t data;
    amxc_var_t ret;
    amxd_object_t* server_obj = NULL;

    amxc_var_init(&ret);
    amxc_var_init(&data);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);

    when_null_trace(pool, exit, ERROR, "No pool given for the static ip");
    when_null_trace(addr, exit, ERROR, "No alias of the static ip given");

    server_obj = amxd_object_get_parent(amxd_object_get_parent(pool));
    when_null_trace(server_obj, exit, ERROR, "No server object found");

    controller = amxd_object_get_value(cstring_t, server_obj, "Controller", NULL);
    when_null_trace(controller, exit, ERROR, "Cannot get controller from pool.");

    create_controller_addr_data(pool, addr, &data);

    if(event_data != NULL) {
        amxc_var_set(cstring_t, GETP_ARG(&data, "addr.Alias"), GETP_CHAR(event_data, "parameters.Alias"));
        amxc_var_set(cstring_t, GETP_ARG(&data, "addr.Chaddr"), GETP_CHAR(event_data, "parameters.Chaddr"));
    }

    status = amxm_execute_function(controller,
                                   controller,
                                   "disable-static-ip",
                                   &data,
                                   &ret);
exit:
    amxc_var_clean(&ret);
    amxc_var_clean(&data);
    free(controller);
    return status;
}

/**
 * @brief
 * Assign a available ip address to the static ip address
 *
 * @param pool The pool the static address is assigned to.
 * @param addr The static address to assign the ip address to.
 * @return int 0 on success
 */
int dm_dhcp_static_address_assign(amxd_object_t* pool, amxd_object_t* addr) {
    int rc = 1;
    amxd_trans_t trans;
    struct in_addr ip_addr;
    struct in_addr min_addr;
    char* ip_addr_str = NULL;
    char* min_addr_str = NULL;
    char str[INET_ADDRSTRLEN + 1];

    amxd_trans_init(&trans);

    when_null_trace(pool, exit, ERROR, "No pool object given")
    when_null_trace(addr, exit, ERROR, "No static address object given");

    ip_addr_str = amxd_object_get_value(cstring_t, pool, "MaxAddress", NULL);
    when_str_empty_trace(ip_addr_str, exit, ERROR, "No address available for auto assign");

    min_addr_str = amxd_object_get_value(cstring_t, pool, "MinAddress", NULL);
    when_str_empty_trace(min_addr_str, exit, ERROR, "No minimum address given");
    rc = inet_pton(AF_INET, min_addr_str, &min_addr);
    when_false_trace(rc == 1, exit, ERROR, "Bad Address parameter '%s'", min_addr_str);
    strncpy(str, ip_addr_str, INET_ADDRSTRLEN);
    do {
        rc = inet_pton(AF_INET, str, &ip_addr);
        when_false_trace(rc == 1, exit, ERROR, "Bad Address parameter '%s'", str);
        amxd_trans_select_object(&trans, addr);
        amxd_trans_set_value(cstring_t, &trans, "Yiaddr", str);
        rc = amxd_trans_apply(&trans, dhcp_get_dm());
        when_true(rc == amxd_status_ok, exit);
        SAH_TRACEZ_INFO(ME, "Unable to apply address %s, trying next one", str);
        amxd_trans_clean(&trans);
        amxd_trans_init(&trans);

        // need to decrease by one the ip address for iteration, so a conversion to hex format of the ip is needed before applying the math
        ip_addr.s_addr = ntohl(htonl(ip_addr.s_addr) - 1);
        inet_ntop(AF_INET, &ip_addr, str, INET_ADDRSTRLEN);
    } while(htonl(ip_addr.s_addr) > htonl(min_addr.s_addr));
    when_failed_trace(rc, exit, ERROR, "No address found for static ip letting the address to be manually set");
exit:
    free(ip_addr_str);
    free(min_addr_str);
    amxd_trans_clean(&trans);
    return rc;
}
