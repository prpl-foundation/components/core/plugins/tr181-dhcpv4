/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <arpa/inet.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "dhcp_action.h"
#include "dm_dhcp.h"
#include "dhcp_info.h"
#include "dhcp_priv.h"
#include "firewall_utility.h"

#define ME "action"

/**
 * @brief
 * Remove a pool from the DHCPv4.Server object.
 *
 * @param object The DHCPv4.Server object.
 * @param param NULL
 * @param reason The action identifier.
 * @param args The data of the pool to remove.
 * @param retval Variant to store the result of the action.
 * @param priv NULL
 * @return amxd_status_ok if the pool was successfully removed.
 */
amxd_status_t _dhcp_server_pool_removed(amxd_object_t* object,
                                        amxd_param_t* param,
                                        amxd_action_t reason,
                                        const amxc_var_t* const args,
                                        amxc_var_t* const retval,
                                        void* priv) {
    amxd_status_t status = amxd_status_unknown_error;
    amxd_object_t* pool = NULL;
    int index = 0;
    const char* name = NULL;
    dhcp_pool_info_t* info = NULL;

    SAH_TRACEZ_INFO(ME, "DHCPv4 server pool removed from TR-181 data model");
    when_null_trace(args, exit, ERROR, "Cannot get the data of the pool to remove.");
    when_null_trace(object, exit, ERROR, "Cannot get DHCPv4.Server object.");

    index = GET_INT32(args, "index");
    if(index == 0) {
        name = GET_CHAR(args, "name");
        when_null_trace(name, exit, ERROR, "Cannot get pool name.");
    }
    pool = amxd_object_get_instance(object, name, index);
    when_null_trace(pool, exit, ERROR, "Cannot get DHCPv4 pool object.");
    info = (dhcp_pool_info_t*) pool->priv;
    when_null_trace(info, exit, ERROR, "Cannot get DHCPv4 pool private information.");
    SAH_TRACEZ_INFO(ME, "Removing DHCPv4 server pool '%s' from UCI...", info->intf_alias);
    dhcp_pool_info_disable(pool);
    dm_dhcp_pool_disable_static(pool);
    dhcp_pool_info_clear(info);
    status = amxd_action_object_destroy(object, param, reason, args, retval, priv);
exit:
    return status;
}

/**
 * @brief
 * check if an ip is already assigned in a the DM of the pool
 *
 * @param pool the pool object to check
 * @param ip the ip that need to be in the DM of the object
 * @param mac the MAC address that need to be in the DM of the object
 * @return true if the IP is in the DM, false elsewhere
 */
static bool check_ip_assigned_in_pool(amxd_object_t* pool, const char* ip, const char* mac) {
    const char* router_ips = NULL;
    const char* dnsserver_ips = NULL;
    amxd_object_t* find_return = NULL;
    bool ret = true;
    amxc_llist_t paths;
    amxd_status_t r = amxd_status_unknown_error;
    amxc_string_t mac_up, mac_low;

    amxc_llist_init(&paths);
    amxc_string_init(&mac_up, 0);
    amxc_string_init(&mac_low, 0);

    when_null_trace(pool, exit, ERROR, "No pool given to check ip in");
    when_str_empty_trace(ip, exit, ERROR, "No ip given to check in pool");
    when_str_empty_trace(mac, exit, ERROR, "No mac address given to check the lease");

    find_return = amxd_object_findf(pool, "StaticAddress.[Yiaddr == '%s'].", ip);
    when_not_null_trace(find_return, exit, WARNING, "Address already in use by other static in pool %s", amxd_object_get_name(pool, AMXD_OBJECT_NAMED));
    router_ips = GET_CHAR(amxd_object_get_param_value(pool, "IPRouters"), NULL);
    when_not_null_trace(strstr(router_ips, ip), exit, WARNING, "Address already in use by a router in pool %s", amxd_object_get_name(pool, AMXD_OBJECT_NAMED));
    dnsserver_ips = GET_CHAR(amxd_object_get_param_value(pool, "DNSServers"), NULL);
    when_not_null_trace(strstr(dnsserver_ips, ip), exit, WARNING, "Address already in use by a dns server in pool %s", amxd_object_get_name(pool, AMXD_OBJECT_NAMED));

    // check that the lease is not taken by a client
    amxc_string_set(&mac_up, mac);
    amxc_string_set(&mac_low, mac);
    amxc_string_to_upper(&mac_up);
    amxc_string_to_lower(&mac_low);
    r = amxd_object_resolve_pathf(pool, &paths, "Client.[Chaddr != '%s' && Chaddr != '%s'].IPv4Address.[IPAddress == '%s']", amxc_string_get(&mac_up, 0), amxc_string_get(&mac_low, 0), ip);
    when_failed_trace(r, exit, ERROR, "Cannot resolve client paths from data model.");
    when_false_trace(amxc_llist_is_empty(&paths), exit, WARNING, "Address already in use by other client in pool %s", amxd_object_get_name(pool, AMXD_OBJECT_NAMED));

    ret = false;
exit:
    amxc_string_clean(&mac_up);
    amxc_string_clean(&mac_low);
    amxc_llist_clean(&paths, amxc_string_list_it_free);
    return ret;
}

/**
 * @brief
 * Check that the given ipv4 is not reserved for special use
 *
 * @param object The DHCPv4.Server.[i].StaticAddress.[i]
 * @param param Not used.
 * @param reason The action identifier.
 * @param args The address to validate
 * @param retval Variant to store the result of the action.
 * @param priv NULL
 * @return amxd_status_ok if the pool was successfully removed.
 */
amxd_status_t _is_not_reserved_addr(amxd_object_t* object,
                                    UNUSED amxd_param_t* param,
                                    UNUSED amxd_action_t reason,
                                    const amxc_var_t* const args,
                                    UNUSED amxc_var_t* const retval,
                                    UNUSED void* priv) {
    amxd_status_t status = amxd_status_unknown_error;
    amxd_object_t* pool = NULL;
    const char* ip = NULL;
    const char* mac = NULL;
    const char* minaddr_str = NULL;
    const char* maxaddr_str = NULL;
    struct in_addr minaddr;
    struct in_addr maxaddr;
    struct in_addr yiaddr;
    dhcp_pool_info_t* info = NULL;
    amxc_var_t* dev_ips = NULL;

    when_true_status(reason != action_param_validate, exit, status = amxd_status_function_not_implemented);

    when_true_status(dhcp_order_in_trans(), exit, status = amxd_status_ok);
    when_null_trace(args, exit, ERROR, "Cannot get the data of the address to validate.");
    when_null_trace(object, exit, ERROR, "No reference to the data model object given.");

    ip = GET_CHAR(args, NULL);
    when_null_trace(ip, exit, ERROR, "Cannot get the address to validate.");
    when_str_empty_status(ip, exit, status = amxd_status_ok);
    pool = amxd_object_get_parent(amxd_object_get_parent(object));
    when_null_trace(pool, exit, ERROR, "No pool object found for the static address");

    minaddr_str = GET_CHAR(amxd_object_get_param_value(pool, "MinAddress"), NULL);
    when_null_trace(minaddr_str, exit, ERROR, "Cannot get the min address of the pool.");
    maxaddr_str = GET_CHAR(amxd_object_get_param_value(pool, "MaxAddress"), NULL);
    when_null_trace(maxaddr_str, exit, ERROR, "Cannot get the max address of the pool.");

    status = amxd_status_invalid_value;

    when_false_trace(inet_pton(AF_INET, ip, &yiaddr) == 1, exit, ERROR, "Bad Yiaddr given.");

    mac = GET_CHAR(amxd_object_get_param_value(object, "Chaddr"), NULL);
    when_str_empty_trace(mac, exit, ERROR, "Cannot get MAC of the StaticAddress.");

    // Check that the address is not taken by an other Pool
    amxd_object_for_each(instance, it, amxd_object_get_parent(pool)) {
        amxd_object_t* tmp_pool = amxc_llist_it_get_data(it, amxd_object_t, it);
        when_true_trace(check_ip_assigned_in_pool(tmp_pool, ip, mac), exit, ERROR, "IP already assigned");
    }

    info = (dhcp_pool_info_t*) pool->priv;
    when_null_trace(info, exit, ERROR, "No private data found for the pool");

    dev_ips = netmodel_getAddrs(info->intf_path, "ipv4", netmodel_traverse_down);

    // Check that the address is not taken
    amxc_var_for_each(dev_ip, dev_ips) {
        struct in_addr device_ip;
        when_false_trace(inet_pton(AF_INET, GET_CHAR(dev_ip, "Address"), &device_ip) == 1, exit, ERROR, "Bad IP on interface given.");
        if(device_ip.s_addr == yiaddr.s_addr) {
            goto exit;
        }
    }


    // Check reserved address in range
    when_false_trace(inet_pton(AF_INET, maxaddr_str, &maxaddr) == 1, exit, ERROR, "Bad max address found.");
    when_false_trace(inet_pton(AF_INET, minaddr_str, &minaddr) == 1, exit, ERROR, "Bad min address found.");
    when_false_trace((yiaddr.s_addr >= minaddr.s_addr) && (yiaddr.s_addr <= maxaddr.s_addr), exit, ERROR, "Static address not in pool range");
    status = amxd_status_ok;
exit:
    amxc_var_delete(&dev_ips);
    return status;
}
