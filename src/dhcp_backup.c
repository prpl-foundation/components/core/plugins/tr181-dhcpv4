/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "dhcp_priv.h"
#include "dm_dhcp.h"
#include "dhcp_backup.h"
#include <amxm/amxm.h>
#include <amxd/amxd_object.h>

#define ME "dhcp_backup"

#define STR_EMPTY(x) (x) == NULL || (x)[0] == '\0'
static const char* upc_dataset_names[pcm_dataset_max] = { "set", "add", "delete" };

static amxc_var_t* pcm_upc_get_dataset(const amxc_var_t* data, pcm_dataset_t dataset) {
    return amxc_var_get_pathf(data, AMXC_VAR_FLAG_DEFAULT,
                              "data.%s.hgwconfig.DHCPv4Server.", upc_dataset_names[dataset]);
}

static void append_new_var_to(UNUSED amxc_var_t* list_client, amxc_var_t* data) {
    amxc_var_for_each(var, data) {
        amxc_var_add(amxc_htable_t, list_client, amxc_var_constcast(amxc_htable_t, var));
    }
}


static amxd_status_t add_instance_to_object(const char* inst, amxd_object_t* object) {
    amxd_status_t rc = amxd_status_unknown_error;
    amxd_trans_t trans;

    amxd_trans_init(&trans);

    when_str_empty_trace(inst, exit, ERROR, "Instance str is empty");
    when_null_trace(object, exit, ERROR, "Object is NULL");

    rc = amxd_status_ok;
    if(amxd_object_get_instance(object, inst, 0) == NULL) {
        amxd_trans_select_object(&trans, object);
        amxd_trans_add_inst(&trans, 0, inst);
        rc = amxd_trans_apply(&trans, dhcp_get_dm());
    }

exit:
    amxd_trans_clean(&trans);
    return rc;
}

static amxd_status_t dhcpv4_for_each_element(amxd_object_t* object, amxc_var_t* data, amxc_var_t* clients) {
    amxd_status_t rc = amxd_status_unknown_error;
    const amxc_htable_t* htable = NULL;
    const amxc_llist_t* list = NULL;
    amxd_object_t* child = NULL;
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    when_null_trace(data, exit, ERROR, "Restored data is empty");
    when_null_trace(object, exit, ERROR, "Restored object is NULL");

    if(amxc_var_type_of(data) == AMXC_VAR_ID_HTABLE) {
        htable = amxc_var_constcast(amxc_htable_t, data);
        amxc_htable_for_each(it, htable) {
            const char* key = NULL;
            amxc_var_t* value = NULL;

            value = amxc_var_from_htable_it(it);
            key = amxc_htable_it_get_key(it);

            when_str_empty(key, exit);
            when_null(value, exit);

            if(strcmp(key, "Client") == 0) {
                append_new_var_to(clients, value);
                continue;
            }

            child = amxd_object_get_child(object, key);

            if((amxc_var_type_of(value) == AMXC_VAR_ID_HTABLE) ||
               (amxc_var_type_of(value) == AMXC_VAR_ID_LIST)) {
                rc = dhcpv4_for_each_element(child, value, clients);

            } else {
                amxd_trans_select_object(&trans, object);
                rc = amxd_trans_set_param(&trans, key, value);
            }

        }
        rc = amxd_trans_apply(&trans, dhcp_get_dm());
        when_failed_trace(rc, exit, ERROR, "Unable to apply the transaction");

    } else if(amxc_var_type_of(data) == AMXC_VAR_ID_LIST) {
        list = amxc_var_constcast(amxc_llist_t, data);
        amxc_llist_for_each(it, list) {
            const char* key = NULL;
            amxc_var_t* value = NULL;

            value = amxc_var_from_llist_it(it);
            when_true_trace(amxc_var_type_of(value) != AMXC_VAR_ID_HTABLE,
                            exit, ERROR, "Wrong data format");

            htable = amxc_var_constcast(amxc_htable_t, value);
            key = amxc_htable_it_get_key(amxc_htable_get_first(htable));
            value = amxc_var_from_htable_it(amxc_htable_get_first(htable));

            when_str_empty(key, exit);
            when_null(value, exit);

            //Adds the instance if it does not exist
            rc = add_instance_to_object(key, object);
            child = amxd_object_get_instance(object, key, 0);
            when_failed_trace(rc, exit, ERROR, "Unable to add the instance %s", key);

            rc = dhcpv4_for_each_element(child, value, clients);
        }
    }

exit:
    amxd_trans_clean(&trans);
    return rc;
}


/**
 *  This is a custom Import method which overwrites the PcmImport from mod_pcm_svc.
 *  It can restore the upgrade persistent values of the DHCPv4Server as well as
 *  the leases by calling the back-end (dnsmasq).
 */
amxd_status_t _Import(amxd_object_t* object,
                      UNUSED amxd_function_t* func,
                      amxc_var_t* args,
                      amxc_var_t* ret) {
    amxd_status_t rc = amxd_status_invalid_arg;
    amxc_var_t clients;
    amxc_var_t data;
    char* controller = NULL;
    amxc_var_t* dataset = NULL;

    amxc_var_init(&data);
    amxc_var_init(&clients);

    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_var_set_type(&clients, AMXC_VAR_ID_LIST);

    when_null_trace(args, exit, ERROR, "Invalid arguments");

    dataset = pcm_upc_get_dataset(args, pcm_dataset_set);
    rc = amxd_status_ok;
    when_null_trace(dataset, exit, WARNING, "Nothing to restore");
    rc = dhcpv4_for_each_element(object, dataset, &clients);
    when_failed_trace(rc, exit, ERROR, "Unable to restore the DHCPv4Server DM");
    SAH_TRACEZ_INFO(ME, "DHCPv4Server DM restored successfully");

    amxc_var_add_key(amxc_llist_t, &data, "Clients", amxc_var_constcast(amxc_llist_t, &clients));
    controller = amxd_object_get_value(cstring_t, object, "Controller", NULL);
    rc = amxm_execute_function(controller, controller, "update-dhcp-leases", &data, ret);
    when_failed_trace(rc, exit, ERROR, "Unable to restore the DHCPv4Server leases");
    SAH_TRACEZ_INFO(ME, "DHCPv4Server leases restored successfully");

exit:
    free(controller);
    amxc_var_clean(&data);
    amxc_var_clean(&clients);
    return rc;
}