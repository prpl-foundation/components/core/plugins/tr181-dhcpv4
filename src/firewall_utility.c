/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <amxc/amxc.h>
#include <amxm/amxm.h>

#include "firewall_utility.h"

#define ME "fwl"

typedef struct {
    amxc_htable_it_t it;
    char* intf_fw_alias;
    uint32_t counter;
} fwl_intf_table_entry_t;

static amxc_htable_t* fwl_intf_table = NULL;

/**
 * @brief
 * Cleanup a firewall iterface table item
 *
 * @param hit The item key
 * @param hit A pointer to the hash table iterator structure
 */
static void entry_it_clean(UNUSED const char* key, amxc_htable_it_t* hit) {
    fwl_intf_table_entry_t* entry = amxc_container_of(hit, fwl_intf_table_entry_t, it);
    when_null(entry, exit);
    amxc_htable_it_clean(&entry->it, NULL);
    free(entry->intf_fw_alias);
    free(entry);
exit:
    return;
}


/**
 * @brief
 * Adds a firewall rule to open the DHCPv4 server port on an interface.
 * Calls fw.set_service function.
 *
 * @param info The private data from the interface to open the port on.
 */
void firewall_open_dhcp_port(dhcp_pool_info_t* info) {
    amxc_var_t args;
    amxc_var_t ret;
    amxc_var_t* tmp = NULL;
    amxc_string_t alias_str;
    amxd_status_t status = amxd_status_unknown_error;
    amxd_object_t* server_object = NULL;
    amxc_htable_it_t* record_it = NULL;
    fwl_intf_table_entry_t* pools = NULL;
    char* controller = NULL;


    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_string_init(&alias_str, 0);

    if(fwl_intf_table == NULL) {
        amxc_htable_new(&fwl_intf_table, 0);
    }

    when_null_trace(info, exit, ERROR, "No private info given for the interface.");
    when_null_trace(info->obj, exit, ERROR, "No object for the pool.");
    when_str_empty_trace(info->intf_path, exit, ERROR, "Unable to get interface path");

    record_it = amxc_htable_get(fwl_intf_table, info->intf_path);
    if(record_it != NULL) {
        pools = amxc_htable_it_get_data(record_it, fwl_intf_table_entry_t, it);
    }

    if(pools == NULL) {
        SAH_TRACEZ_INFO(ME, "Opening firewall port '%d' for interface '%s'...", DHCP_PORT, info->intf_path);

        amxc_string_setf(&alias_str, FW_ALIAS_PREFIX "%s", info->intf_path);
        amxc_string_replace(&alias_str, "Device.IP.Interface.", "intf-", 1);
        amxc_string_remove_at(&alias_str, amxc_string_text_length(&alias_str) - 1, 1);

        amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
        tmp = amxc_var_add_new_key(&args, "id");
        amxc_var_push(cstring_t, tmp, amxc_string_take_buffer(&alias_str));
        amxc_var_add_key(uint32_t, &args, "destination_port", DHCP_PORT);
        amxc_var_add_key(bool, &args, "enable", true);
        amxc_var_add_key(cstring_t, &args, "interface", info->intf_path);
        amxc_var_add_key(uint32_t, &args, "ipversion", 4);
        amxc_var_add_key(cstring_t, &args, "protocol", "17");

        server_object = amxd_object_get_parent(amxd_object_get_parent(info->obj));
        controller = amxd_object_get_value(cstring_t, server_object, "FWController", NULL);
        when_null_trace(controller, exit, ERROR, "Cannot get controller from pool.");
        status = amxm_execute_function(controller,
                                       FIREWALL_MODULE,
                                       "set_service",
                                       &args,
                                       &ret);
        when_failed_trace(status, exit, WARNING, "Unable to add firewall rule returned %d.", status);

        pools = calloc(1, sizeof(fwl_intf_table_entry_t));
        when_null_trace(pools, exit, ERROR, "Cannot allocate memory for pools item.");

        pools->intf_fw_alias = amxc_var_take(cstring_t, tmp);
        pools->counter = 1;
        amxc_htable_insert(fwl_intf_table, info->intf_path, &pools->it);
    } else {
        pools->counter++;
        SAH_TRACEZ_INFO(ME, "Firewall service with id '%s' found (used by %d pools)", pools->intf_fw_alias, pools->counter);
    }

    info->firewall_id = pools->intf_fw_alias;

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_string_clean(&alias_str);
    free(controller);
    return;
}

/**
 * @brief
 * Deletes the firewall rule to open the DNSSD port on an interface.
 * Calls Firewall.deleteService function.
 *
 * @param info The private data from the interface to open the port on.
 */
void firewall_close_dhcp_port(dhcp_pool_info_t* info) {
    amxc_var_t args;
    amxc_var_t ret;
    amxd_status_t status = amxd_status_unknown_error;
    char* controller = NULL;
    amxd_object_t* server_object = NULL;
    amxc_htable_it_t* record_it = NULL;
    fwl_intf_table_entry_t* pools = NULL;

    amxc_var_init(&args);
    amxc_var_init(&ret);

    when_null_trace(info, exit, ERROR, "No private info given for the interface.");
    when_str_empty_trace(info->firewall_id, exit, WARNING, "Unable to get previous firewall id");

    record_it = amxc_htable_get(fwl_intf_table, info->intf_path);
    when_null_trace(record_it, exit, ERROR, "Cannot get iterator for this interface in firewall table.");

    pools = amxc_htable_it_get_data(record_it, fwl_intf_table_entry_t, it);
    when_null_trace(pools, exit, ERROR, "Cannot get interface entry in firewall table.");

    if(pools->counter <= 1) {
        SAH_TRACEZ_INFO(ME, "Closing firewall service with id %s", info->firewall_id);

        amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
        amxc_var_add_key(cstring_t, &args, "id", info->firewall_id);

        server_object = amxd_object_get_parent(amxd_object_get_parent(info->obj));
        controller = amxd_object_get_value(cstring_t, server_object, "FWController", NULL);
        when_null_trace(controller, exit, ERROR, "Cannot get controller from pool.");
        status = amxm_execute_function(controller,
                                       FIREWALL_MODULE,
                                       "delete_service",
                                       &args,
                                       &ret);
        when_failed_trace(status, exit, WARNING, "Unable to delete firewall rule.");
        entry_it_clean(info->intf_path, &pools->it);
    } else {
        pools->counter--;
        SAH_TRACEZ_INFO(ME, "Firewall service with id '%s' staying open because of %d pools", info->firewall_id, pools->counter);
    }

    info->firewall_id = NULL;
exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    free(controller);
    return;
}

/**
 * @brief
 * Cleanup the firewall utility object.
 */
void firewall_cleanup(void) {
    amxc_htable_delete(&fwl_intf_table, entry_it_clean);
}
