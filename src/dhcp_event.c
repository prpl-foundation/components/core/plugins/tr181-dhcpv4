/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "dhcp_priv.h"
#include "dhcp_event.h"
#include "dm_dhcp.h"
#include "dhcp_info.h"
#include "firewall_utility.h"

#define ME "event"

void _print_event(UNUSED const char* const event_name,
                  const amxc_var_t* const event_data,
                  UNUSED void* const priv) {
    if(sahTraceZoneLevel(sahTraceGetZone(ME)) >= TRACE_LEVEL_INFO) {
        SAH_TRACEZ_INFO(ME, "received event: %s", event_name);
        amxc_var_log(event_data);
    }
}

/**
 * @brief
 * Update the remaining lease time of a lease by comparaison of the old system time and the new one gets on the system
 *
 * @param template Pools template for instances
 * @param lease The lease object to change
 * @param priv The transaction object
 * @return int 0 if success to update
 */
static int dhcp_lease_sys_time_updated(UNUSED amxd_object_t* template,
                                       amxd_object_t* lease,
                                       void* priv) {
    int ret = 1;
    amxd_trans_t* trans = (amxd_trans_t*) priv;
    amxc_ts_t new_time_sys;
    amxc_ts_t* old_time_sys = NULL;
    amxc_ts_t* expires = NULL;
    u_int64_t delta = 0;

    when_null_trace(lease, exit, ERROR, "Lease to update is NULL");

    old_time_sys = amxd_object_get_value(amxc_ts_t, lease, "LastUpdate", NULL);
    expires = amxd_object_get_value(amxc_ts_t, lease, "LeaseTimeRemaining", NULL);
    amxc_ts_now(&new_time_sys);
    delta = new_time_sys.sec - old_time_sys->sec;
    expires->sec += delta;

    amxd_trans_select_object(trans, lease);

    amxd_trans_set_value(amxc_ts_t, trans, "LastUpdate", &new_time_sys);
    amxd_trans_set_value(amxc_ts_t, trans, "LeaseTimeRemaining", expires);

    ret = 0;
exit:
    free(old_time_sys);
    free(expires);
    return ret;
}

/**
 * @brief
 * Update all the leases by comparing the previous system date with the new one
 *
 * @param event_name not used
 * @param event_data not used
 * @param priv not used
 */
void _dhcp_server_sys_time_updated(UNUSED const char* const event_name,
                                   const amxc_var_t* const event_data,
                                   UNUSED void* const priv) {
    amxd_object_t* pools = amxd_dm_findf(dhcp_get_dm(), "DHCPv4Server.Pool.");
    amxd_trans_t trans;
    amxc_var_t* status = NULL;
    int rv = 0;

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_prot, true);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    when_null_trace(pools, exit, WARNING, "No pools to update");
    when_null_trace(event_data, exit, ERROR, "No data given in the event");

    status = GETP_ARG(event_data, "parameters.Status");
    when_null_trace(status, exit, INFO, "No status updated");

    when_false_trace(strcmp(GET_CHAR(status, "to"), "Synchronized") == 0, exit, INFO, "Not updating base time as system time was not update");

    amxd_object_for_all(pools, ".*.Client.*.IPv4Address.*", dhcp_lease_sys_time_updated, (void*) &trans);

    rv = amxd_trans_apply(&trans, dhcp_get_dm());
    when_failed_trace(rv, exit, ERROR, "Cannot update lease with system time");
exit:
    amxd_trans_clean(&trans);
    return;
}

/**
 * @brief
 * Construct from a list of ips, an hax string by removing the comma
 *
 * @param hex_value The return value
 * @param value The list of ips, comma separated
 */
static void construct_hex_value(amxc_string_t* hex_value, char* value) {
    char* octets = NULL;
    when_null_trace(hex_value, exit, ERROR, "The hex value is not initialized");
    when_str_empty_trace(value, exit, ERROR, "WINSServer value is empty");

    octets = strtok(value, ".,");
    while(octets != NULL) {
        int oct_val = atoi(octets);
        amxc_string_appendf(hex_value, "%s%X", (oct_val > 15) ? "" : "0", oct_val);
        octets = strtok(NULL, ".,");
    }

exit:
    return;
}

/**
 * @brief
 * Publishes an event for a new or removed DHCP lease.
 *
 * @param name event name
 * @param data event data
 */
void dhcp_server_send_lease_event(const char* name, amxc_var_t* const data) {
    amxd_object_t* obj = NULL;
    amxc_var_t data_copy;
    amxc_var_init(&data_copy);
    when_null_trace(name, exit, ERROR, "Invalid name argument");
    when_null_trace(data, exit, ERROR, "Invalid data argument");
    obj = amxd_dm_findf(dhcp_get_dm(), "%s", DHCP_SERVER);
    if(obj != NULL) {
        SAH_TRACEZ_INFO(ME, "Sending '%s' DHCP event", name);
        amxc_var_copy(&data_copy, data);
        amxd_object_emit_signal(obj, name, &data_copy);
    }
exit:
    amxc_var_clean(&data_copy);
    return;
}

/**
 * @brief
 * Starts or stops the DHCPv4 service.
 * Called when the TR181 DHCPv4.Server.Enable data model parameter has changed.
 *
 * @param event_name event name
 * @param event_data event data
 * @param priv private data associated with event subscription
 */
void _dhcp_server_enable(UNUSED const char* const event_name,
                         const amxc_var_t* const event_data,
                         UNUSED void* const priv) {
    bool enable = false;
    amxc_var_t data;
    amxd_object_t* dhcpv4_obj = amxd_dm_findf(dhcp_get_dm(), "%s", DHCP_SERVER);
    int r = 1;

    amxc_var_init(&data);

    when_null_trace(event_data, exit, ERROR, "Invalid event_data argument");
    when_null_trace(dhcpv4_obj, exit, ERROR, "Cannot find DHCPv4Server object");
    SAH_TRACEZ_INFO(ME, "DHCPv4 server enable flag has changed in TR-181 data model");

    enable = GETP_BOOL(event_data, "parameters.Enable.to");
    SAH_TRACEZ_INFO(ME, "Handling DHCPv4.Server.Enable=%d...", enable ? 1 : 0);
    dm_dhcp_set_all_pools_enable(enable);

    // add signal to the end of the queue to start back-end after the defaults
    if(enable) {
        amxd_object_get_params(dhcpv4_obj, &data, amxd_dm_access_protected);
        amxd_object_emit_signal(amxd_dm_get_root(dhcp_get_dm()), "dhcpv4-server-start-back-end", &data);
    } else {
        char* controller = NULL;
        amxc_var_t ret;

        controller = amxd_object_get_value(cstring_t, dhcpv4_obj, "Controller", NULL);
        when_null_trace(controller, exit, ERROR, "Cannot get controller from server.");

        amxc_var_init(&ret);

        r = amxm_execute_function(controller,
                                  controller,
                                  "stop-back-end",
                                  &data,
                                  &ret);

        free(controller);
        amxc_var_clean(&ret);
        when_failed_trace(r, exit, ERROR, "Cannot stop dhcpv4 server back-end : %d", r);
    }
exit:
    amxc_var_clean(&data);
    return;
}

void _dhcp_server_config_changed(UNUSED const char* const event_name,
                                 const amxc_var_t* const event_data,
                                 UNUSED void* const priv) {
    amxc_var_t* tmp = NULL;
    int r = 1;
    char* controller = NULL;
    amxc_var_t ret;
    amxc_var_t data;
    amxd_object_t* dhcpv4_obj = amxd_dm_findf(dhcp_get_dm(), "%s", DHCP_SERVER);
    bool enable = amxd_object_get_value(bool, dhcpv4_obj, "Enable", NULL);

    amxc_var_init(&ret);
    amxc_var_init(&data);

    when_null_trace(dhcpv4_obj, exit, ERROR, "Cannot find DHCPv4Server object");

    controller = amxd_object_get_value(cstring_t, dhcpv4_obj, "Controller", NULL);
    when_null_trace(controller, exit, ERROR, "Cannot get controller from server.");

    if(enable) {
        r = amxm_execute_function(controller,
                                  controller,
                                  "stop-back-end",
                                  &data,
                                  &ret);

        when_failed_trace(r, exit, ERROR, "Cannot stop dhcpv4 server back-end : %d", r);

        amxd_object_get_params(dhcpv4_obj, &data, amxd_dm_access_protected);
        if((tmp = GETP_ARG(event_data, "parameters.IgnoreIDS")) != NULL) {
            amxc_var_set_key(&data, "IgnoreIDS", GET_ARG(tmp, "to"), AMXC_VAR_FLAG_DEFAULT);
        }
        if((tmp = GETP_ARG(event_data, "parameters.DDNSUpdate")) != NULL) {
            amxc_var_set_key(&data, "DDNSUpdate", GET_ARG(tmp, "to"), AMXC_VAR_FLAG_DEFAULT);
        }
        if((tmp = GETP_ARG(event_data, "parameters.EnableFQDN")) != NULL) {
            amxc_var_set_key(&data, "EnableFQDN", GET_ARG(tmp, "to"), AMXC_VAR_FLAG_DEFAULT);
        }

        r = amxm_execute_function(controller,
                                  controller,
                                  "start-back-end",
                                  &data,
                                  &ret);

        when_failed_trace(r, exit, ERROR, "Cannot start dhcpv4 server back-end : %d", r);
    }
exit:
    free(controller);
    amxc_var_clean(&ret);
    amxc_var_clean(&data);
    return;
}

/**
 * @brief
 * Adds or updates a DHCPv4 server pool in the UCI files.
 * Called when a TR181 DHCPv4.Server.Pool data model object instance is added or changed.
 *
 * @param event_name event name
 * @param event_data event data
 * @param priv private data associated with event subscription
 */
void _dhcp_server_pool_added(UNUSED const char* const event_name,
                             const amxc_var_t* const event_data,
                             UNUSED void* const priv) {
    bool enable = false;
    amxd_dm_t* dm = NULL;
    amxd_object_t* templ = NULL;
    amxd_object_t* pool = NULL;
    const char* name = NULL;

    SAH_TRACEZ_INFO(ME, "In DHCPv4 server pool added.");
    when_null_trace(event_data, exit, ERROR, "Invalid event_data argument");

    name = GET_CHAR(event_data, "name");
    when_null_trace(name, exit, ERROR, "Cannot get object name.");

    dm = dhcp_get_dm();
    when_null_trace(dm, exit, ERROR, "Cannot get DHCPv4 data model.");
    templ = amxd_dm_signal_get_object(dm, event_data);
    when_null_trace(templ, exit, ERROR, "Cannot get template for pool object.");
    pool = amxd_object_get_instance(templ, name, 0);
    when_null_trace(pool, exit, ERROR, "Cannot get pool object.");

    pool->priv = dhcp_pool_info_create(pool);
    when_null_trace(pool->priv, exit, ERROR, "Cannot create pool private info.");

    enable = amxd_object_get_value(bool, pool, "Enable", NULL);
    enable ? dhcp_pool_info_enable(pool) : dhcp_pool_info_disable(pool);
exit:
    return;
}

/**
 * @brief
 * Enable all static addresses of the pool with Enable parameter set to true.
 *
 * @param pool the pool object that the static ip addresses are associated to.
 */
static void dhcp_server_pool_enable_static_if_enabled_in_dm(amxd_object_t* pool) {
    amxd_object_for_each(instance, it, amxd_object_findf(pool, ".StaticAddress.")) {
        amxd_object_t* addr = amxc_container_of(it, amxd_object_t, it);
        if(amxd_object_get_value(bool, addr, "Enable", NULL)) {
            dm_dhcp_static_address_enable(pool, addr);
        }
    }
}

/**
 * @brief
 * Consider the Server.Pool DM object changed so update the underlying state
 *
 * @param pool the Pool object
 */
static void dhcp_server_pool_update(amxd_object_t* pool) {
    when_null_trace(pool, exit, ERROR, "DHCPv4 pool object can't be NULL");

    if(amxd_object_get_value(bool, pool, "Enable", NULL)) {
        dhcp_pool_info_enable(pool);
        dhcp_server_pool_enable_static_if_enabled_in_dm(pool);
    } else {
        dhcp_pool_info_disable(pool);
        dm_dhcp_pool_disable_static(pool);
    }

exit:
    return;
}

/**
 * @brief
 * Updates the DHCPv4 server pools in the UCI files whose Order Parameter will have changed.
 *
 * When a Server.Pool.{i}.Order parameter is set only that Order change will trigger an event.
 * All subsequently updated Order Parameters (incremented or compacted) won't trigger an event.
 *
 * Example:
 * A1|B2|C3|D4 (Before)
 * A1|D2|B3|C4 (After setting D Order to 2)
 *     |  ^--^-- Order changes without event (manual update)
 *     ^-- Order change with event (event_order)
 *
 * @param pools the Pool template object
 * @param event_order the Order change that got an event
 */
static void dhcp_server_pools_update_order_changes(amxd_object_t* pools, uint32_t min_order, uint32_t max_order) {
    when_null_trace(pools, exit, ERROR, "Pool template object is NULL");

    amxd_object_for_each(instance, it, pools) {
        amxd_object_t* pool_it_obj = amxc_llist_it_get_data(it, amxd_object_t, it);
        uint32_t order = 0;
        when_null_trace(pool_it_obj, continue_loop, ERROR, "Iterated Pool object NULL");

        order = amxd_object_get_uint32_t(pool_it_obj, "Order", NULL);
        when_false_trace(order > 0, continue_loop, ERROR, "Failed to get Pool Order Parameter");

        when_false(min_order == 0 || order >= min_order, continue_loop);
        when_false(max_order == 0 || order <= max_order, continue_loop);

        dhcp_server_pool_update(pool_it_obj);
continue_loop:
        continue;
    }
exit:
    return;
}

/**
 * @brief
 * Updates a DHCPv4 server pool in the UCI files.
 * Called when a TR181 DHCPv4.Server.Pool data model object instance is changed.
 *
 * When the "Order" parameter is changed only that Order change will trigger an event.
 * All subsequently updated Order Parameters (incrementing and compacting) won't trigger an event.
 * These will need to be triggered manually
 *
 * @param event_name event name
 * @param event_data event data
 * @param priv private data associated with event subscription
 */
void _dhcp_server_pool_changed(UNUSED const char* const event_name,
                               const amxc_var_t* const event_data,
                               UNUSED void* const priv) {
    amxd_dm_t* dm = NULL;
    amxd_object_t* pool = NULL;
    uint32_t to_order = 0;
    uint32_t from_order = 0;

    SAH_TRACEZ_INFO(ME, "DHCPv4 server pool has been changed in TR-181 data model");
    when_null_trace(event_data, exit, ERROR, "Invalid event_data argument");

    dm = dhcp_get_dm();
    when_null_trace(dm, exit, ERROR, "Cannot get DHCPv4 data model.");
    pool = amxd_dm_signal_get_object(dm, event_data);
    when_null_trace(pool, exit, ERROR, "Cannot get DHCPv4 pool object.");

    to_order = GETP_UINT32(event_data, "parameters.Order.to");
    if(to_order == 0) {
        dhcp_server_pool_update(pool);
    } else {
        uint32_t min_order = 0;
        uint32_t max_order = 0;
        from_order = GETP_UINT32(event_data, "parameters.Order.from");

        if(from_order > to_order) {
            min_order = to_order;
            max_order = from_order;
        } else {
            min_order = from_order;
            max_order = to_order;
        }

        dhcp_server_pools_update_order_changes(amxd_object_get_parent(pool),
                                               min_order, max_order);
    }
exit:
    return;
}

/**
 * @brief
 * Function handler to start the back-end
 *
 * @param event_name event name
 * @param event_data data of the event
 * @param priv NULL
 */
void _dhcp_server_back_end_start(UNUSED const char* const event_name,
                                 const amxc_var_t* const event_data,
                                 UNUSED void* const priv) {
    int r = 1;
    char* controller = NULL;
    amxc_var_t ret;
    amxc_var_t data;
    amxd_object_t* dhcpv4_obj = amxd_dm_findf(dhcp_get_dm(), "%s", DHCP_SERVER);

    amxc_var_init(&ret);
    amxc_var_init(&data);
    amxc_var_copy(&data, event_data);

    when_null_trace(dhcpv4_obj, exit, ERROR, "Cannot find DHCPv4.Server object");

    controller = amxd_object_get_value(cstring_t, dhcpv4_obj, "Controller", NULL);
    when_null_trace(controller, exit, ERROR, "Cannot get controller from pool.");
    r = amxm_execute_function(controller,
                              controller,
                              "start-back-end",
                              &data,
                              &ret);
    when_failed_trace(r, exit, ERROR, "Cannot start dhcpv4 server back-end");
exit:
    amxc_var_clean(&data);
    amxc_var_clean(&ret);
    free(controller);
    return;
}

/**
 * @brief
 * callback function when data managing the filter association criterion changes
 *
 * @param event_name name of the event
 * @param event_data change done in the vendor class data
 * @param priv Not used
 */
void _dhcp_server_pool_filter_changed(UNUSED const char* const event_name,
                                      const amxc_var_t* const event_data,
                                      UNUSED void* const priv) {
    amxd_object_t* pool = NULL;
    dhcp_pool_info_t* info = NULL;
    SAH_TRACE_IN();
    pool = amxd_dm_signal_get_object(dhcp_get_dm(), event_data);
    when_null_trace(pool, exit, ERROR, "Cannot get DHCPv4 pool option object.");

    info = (dhcp_pool_info_t*) pool->priv;
    when_null_trace(info, exit, ERROR, "DHCPv4 pool private data is NULL");
    SAH_TRACEZ_INFO(ME, "enable when filter changed : %d", info->flags.bit.enable);
    if(info->flags.bit.enable == 1) {
        when_failed_trace(dm_dhcp_update_pool_status(pool), exit, ERROR, "Failed to update DHCPv4 pool status");
    }
exit:
    SAH_TRACE_OUT();
    return;
}

/**
 * @brief
 * Pools Order rearranged
 *
 * @param event_name name of the event
 * @param event_data change done in the vendor class data
 * @param priv Not used
 */
void _dhcp_server_pools_order_rearranged(UNUSED const char* const event_name,
                                         UNUSED const amxc_var_t* const event_data,
                                         UNUSED void* const priv) {
    SAH_TRACE_IN();
    amxd_object_t* pools = amxd_dm_findf(dhcp_get_dm(), "DHCPv4Server.Pool.");

    when_null_trace(pools, exit, ERROR, "Cannot get DHCPv4 Pool template object.");

    dhcp_server_pools_update_order_changes(pools, 0, 0);
exit:
    SAH_TRACE_OUT();
    return;
}

/**
 * @brief
 * DHCPv4Server.Pool.{i} callback when a static address change to update known devices if AllowDevices != "All"
 *
 * @param event_name The name of the event
 * @param event_data Data of the StaticAddress added/changed
 * @param priv not used
 */
void _dhcp_server_pool_known_client_changed(const char* const event_name,
                                            const amxc_var_t* const event_data,
                                            UNUSED void* const priv) {
    amxd_object_t* event_object = NULL;
    amxd_object_t* pool = NULL;
    bool enable = false;
    char* allow = NULL;

    when_null_trace(event_name, exit, ERROR, "No event name given");
    when_null_trace(event_data, exit, ERROR, "No event data given");

    event_object = amxd_dm_signal_get_object(dhcp_get_dm(), event_data);
    when_null_trace(event_object, exit, ERROR, "Cannot retrieve event object");

    if(strcmp(event_name, "dm:object-changed") == 0) {
        pool = amxd_object_get_parent(amxd_object_get_parent(event_object));
    } else {
        pool = amxd_object_get_parent(event_object);
    }

    when_null_trace(pool, exit, ERROR, "Cannot retrieve Pool object");

    enable = amxd_object_get_value(bool, pool, "Enable", NULL);
    allow = amxd_object_get_value(cstring_t, pool, "AllowedDevices", NULL);

    when_str_empty_trace(allow, exit, ERROR, "No allow rule given");

    if(enable && (strcmp(allow, "All") != 0)) {
        dhcp_pool_info_enable(pool);
    }

exit:
    free(allow);
    return;
}

void _dhcp_server_pool_winsserver_changed(UNUSED const char* const event_name,
                                          const amxc_var_t* const event_data,
                                          UNUSED void* const priv) {
    char* winsserver = NULL;
    const char* pool_str = NULL;
    amxc_string_t hex_value;
    const char* hex_value_str = NULL;
    amxd_object_t* option_obj = NULL;
    amxd_object_t* option_44 = NULL;
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxc_string_init(&hex_value, 0);

    when_null_trace(event_data, exit, ERROR, "No event data provided");
    winsserver = amxc_var_dyncast(cstring_t, GETP_ARG(event_data, "parameters.0.to"));
    when_null_trace(winsserver, exit, ERROR, "Couldn't parse the WINSServer value");

    pool_str = GET_CHAR(event_data, "object");
    when_null_trace(pool_str, exit, ERROR, "No pool object provided");

    option_obj = amxd_dm_findf(dhcp_get_dm(), "%sOption.", pool_str);
    when_null_trace(option_obj, exit, ERROR, "No %sOption object found", pool_str);

    option_44 = amxd_dm_findf(dhcp_get_dm(), "%sOption.[Tag==44]", pool_str);

    if(str_empty(winsserver) && (option_44 != NULL)) {
        amxd_trans_select_object(&trans, option_obj);
        amxd_trans_del_inst(&trans, 0, amxd_object_get_name(option_44, AMXD_OBJECT_NAMED));
        when_failed_trace(amxd_trans_apply(&trans, dhcp_get_dm()), exit, ERROR, "Cannot apply the transaction");
        goto exit;
    }

    construct_hex_value(&hex_value, winsserver);
    hex_value_str = amxc_string_get(&hex_value, 0);
    when_str_empty_trace(hex_value_str, exit, ERROR, "The value of option-44 is not constructed correctly");

    if(option_44 == NULL) {
        amxd_trans_select_object(&trans, option_obj);
        amxd_trans_add_inst(&trans, 0, "cpe-dhcp-44");
        amxd_trans_set_value(int8_t, &trans, "Tag", 44);
        amxd_trans_set_value(cstring_t, &trans, "Value", hex_value_str);
        amxd_trans_set_value(bool, &trans, "Enable", true);
    } else {
        amxd_trans_select_object(&trans, option_44);
        amxd_trans_set_value(cstring_t, &trans, "Value", hex_value_str);
    }

    when_failed_trace(amxd_trans_apply(&trans, dhcp_get_dm()), exit, ERROR, "Cannot apply the transaction");

exit:
    free(winsserver);
    amxc_string_clean(&hex_value);
    amxd_trans_clean(&trans);
    return;
}