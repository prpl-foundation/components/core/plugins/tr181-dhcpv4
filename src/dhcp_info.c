/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include <amxc/amxc_macros.h>

#include "dhcp_info.h"
#include "dm_dhcp.h"

#define ME "dhcp_info"

/**
 * @brief
 * Allocates and populates the memory used by a pool private data.
 *
 * @param dhcp_pool The related DHCPv4.Server.Pool object.
 * @return dhcp_pool_info_t* Pointer to pool item, NULL elsewhere
 */
dhcp_pool_info_t* dhcp_pool_info_create(amxd_object_t* dhcp_pool) {
    dhcp_pool_info_t* info = (dhcp_pool_info_t*) calloc(1, sizeof(dhcp_pool_info_t));
    char* path = NULL;

    when_null_trace(info, exit, ERROR, "Cannot allocate memory for pool private information.");

    info->obj = dhcp_pool;
    info->flags.all_flags = 0;
    info->netmodel_addr_query = NULL;
    info->addresses_ht = NULL;
    path = amxd_object_get_value(cstring_t, dhcp_pool, "Interface", NULL);
    when_null_trace(path, exit, ERROR, "Cannot get interface for pool.");

    info->intf_path = path;
    path = NULL;
    info->flags.bit.intf_ok = 1;
exit:
    free(path);
    return info;
}

/**
 * @brief
 * Cleans up the memory used by a pool private data.
 * Closes the NetModel query if needed.
 *
 * @param info The pool private info to clean
 */
void dhcp_pool_info_clear(dhcp_pool_info_t* info) {
    when_null_trace(info, exit, WARNING, "No pool private data to free.");
    if(info->netmodel_addr_query != NULL) {
        netmodel_closeQuery(info->netmodel_addr_query);
        info->netmodel_addr_query = NULL;
    }
    if(info->netmodel_alias_query != NULL) {
        netmodel_closeQuery(info->netmodel_alias_query);
        info->netmodel_alias_query = NULL;
    }
    if(info->addresses_ht != NULL) {
        amxc_var_delete(&(info->addresses_ht));
        info->addresses_ht = NULL;
    }
    free(info->intf_path);
    free(info->intf_alias);
    info->obj = NULL;
    free(info);
exit:
    return;
}

/**
 * @brief
 * add address information to the pool private information list.
 *
 * @param addrs address information from the Netmodel query.
 * @param addrs_ht The htable of the addr.
 */
static void dhcp_addr_info_add(const amxc_var_t* addr, amxc_var_t* addrs_ht) {
    const char* address = NULL;

    when_null_trace(addr, exit, ERROR, "No addresses given to populate pool information list.");

    address = GET_CHAR(addr, "Address");
    when_null_trace(address, exit, ERROR, "Cannot resolve address from list of addresses.");

    SAH_TRACEZ_INFO(ME, "Adding IPv4 address '%s'.", address);
    amxc_var_add_key(amxc_htable_t, addrs_ht, address, NULL);
exit:
    return;
}

/**
 * @brief
 * Apply change of pool network interfaces' change to the private info.
 * This assume that an ip address is unique on a given network interface.
 *
 * @param sig_name signal name
 * @param addrs List of netmodel address.
 * @param priv private data of the pool object.
 */
static void dhcp_pool_info_change_addrs_cb(UNUSED const char* sig_name,
                                           const amxc_var_t* addrs,
                                           void* priv) {
    dhcp_pool_info_t* info = NULL;
    amxc_var_t new_addrs_ht;

    amxc_var_init(&new_addrs_ht);
    amxc_var_set_type(&new_addrs_ht, AMXC_VAR_ID_HTABLE);

    SAH_TRACEZ_INFO(ME, "Callback function for change_addrs query called.");

    when_null_trace(addrs, exit, ERROR, "No addresses given.");
    when_null_trace(priv, exit, ERROR, "No network interface information private data given.");

    info = (dhcp_pool_info_t*) priv;
    info->flags.bit.addr_ok = 0;

    amxc_var_for_each(addr, addrs) {
        dhcp_addr_info_add(addr, &new_addrs_ht);
        info->flags.bit.addr_ok = 1;
    }

    if(info->addresses_ht != NULL) {
        amxc_var_delete(&(info->addresses_ht));
    }
    amxc_var_new(&(info->addresses_ht));
    amxc_var_copy(info->addresses_ht, &new_addrs_ht);

    dm_dhcp_update_pool_status(info->obj);
exit:
    amxc_var_clean(&new_addrs_ht);
    return;
}

/**
 * @brief
 * Callback function for the InterfaceAlias query. Will update the Status of the dhcp pool.
 *
 * @param sig_name Signal name, not used.
 * @param data Interface alias name.
 * @param priv Private pool_info_t corresponding to the pool interface.
 */
static void dhcp_pool_info_changed_alias_cb(UNUSED const char* const sig_name,
                                            const amxc_var_t* const data,
                                            void* const priv) {
    dhcp_pool_info_t* info = (dhcp_pool_info_t*) priv;
    char* intf_alias = NULL;
    SAH_TRACEZ_INFO(ME, "Callback function for Interface alias query called.");
    when_null_trace(data, exit, ERROR, "Data is null.");
    when_null_trace(info, exit, ERROR, "Unable to retrieve info for the dhcp.");

    info->flags.bit.intf_alias = 0;

    intf_alias = amxc_var_dyncast(cstring_t, data);
    when_null_trace(intf_alias, exit, ERROR, "Cannot get the interface Alias from NetModel.");
    free(info->intf_alias);
    info->intf_alias = intf_alias;
    info->flags.bit.intf_alias = 1;
    dm_dhcp_update_pool_status(info->obj);
exit:
    return;
}

/**
 * @brief
 * Helper to open all the netmodel queries needed for a DHCPv4 pool configuration.
 *
 * @param info The pool private information.
 */
static void dhcp_pool_open_queries(dhcp_pool_info_t* info) {
    when_null_trace(info, exit, ERROR, "Cannot get pool info.");

    if(info->netmodel_addr_query != NULL) {
        netmodel_closeQuery(info->netmodel_addr_query);
        info->netmodel_addr_query = NULL;
        info->flags.bit.addr_ok = 0;
    }
    if(info->netmodel_alias_query != NULL) {
        netmodel_closeQuery(info->netmodel_alias_query);
        info->netmodel_alias_query = NULL;
        info->flags.bit.intf_alias = 0;
    }

    info->netmodel_addr_query = netmodel_openQuery_getAddrs((const char*) info->intf_path,
                                                            "DHCPv4s",
                                                            "ipv4",
                                                            netmodel_traverse_down,
                                                            dhcp_pool_info_change_addrs_cb,
                                                            info);
    if(info->netmodel_addr_query == NULL) {
        SAH_TRACEZ_ERROR(ME, "Failed to add addr query to netmodel");
        dm_dhcp_update_pool_status(info->obj);
    }

    info->netmodel_alias_query = netmodel_openQuery_getFirstParameter((const char*) info->intf_path,
                                                                      "DHCPv4s",
                                                                      "InterfaceAlias",
                                                                      NULL,
                                                                      netmodel_traverse_down,
                                                                      dhcp_pool_info_changed_alias_cb,
                                                                      (void*) info);
    if(info->netmodel_alias_query == NULL) {
        SAH_TRACEZ_ERROR(ME, "Failed to add alias query to netmodel");
        dm_dhcp_update_pool_status(info->obj);
    }
exit:
    return;
}

/**
 * @brief
 * Subscribe to netmodel queries.
 *
 * @param dhcp_pool the dhcp pool object.
 */
void dhcp_pool_info_enable(amxd_object_t* dhcp_pool) {
    dhcp_pool_info_t* info = NULL;
    SAH_TRACEZ_INFO(ME, "Enable pool info function called.");
    when_null_trace(dhcp_pool, exit, ERROR, "Cannot get the dhcp pool object.");
    when_null_trace(dhcp_pool->priv, exit, ERROR, "Cannot get private pool info data from pool object.");

    info = (dhcp_pool_info_t*) dhcp_pool->priv;

    info->flags.bit.enable = 1;
    dhcp_pool_open_queries(info);
exit:
    return;
}

/**
 * @brief
 * Unsubscribe to netmodel queries
 *
 * @param dhcp_pool the dhcp pool object.
 */
void dhcp_pool_info_disable(amxd_object_t* dhcp_pool) {
    dhcp_pool_info_t* info = NULL;
    SAH_TRACEZ_INFO(ME, "Disable pool info function called.");
    when_null_trace(dhcp_pool, exit, ERROR, "Cannot get the dhcp pool object.");
    when_null_trace(dhcp_pool->priv, exit, ERROR, "Cannot get private pool info data from pool object.");

    info = (dhcp_pool_info_t*) dhcp_pool->priv;

    info->flags.bit.enable = 0;

    dm_dhcp_update_pool_status(info->obj);

    if(info->netmodel_addr_query != NULL) {
        netmodel_closeQuery(info->netmodel_addr_query);
        info->netmodel_addr_query = NULL;
        info->flags.bit.addr_ok = 0;
    }
    if(info->netmodel_alias_query != NULL) {
        netmodel_closeQuery(info->netmodel_alias_query);
        info->netmodel_alias_query = NULL;
        info->flags.bit.intf_alias = 0;
    }
exit:
    return;
}
