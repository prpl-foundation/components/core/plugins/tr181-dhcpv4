include makefile.inc

NOW = $(shell date +"%Y-%m-%d(%H:%M:%S %z)")

# Extra destination directories
PKGDIR = ./output/$(MACHINE)/pkg/

define create_changelog
	@$(ECHO) "Update changelog"
	mv CHANGELOG.md CHANGELOG.md.bak
	head -n 9 CHANGELOG.md.bak > CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(ECHO) "## Release $(VERSION) - $(NOW)" >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(GIT) log --pretty=format:"- %s" $$($(GIT) describe --tags | grep -v "merge" | cut -d'-' -f1)..HEAD  >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	tail -n +10 CHANGELOG.md.bak >> CHANGELOG.md
	rm CHANGELOG.md.bak
endef

# targets
all:
	$(MAKE) -C src all
	$(MAKE) -C lib_dnsmasq/src all
	$(MAKE) -C mod-dhcp-uci/src all
	$(MAKE) -C mod-dhcp-dnsmasq/src all
	$(MAKE) -C odl all

clean:
	$(MAKE) -C src clean
	$(MAKE) -C lib_dnsmasq/src clean
	$(MAKE) -C mod-dhcp-uci/src clean
	$(MAKE) -C mod-dhcp-dnsmasq/src clean
	$(MAKE) -C odl clean

install: all
	$(INSTALL) -D -p -m 0644 odl/dhcpv4-manager.odl $(DEST)/etc/amx/$(COMPONENT)/dhcpv4-manager.odl
	$(INSTALL) -D -p -m 0644 odl/dhcpv4-manager_definition.odl $(DEST)/etc/amx/$(COMPONENT)/dhcpv4-manager_definition.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_mapping.odl $(DEST)/etc/amx/tr181-device/extensions/01_device-dhcpv4-server_mapping.odl
	$(INSTALL) -D -p -m 0644 odl/dhcpv4-manager_caps.odl $(DEST)/etc/amx/$(COMPONENT)/dhcpv4-manager_caps.odl
	$(INSTALL) -d -m 0755 $(DEST)//etc/amx/$(COMPONENT)/server
	$(foreach odl,$(wildcard odl/server/*.odl), $(INSTALL) -D -p -m 0644 $(odl) $(DEST)/etc/amx/$(COMPONENT)/server/;)
	$(INSTALL) -d -m 0755 $(DEST)//etc/amx/$(COMPONENT)/defaults.d
	$(foreach odl,$(wildcard odl/defaults.d/*.odl), $(INSTALL) -D -p -m 0644 $(odl) $(DEST)/etc/amx/$(COMPONENT)/defaults.d/;)
	$(INSTALL) -D -p -m 0660 acl/admin/$(COMPONENT).json $(DEST)$(ACLDIR)/admin/$(COMPONENT).json
	$(INSTALL) -D -p -m 0660 acl/cwmp/$(COMPONENT).json $(DEST)$(ACLDIR)/cwmp/$(COMPONENT).json
	$(INSTALL) -D -p -m 0644 acl/$(COMPONENT).json $(DEST)/usr/share/acl.d/$(COMPONENT).json
	$(INSTALL) -d -m 0755 $(DEST)$(BINDIR)
	ln -sfr $(DEST)/usr/bin/amxrt $(DEST)$(BINDIR)/$(COMPONENT)
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/$(COMPONENT).so $(DEST)/usr/lib/amx/$(COMPONENT)/$(COMPONENT).so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-dhcp-uci.so $(DEST)/usr/lib/amx/$(COMPONENT)/modules/mod-dhcp-uci.so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-dhcp-dnsmasq.so $(DEST)/usr/lib/amx/$(COMPONENT)/modules/mod-dhcp-dnsmasq.so
	$(INSTALL) -D -p -m 0755 scripts/$(COMPONENT).sh $(DEST)$(INITDIR)/$(COMPONENT)

package: all
	$(INSTALL) -D -p -m 0644 odl/dhcpv4-manager.odl $(PKGDIR)/etc/amx/$(COMPONENT)/dhcpv4-manager.odl
	$(INSTALL) -D -p -m 0644 odl/dhcpv4-manager_definition.odl $(PKGDIR)/etc/amx/$(COMPONENT)/dhcpv4-manager_definition.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_mapping.odl $(PKGDIR)/etc/amx/tr181-device/extensions/01_device-dhcpv4-server_mapping.odl
	$(INSTALL) -D -p -m 0644 odl/dhcpv4-manager_caps.odl $(PKGDIR)/etc/amx/$(COMPONENT)/dhcpv4-manager_caps.odl
	$(INSTALL) -d -m 0755 $(PKGDIR)//etc/amx/$(COMPONENT)/server
	$(INSTALL) -D -p -m 0644 odl/server/*.odl $(PKGDIR)/etc/amx/$(COMPONENT)/server/
	$(INSTALL) -d -m 0755 $(PKGDIR)//etc/amx/$(COMPONENT)/defaults.d
	$(INSTALL) -D -p -m 0644 odl/defaults.d/*.odl $(PKGDIR)/etc/amx/$(COMPONENT)/defaults.d/
	$(INSTALL) -D -p -m 0660 acl/admin/$(COMPONENT).json $(PKGDIR)$(ACLDIR)/admin/$(COMPONENT).json
	$(INSTALL) -D -p -m 0660 acl/cwmp/$(COMPONENT).json $(PKGDIR)$(ACLDIR)/cwmp/$(COMPONENT).json
	$(INSTALL) -D -p -m 0644 acl/$(COMPONENT).json $(PKGDIR)/usr/share/acl.d/$(COMPONENT).json
	$(INSTALL) -d -m 0755 $(PKGDIR)$(BINDIR)
	rm -f $(PKGDIR)$(BINDIR)/$(COMPONENT)
	ln -sfr $(PKGDIR)/usr/bin/amxrt $(PKGDIR)$(BINDIR)/$(COMPONENT)
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/$(COMPONENT).so $(PKGDIR)/usr/lib/amx/$(COMPONENT)/$(COMPONENT).so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-dhcp-uci.so $(PKGDIR)/usr/lib/amx/$(COMPONENT)/modules/mod-dhcp-uci.so
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/mod-dhcp-dnsmasq.so $(PKGDIR)/usr/lib/amx/$(COMPONENT)/modules/mod-dhcp-dnsmasq.so
	$(INSTALL) -D -p -m 0755 scripts/$(COMPONENT).sh $(PKGDIR)$(INITDIR)/$(COMPONENT)
	cd $(PKGDIR) && $(TAR) -czvf ../$(COMPONENT)-$(VERSION).tar.gz .
	cp $(PKGDIR)../$(COMPONENT)-$(VERSION).tar.gz .
	make -C packages

changelog:
	$(call create_changelog)

doc:
	$(eval ODLFILES += odl/dhcpv4-manager_definition.odl)

	mkdir -p output/xml
	mkdir -p output/html
	mkdir -p output/confluence
	amxo-cg -Gxml,output/xml/$(COMPONENT).xml $(or $(ODLFILES), "")
	amxo-xml-to -x html -o output-dir=output/html -o title="$(COMPONENT)" -o version=$(VERSION) -o sub-title="Datamodel reference" output/xml/*.xml
	amxo-xml-to -x confluence -o output-dir=output/confluence -o title="$(COMPONENT)" -o version=$(VERSION) -o sub-title="Datamodel reference" output/xml/*.xml

test:
	$(MAKE) -C test run
	$(MAKE) -C lib_dnsmasq/test run
	$(MAKE) -C mod-dhcp-uci/test run
	$(MAKE) -C mod-dhcp-dnsmasq/test run
	$(MAKE) -C test coverage

.PHONY: all clean changelog install package doc test