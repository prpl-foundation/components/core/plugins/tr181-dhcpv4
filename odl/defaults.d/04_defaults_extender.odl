/**
 * Default DHCPv4.Server.Pool configuration for WNC board
 *
 * This file is based on the normal configuration gotten from:
 * - /etc/config/network
 * - /etc/config/dhcp
 * This configuration can override settings in the second file,
 * any change in these defaults should be checked for correction with
 * IP.Interface. defaults.
 *
 * @version 1.0
 */

%populate {
    object 'DHCPv4Server' {
        object 'Pool' {
            instance add('lan_extender') {
                parameter 'Interface' = "${ip_intf_lan}";
                parameter 'LeaseTime' = 43200;
                parameter 'Tag' = "extender";
                parameter 'DNSServers' = "192.168.1.1" {
                    userflags %usersetting;
                }
                parameter 'Enable' = true {
                    userflags %usersetting;
                }
                parameter 'IPRouters' = "192.168.1.1" {
                    userflags %usersetting;
                }
                parameter 'SubnetMask' = "255.255.255.0" {
                    userflags %usersetting;
                }
                parameter 'MinAddress' = "192.168.1.100" {
                    userflags %usersetting;
                }
                parameter 'MaxAddress' = "192.168.1.150" {
                    userflags %usersetting;
                }
                parameter 'VendorClassID' = "extender";
                parameter 'VendorClassIDExclude' = false;
                parameter 'VendorClassIDMode' = "Substring";
                object 'Option' {
                    instance add('cpe-dhcp-125') {
                        parameter 'Enable' = true;
                        parameter 'Tag' = 125;
                        parameter 'Value' = "$(GW_DHCPV4_OPTION_125_VALUE)";
                    }
                }
            }
        }
    }
}
