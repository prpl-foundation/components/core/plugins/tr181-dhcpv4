%define {
    select DHCPv4Server {
        /**
         * DHCPv4.Server.Pool from TR-181 datamodel 2.11
         *
         * @version 1.0
         */
        %persistent object Pool[] {
            on action add-inst call add_ordered_instance;
            on action del-inst call dhcp_server_pool_removed;
            on action destroy call remove_ordered_instance;

            /**
             * RPC method to add multiple static client in the pool, only the MAC of the wanted clients is needed.
             * An ip will be assigned in the available range.
             * @param MACs : list containing all the MAC of the clients.
             * @return bool : true on success
             */
            %protected bool setStaticClients(%in %mandatory list MACs);

            /**
             * RPC method to remove multiple static client in the pool, only the MAC of the wanted static ip is needed.
             * If a MAc is not found nothing will be done and the method will succeed
             * @param MACs : list containing all the MAC of the static ips to remove.
             * @return bool : true on success
             */
            %protected bool rmStaticClients(%in %mandatory list MACs);

            /**
             * RPC method to remove the states of the leases given in the list for MAC addresses.
             * If no MAC addresses is given then all leases are removed.
             * @param MACs : list containing all the MAC of the static ips to remove.
             * @return bool : true on success
             */
            %protected bool cleanClientLeases(%in list MACs);

            /**
             * Number of instances
             *
             * @version 1.0
             * type: unsignedInt
             */
            counted with PoolNumberOfEntries;

            /**
             * Tag to identify the type of pool created, an extender pool take precedence over the master pool.
             * This parameter have a precedence over the Order parameter.
             *
             * @version 1.0
             * type: string enumeration
             */
            %protected %persistent string Tag = "master" {
                on action validate call check_enum
                    ["master", "extender"];
            }

            /**
             * Enable this instance
             *
             * @version 1.0
             * type: bool
             */
            %persistent bool Enable {
                default false;
            }

            /**
             * Status as ennumeration defined in TR-181
             *
             * @version 1.0
             * type: string enumeration
             */
            %read-only string Status = "Disabled" {
                on action validate call check_enum
                    ["Disabled", "Enabled", "Error_Misconfigured", "Error"];
            }

            /**
             * Alias for the instance
             *
             * @version 1.0
             * type: string(64)
             */
            %persistent %unique %key string Alias {
                on action validate call check_maximum_length 64;
            }

            /**
             * Priority order for the Pool
             *
             * @version 1.0
             * type: unsignedInt reordering and be unique on change/insertion
             */
            %persistent uint32 Order {
                on action validate call check_order_range;
            }

            /**
             * Instance from IP.Interface path needs to be linked to a Pool instance
             *
             * @version 1.0
             * type: string(256)
             */
            %persistent string Interface = "" {
                on action validate call check_maximum_length 256;
            }

            /**
             * VendorClassID as Pool association criterion
             *
             * @version 1.0
             * type: string(255)
             */
            %persistent string VendorClassID = "" {
                on action validate call check_maximum_length 255;
            }

            /**
             * Exclude VendorClassID criterion match
             *
             * @version 1.0
             * type: bool
             */
            %persistent bool VendorClassIDExclude = false;

            /**
             * VendorClassID match criterion
             *
             * @version 1.0
             * type: string enumeration
             */
            %persistent string VendorClassIDMode = "Exact" {
                on action validate call check_enum
                    ["Exact", "Prefix", "Suffix", "Substring"];
            }

            /**
             * ClientID Pool association criterion
             *
             * @version 1.0
             */
            %persistent string ClientID = "" {
                on action validate call check_maximum_length 255;
                on action validate call matches_regexp "^[0-9A-Fa-f]+$";
            }

            /**
             * Exclude ClientID criterion match
             *
             * @version 1.0
             * type: bool
             */
            %persistent bool ClientIDExclude = false;

            /**
             * UserClassID as Pool association criterion
             *
             * @version 1.0
             */
            %persistent string UserClassID = "" {
                on action validate call check_maximum_length 255;
                on action validate call matches_regexp "^[0-9A-Fa-f]+$";
            }

            /**
             * Exclude UserClassID as criterion match
             *
             * @version 1.0
             * type: bool
             */
            %persistent bool UserClassIDExclude = false;

            /**
             * MAC Address association criterion
             *
             * @version 1.0
             * type: string(17)
             */
            %persistent string Chaddr = "" {
                on action validate call is_valid_macaddr;
            }

            /**
             * Bit-mask used for Chaddr classification
             *
             * @version 1.0
             * type: string(17)
             */
            %persistent string ChaddrMask = "" {
                on action validate call is_valid_macaddr;
            }

            /**
             * Exclude Chaddr criterion match
             *
             * @version 1.0
             * type: bool
             */
            %persistent bool ChaddrExclude = false;

            /**
             * Pool association criterion. Determines which devices are allowed, Enumeration of:
             *
             * - All (All clients are served)
             * - Known (Only clients, whose MAC address is listed in the Client.{i}. table (parameter Client.{i}.Chaddr) or in the StaticAddress.{i}. table (parameter StaticAddress.{i}.Chaddr) are served. The parameter {{param: non-existent #.Chaddr}} is not used, if this value is set)
             * - Unknown (Only clients, whose MAC address is not listed in the Client.{i}. table (parameter Client.{i}.Chaddr) or in the StaticAddress.{i}. table (parameter StaticAddress.{i}.Chaddr) are served)
             *
             * @version 1.0
             * type: string
             */
            %persistent string AllowedDevices = "All" {
                on action validate call check_enum
                    ["All", "Known", "Unknown"];
            }

            /**
             * First IP Address of the Pool
             *
             * @version 1.0
             * type: string(15)
             */
            %persistent string MinAddress = "" {
                on action validate call is_valid_ipv4;
            }

            /**
             * Last IP Address of the Pool
             *
             * @version 1.0
             * type: string(15)
             */
            %persistent string MaxAddress = "" {
                on action validate call is_valid_ipv4;
            }

            /**
             * List of reserved IP addresses
             *
             * @version 1.0
             * TODO: type: list of IP addresses up to 32
             */
            %persistent csv_string ReservedAddresses = "";

            /**
             * Subnet mask for the client's network
             *
             * @version 1.0
             * type: string(15)
             */
            %persistent string SubnetMask = "" {
                on action validate call is_valid_ipv4;
            }

            /**
             * List of DNSServers
             * Support for more than three DNSServers is OPTIONAL
             *
             * @version 1.0
             * TODO: type: CSV up to 4 IP addresses
             */
            %persistent csv_string DNSServers ;

            /**
             * Domain name to provide clients on the LAN interface
             *
             * @version 1.0
             * type: string(64)
             */
            %persistent string DomainName {
                on action validate call check_maximum_length 64;
            }

            /**
             * List of IP routers on the subnet
             * Support for more than three routers is OPTIONAL
             *
             * @version 1.0
             * TODO: type: CSV up to 4 IP addresses
             */
            %persistent csv_string IPRouters = "";

            /**
             * Lease time in seconds for the clients
             * A value of -1 means infinite
             *
             * @version 1.0
             * type: int32
             */
            %persistent int32 LeaseTime {
                default 86400;
                userflags %upc;
            }

            /**
             * Comma separated list of IPv4 WINS Servers
             *
             * @version 1.0
             * type: CSV up to 4 IP addresses
             */
            %persistent csv_string '${prefix_}WINSServer' = "" {
                on action validate call is_valid_ipv4;
                userflags %usersetting;
            }


            /**
             * DHCPv4.Server.Pool.Client from TR-181 datamodel 2.11
             *
             * @version 1.0
             */
            %persistent %read-only object Client[] {
                /**
                 * Number of Client instances
                 *
                 * @version 1.0
                 * type: unsignedInt
                 */
                counted with ClientNumberOfEntries;

                /**
                 * Alias for the instance
                 *
                 * @version 1.0
                 * type: string(64)
                 */
                %unique %key string Alias {
                    on action validate call check_maximum_length 64;
                }

                /**
                 * MAC Address associated to the client
                 *
                 * @version 1.0
                 * type: string(17)
                 */
                %persistent %read-only string Chaddr = "" {
                    on action validate call is_valid_macaddr;
                    userflags %upc;
                }

                /**
                 * Mark whether the client is active
                 * To list inactive DHCPv4 clients is OPTIONAL
                 *
                 * @version 1.0
                 * type: bool
                 */
                %read-only bool Active = true;

                /**
                 * IP addresses assigned to this client
                 *
                 * @version 1.0
                 */
                %persistent %read-only object IPv4Address[] {
                    /**
                     * Number of instances in the IPv4Address table
                     *
                     * @version 1.0
                     * type: unsignedInt
                     */
                    counted with IPv4AddressNumberOfEntries;

                    /**
                     * IPv4 Address
                     *
                     * @version 1.0
                     * type: string(15)
                     */
                    %persistent %read-only string IPAddress = "" {
                        on action validate call is_valid_ipv4;
                        userflags %upc;
                    }

                    /**
                     * Time at which the DHCP lease will expire
                     *
                     * @version 1.0
                     * - unknown set to 0001-01-01T00:00:00Z
                     * - infinite must be 9999-12-31T23:59:59Z
                     */
                    %persistent %read-only datetime LeaseTimeRemaining = "0001-01-01T00:00:00Z" {
                        userflags %upc;
                    }

                    /**
                     * Whether this IP assignment is configured to be static.
                     *
                     * DHCPv4.Server.Pool.<pool>.Client.<client>.IPv4Address.<ip>.IsStatic is true if and only if
                     * there exists a static address configuration <staticaddr> such that
                     * DHCPv4.Server.Pool.<pool>.StaticAddress.<staticaddr>.Enable == true
                     * && DHCPv4.Server.Pool.<pool>.StaticAddress.<staticaddr>.Yiaddr == <ip>
                     * && DHCPv4.Server.Pool.<pool>.StaticAddress.<staticaddr>.Chaddr == DHCPv4.Server.Pool.<pool>.Client.<client>.Chaddr
                     */
                    %read-only bool IsStatic = false;

                    /**
                     *  Time when the lease was updated
                     *
                     * It is used when the date on the system change to recalculate the expiration of the lease.
                     */
                    %persistent %protected %read-only datetime LastUpdate = "0001-01-01T00:00:00Z";
                }

                /**
                 * DHCPv4 Options supplied by this client
                 *
                 * @version 1.0
                 */
                 %persistent %read-only object Option[] {
                    on action destroy call dhcps_client_option_destroy;

                    /**
                     * Number of entries in the Option table
                     *
                     * @version 1.0
                     * type: unsignedInt
                     */
                    counted with OptionNumberOfEntries;

                    /**
                     * Option tag as defined in RFC2132
                     *
                     * @version 1.0
                     */
                    %persistent %read-only uint8 Tag{
                        userflags %upc;
                    }

                    /**
                     * Hexbinary coded option value
                     *
                     * @version 1.0
                     */
                    %persistent %read-only string Value {
                        on action validate call check_maximum_length 255;
                        on action validate call matches_regexp "^[0-9A-Fa-f]+$";
                        userflags %upc;
                    }
                }
            }

            /**
             * DHCPv4.Server.Pool.StaticAddress from TR-181 datamodel 2.11
             *
             * @version 1.0
             */
            %persistent object StaticAddress[] {
                /**
                 * Number of StaticAddress instances in the table
                 *
                 * @version 1.0
                 * type: unsignedInt
                 */
                counted with StaticAddressNumberOfEntries;

                /**
                 * Enable this StaticAddress instance
                 *
                 * @version 1.0
                 * type: bool
                 */
                %persistent bool Enable {
                    default false;
                    userflags %usersetting;
                }

                /**
                 * Alias for this StaticAddress
                 *
                 * @version 1.0
                 * type: string(64)
                 */
                %persistent %unique %key string Alias {
                    on action validate call check_maximum_length 64;
                }

                /**
                 * MAC address associated to this StaticAddress
                 *
                 * @version 1.0
                 * type: string(17)
                 */
                %persistent string Chaddr = "" {
                    on action validate call is_valid_macaddr;
                    userflags %usersetting;
                }

                /**
                 * IP Address to be assigned to the client matching the
                 * MAC address
                 *
                 * Marked as %unique to avoid problems restoring from configuration
                 *
                 * @version 1.1
                 * type: string(15)
                 */
                %persistent string Yiaddr = "" {
                    on action validate call is_not_reserved_addr;
                    userflags %usersetting;
                }
            }

            /**
             * DHCPv4.Server.Pool.Option from TR-181 datamodel 2.11
             *
             * Options that must, if enabled, be returned to clients associated
             * to this Pool.
             *
             * @version 1.0
             */
            %persistent object Option[] {
                /**
                 * Number of Options in the table
                 *
                 * @version 1.0
                 * type: unsignedInt
                 */
                counted with OptionNumberOfEntries;

                /**
                 * Enable this Option
                 *
                 * @version 1.0
                 * type: bool
                 */
                %persistent bool Enable = false;

                /**
                 * Force this Option
                 *
                 * @version 1.0
                 * type: bool
                 */
                %persistent bool '${prefix_}Force' = false;

                /**
                 * Alias for the Option
                 *
                 * @version 1.0
                 * type: string(64)
                 */
                %persistent %unique %key string Alias {
                    on action validate call check_maximum_length 64;
                }

                /**
                 * Tag for the option as defined in RFC2132
                 *
                 * @version 1.0
                 */
                %persistent uint8 Tag = 254 {
                    on action validate call check_range [1, 254];
                }

                /**
                 * Hexbinary encoded option value
                 *
                 * @version 1.0
                 */
                %persistent string Value = "" {
                    on action validate call check_maximum_length 255;
                    on action validate call matches_regexp "^[0-9A-Fa-f]+$";
                }
            }
        }
    }
}

%populate {
    // Pool events
    on event "dm:instance-added" call dhcp_server_pool_added
        filter 'path == "DHCPv4Server.Pool."';

    on event "dm:instance-removed" call dhcp_server_pools_order_rearranged
        filter 'path == "DHCPv4Server.Pool."';

    on event "dm:object-changed" call order_changed
        filter 'path matches "^DHCPv4Server\.Pool\.[0-9]+\.$" and
                contains("parameters.Order")';

    on event "dm:object-changed" call dhcp_server_pool_changed
        filter 'path matches "^DHCPv4Server\.Pool\.[0-9]+\.$" and
                (contains("parameters.MinAddress") or
                contains("parameters.MaxAddress") or
                contains("parameters.SubnetMask") or
                contains("parameters.Interface") or
                contains("parameters.LeaseTime") or
                contains("parameters.Order") or
                contains("parameters.Enable") or
                contains("parameters.DomainName"))';

    on event "dm:object-changed" call dhcp_server_pool_filter_changed
        filter 'path matches "^DHCPv4Server\.Pool\.[0-9]+\.$" and
            (contains("parameters.VendorClassID") or
            contains("parameters.VendorClassIDExclude") or
            contains("parameters.VendorClassIDMode") or
            contains("parameters.Chaddr") or
            contains("parameters.ChaddrMask") or
            contains("parameters.ChaddrExclude") or
            contains("parameters.Tag") or
            contains("parameters.AllowedDevices"))';

    on event "dm:instance-removed" call dhcp_server_pool_known_client_changed
        filter 'path matches "^DHCPv4Server\.Pool\.[0-9]+\.StaticAddress\.$" or
                path matches "^DHCPv4Server\.Pool\.[0-9]+\.Client\.$"';

    on event "dm:instance-added" call dhcp_server_pool_known_client_changed
        filter 'path matches "^DHCPv4Server\.Pool\.[0-9]+\.StaticAddress\.$" or
                path matches "^DHCPv4Server\.Pool\.[0-9]+\.Client\.$"';

    on event "dm:object-changed" call dhcp_server_pool_known_client_changed
        filter 'path matches "^DHCPv4Server\.Pool\.[0-9]+\.StaticAddress\.[0-9]+\.$" and
                (contains("parameters.Yiaddr") or
                contains("parameters.Chaddr") or
                contains("parameters.Enable"))';

    on event "dm:object-changed" call dhcp_server_pool_winsserver_changed
        filter 'path matches "^DHCPv4Server\.Pool\.[0-9]+\.$" and
            contains("parameters.${prefix_}WINSServer")';

    // Client events

    on event "dm:object-changed" call dhcp_server_client_active
        filter 'path matches "^DHCPv4Server\.Pool\.[0-9]+\.Client\.[0-9]+\.$" and
                contains("parameters.IPv4AddressNumberOfEntries")';

    on event "dm:object-changed" call dhcps_client_option_hostname_active_changed
        filter 'path matches "^DHCPv4Server\.Pool\.[0-9]+\.Client\.[0-9]+\.$" and
            contains("parameters.Active")';

    on event "dm:instance-added" call dhcp_client_check_if_static
        filter 'path matches "^DHCPv4Server\.Pool\.[0-9]+\.Client\.[0-9]+\.IPv4Address\.$"';

    on event "dm:instance-added" call dhcps_client_option_hostname_ipaddress_added
        filter 'path matches "^DHCPv4Server\.Pool\.[0-9]+\.Client\.[0-9]+\.IPv4Address\.$"';

    on event "dm:instance-removed" call dhcps_client_option_hostname_ipaddress_removed
        filter 'path matches "^DHCPv4Server\.Pool\.[0-9]+\.Client\.[0-9]+\.IPv4Address\.$"';

    on event "dm:object-changed" call dhcps_client_option_hostname_ipaddress_changed
        filter 'path matches "^DHCPv4Server\.Pool\.[0-9]+\.Client\.[0-9]+\.IPv4Address\.[0-9]+\.$" and
            contains("parameters.IPAddress")';

    on event "dm:object-changed" call dhcps_client_ipaddress_changed
        filter 'path matches "^DHCPv4Server\.Pool\.[0-9]+\.Client\.[0-9]+\.IPv4Address\.[0-9]+\.$" and
            contains("parameters.IPAddress")';

    on event "dm:instance-added" call dhcps_client_option_hostname_added
        filter 'path matches "^DHCPv4Server\.Pool\.[0-9]+\.Client\.[0-9]+\.Option\.$"
                && parameters.Tag == 12';

    on event "dm:object-changed" call dhcps_client_option_changed
        filter 'path matches "^DHCPv4Server\.Pool\.[0-9]+\.Client\.[0-9]+\.Option\.[0-9]+\.$"
                && contains("parameters.Value")';

    // Static address events

    on event "dm:instance-added" call dhcp_server_static_address_added
        filter 'path matches "^DHCPv4Server\.Pool\.[0-9]+\.StaticAddress\.$"';

    on event "dm:instance-removed" call dhcp_server_static_address_removed
        filter 'path matches "^DHCPv4Server\.Pool\.[0-9]+\.StaticAddress\.$"';

    on event "dm:instance-removed" call dhcp_removed_static
        filter 'path matches "^DHCPv4Server\.Pool\.[0-9]+\.StaticAddress\.$"';

    on event "dm:object-changed" call dhcp_server_static_address_changed
        filter 'path matches "^DHCPv4Server\.Pool\.[0-9]+\.StaticAddress\.[0-9]+\.$" and
                (contains("parameters.Yiaddr") or
                contains("parameters.Chaddr") or
                contains("parameters.Enable"))';

    on event "dm:object-changed" call dhcp_change_static
        filter 'path matches "^DHCPv4Server\.Pool\.[0-9]+\.StaticAddress\.[0-9]+\.$" and
            (contains("parameters.Enable") or
            contains("parameters.Yiaddr") or
            contains("parameters.Chaddr"))';

    // Pool option events

    on event "dm:instance-removed" call dhcp_option_add_or_remove
        filter 'path matches "^DHCPv4Server\.Pool\.[0-9]+\.Option\.$"';

    on event "dm:instance-added" call dhcp_option_add_or_remove
        filter 'path matches "^DHCPv4Server\.Pool\.[0-9]+\.Option\.$"';

    on event "dm:object-changed" call dhcp_option_changed
        filter 'path matches "^DHCPv4Server\.Pool\.[0-9]+\.Option\.[0-9]+\.$"';
}
