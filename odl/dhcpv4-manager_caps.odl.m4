%config {
    %global privileges = {
        user = "USER_ID", group = "GROUP_ID", capabilities = ["CAP_KILL", "CAP_NET_BIND_SERVICE", "CAP_NET_RAW", "CAP_NET_ADMIN", "CAP_DAC_OVERRIDE", "CAP_SETFCAP"]
    };
}