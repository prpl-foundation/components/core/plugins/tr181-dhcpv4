# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v2.12.21 - 2024-12-16(14:48:30 +0000)

## Release v2.12.20 - 2024-12-16(09:38:41 +0000)

### Other

- [TR181-DHCPv4Server] Server is slow because it waits for the Time plugin

## Release v2.12.19 - 2024-12-06(13:28:48 +0000)

### Other

- [dhcpv4server] Handling of dhcp option 43

## Release v2.12.18 - 2024-11-08(14:04:10 +0000)

### Other

- Reduce regex usage in signal handling

## Release v2.12.17 - 2024-10-25(12:14:24 +0000)

### Other

- - [dhcpv4-server] make dhcpv4 server work with mod-dmext
- - [dhcpv4-server] make dhcpv4 server work with mod-dmext

## Release v2.12.16 - 2024-09-19(07:50:38 +0000)

### Other

- - [DHCPv4Server] Configure a static IP address for a device already connected: IsStatic is not updated to 1

## Release v2.12.15 - 2024-09-10(07:25:21 +0000)

### Other

- [Security][AppArmor] Apparmor must be available on PRPL builds

## Release v2.12.14 - 2024-07-11(12:23:21 +0000)

### Other

- - DHCPv4 server] only one lease should be visible in the DM
- - [dhcpv4-server] wrong IPAddress under DNS.X_PRPL-COM_Host. (after modifying the subnet)

## Release v2.12.13 - 2024-07-08(06:56:26 +0000)

## Release v2.12.12 - 2024-06-26(07:39:27 +0000)

### Other

- - [tr181-dhcpv4-server] when an inactive client becomes active, new tags are not added

## Release v2.12.11 - 2024-06-18(09:36:45 +0000)

### Other

- DomainName should be supported and should also be used for DNS suffix

## Release v2.12.10 - 2024-06-17(10:07:29 +0000)

### Other

- - amx plugin should not run as root user

## Release v2.12.9 - 2024-06-03(12:03:30 +0000)

### Fixes

- dhcpv4-manager crash enabling an empty static LAN IP instance

## Release v2.12.8 - 2024-05-23(06:32:24 +0000)

### Fixes

- [dhcpv4-server] restart back-end when crashed

## Release v2.12.7 - 2024-05-22(21:09:15 +0000)

### Fixes

- [DHCPv4 server] Unable to add static MAC address with different case than client MAC address

## Release v2.12.6 - 2024-05-22(07:41:31 +0000)

### Other

- add missing runtime dependencies

## Release v2.12.5 - 2024-04-29(08:32:01 +0000)

### Other

- [dnsmasq] Put TV devices in the .100-.150 DHCP range

## Release v2.12.4 - 2024-04-10(10:09:23 +0000)

### Changes

- Make amxb timeouts configurable

## Release v2.12.3 - 2024-03-28(11:37:59 +0000)

### Fixes

- [DHCPv4Server] StaticAddress doesn't work after being changed

## Release v2.12.2 - 2024-03-27(14:35:55 +0000)

### Fixes

-  [DHCPv4Server] Client disappears if time updates

## Release v2.12.1 - 2024-03-22(08:49:22 +0000)

### Fixes

- [DHCPv4Server] Unable to make an existing DHCP lease static

## Release v2.12.0 - 2024-03-19(17:48:37 +0000)

### New

- CLONE - [Data model] Add (X_PRPL-COM_)WINSServer parameter in DHCPv4 Server Pool: part 2, functional part

## Release v2.11.2 - 2024-03-19(12:31:30 +0000)

### Fixes

- Overlapping pool for firewall services

## Release v2.11.1 - 2024-03-18(14:41:14 +0000)

### Other

- Rework TR-181 interface stacks indexing number

## Release v2.11.0 - 2024-02-05(16:22:20 +0000)

### New

- Add tr181-device proxy odl files to components

## Release v2.10.2 - 2024-01-23(14:02:50 +0000)

### Fixes

- [PCM] The BackupStatus takes the value "PartialSuccess" instead of "Success"

## Release v2.10.1 - 2024-01-23(10:41:48 +0000)

### Fixes

- CLONE - The creation of a static lease assigns an address that is currently unavailable

## Release v2.10.0 - 2024-01-17(09:33:58 +0000)

### New

- Add tr181-device proxy odl files to components

## Release v2.9.1 - 2024-01-10(16:16:00 +0000)

### Fixes

- [DHCPv4 Server] Scoop of static ip is global and should be local Dev static local

## Release v2.9.0 - 2024-01-09(15:12:54 +0000)

### New

- CLONE - [Data model] Add (X_PRPL-COM_)WINSServer parameter in DHCPv4 Server Pool: part 2, functional part

## Release v2.8.4 - 2023-12-18(09:16:21 +0000)

### Fixes

- handel different reasons in validate function

## Release v2.8.3 - 2023-12-13(16:29:42 +0000)

### Fixes

- [SAFRAN PRPL] [DHCP] Weird behavior after upgarde

## Release v2.8.2 - 2023-12-11(12:15:32 +0000)

### Fixes

- [DHCPv4 Server] Rework of unit test and test new features

## Release v2.8.1 - 2023-12-06(15:33:15 +0000)

### Fixes

- Revert "- LAN Static dhcpv4 configuration is not upgrade persistent"

## Release v2.8.0 - 2023-12-06(15:10:06 +0000)

### New

- CLONE - [Data model] Add (X_PRPL-COM_)WINSServer parameter in DHCPv4 Server Pool: part 1, provide DataModel

## Release v2.7.1 - 2023-12-06(13:42:22 +0000)

### Fixes

- - LAN Static dhcpv4 configuration is not upgrade persistent

## Release v2.7.0 - 2023-11-30(10:59:32 +0000)

### New

- [PCM] Upgrade Persistent support for Plugins and data models

## Release v2.6.0 - 2023-11-24(14:42:33 +0000)

### New

- [DHCPv4][PCM] Device leases MUST be upgrade persistent

## Release v2.5.1 - 2023-11-23(08:16:32 +0000)

### Fixes

- [DHCPv4Server] Oder parameter increase is breaking if go over range before comp

## Release v2.5.0 - 2023-11-15(14:01:44 +0000)

### New

- Make static leases having default Yiaddr

## Release v2.4.0 - 2023-11-15(10:09:52 +0000)

### New

- Implement the AllowDevices parameter

## Release v2.3.1 - 2023-10-28(07:36:47 +0000)

### Fixes

- [DHCPv4] Don't skip options with a Value

## Release v2.3.0 - 2023-10-28(07:11:48 +0000)

### New

- CLONE - Add support for Device.­DHCPv­4.­Server.­Pool.­{i}.Order

## Release v2.2.1 - 2023-10-26(14:20:37 +0000)

### Fixes

- [DHCPv4] Create dnsmasq config dir before writing config files

## Release v2.2.0 - 2023-10-20(13:01:09 +0000)

### New

- [dhcpv4 server] not supported config needed

## Release v2.1.2 - 2023-10-13(14:02:55 +0000)

### Changes

-  All applications using sahtrace logs should use default log levels

## Release v2.1.1 - 2023-10-11(12:35:57 +0000)

### Fixes

- [dhcpv4-manager] Crash on dhcpv4-manager when set alternative and then get default range ipv4

## Release v2.1.0 - 2023-10-09(10:51:17 +0000)

### New

- [dhcpv4] server need to support MAC filtering

### Fixes

- [dnsmasq] Devices with extenders tag should be given leases in increasing order

## Release v2.0.3 - 2023-09-28(07:56:26 +0000)

### Other

- [DHCPv4 Server] extend DM to support non master tag [add]

## Release v2.0.2 - 2023-09-20(09:11:38 +0000)

### Other

- [DHCPv4 server] should support VendorClassID option [add]

## Release v2.0.1 - 2023-09-09(06:40:32 +0000)

### Fixes

- [tr181-pcm] All the plugins that are register to PCM should set the pcm_svc_config

## Release v2.0.0 - 2023-09-07(10:18:16 +0000)

### Removed

- Move the force dora cycle functionality

## Release v1.17.4 - 2023-09-05(14:47:57 +0000)

### Fixes

- [tr181-dhcprelay]"dnsmasq" Not Reacting as Expected with Disabled DHCPv4Server or Enabled DHCPv4Relay

## Release v1.17.3 - 2023-08-22(12:12:40 +0000)

### Fixes

- interface is down after boot

## Release v1.17.2 - 2023-08-02(11:04:36 +0000)

### Fixes

- [DHCPv4] Force DORA: Status ends up Error_Misconfigured if one interface cannot be toggled

## Release v1.17.1 - 2023-08-02(07:04:48 +0000)

### Fixes

- [DHCPv4] Force DORA on WiFi: VAP should be toggled instead of radios

## Release v1.17.0 - 2023-07-26(11:48:47 +0000)

### New

- [tr181-pcm] Set the usersetting parameters for each plugin

## Release v1.16.0 - 2023-07-14(09:30:39 +0000)

### New

- [tr181-logical][LAN mib] The Lan mib must expose the domainSearchList parameter

## Release v1.15.0 - 2023-06-29(15:57:41 +0000)

### New

- [TR181-DHCPv4Server] Add IsStatic for easy tracking of what is gone

## Release v1.14.0 - 2023-06-27(07:48:30 +0000)

### New

- [dhcpv4-manager] Implementation of option 125 ( specific vendor option ) parsing for dnsmasq module

## Release v1.13.2 - 2023-06-20(07:45:50 +0000)

### Fixes

- [dhcpv4 server] dnsmasq is not stopped when switching DHCPv4Server to disable

## Release v1.13.1 - 2023-06-17(06:42:32 +0000)

### Fixes

- [TR181-DHCPv4Server] When updating the DHCPv4 pool range, existing client entries are not invalidated

## Release v1.13.0 - 2023-06-15(10:56:23 +0000)

### New

- add dhcpv4 option 125

## Release v1.12.0 - 2023-06-15(10:27:09 +0000)

### New

- [tr181-pcm] Set the usersetting parameters for each plugin

## Release v1.11.0 - 2023-06-07(08:21:18 +0000)

### New

- add ACLs permissions for cwmp user

## Release v1.10.5 - 2023-05-23(15:17:47 +0000)

## Release v1.10.4 - 2023-05-11(09:01:27 +0000)

### Other

- [Coverage] Remove SAHTRACE defines in order to increase branching coverage

## Release v1.10.3 - 2023-04-20(13:12:07 +0000)

### Fixes

- service protocol should be integers

## Release v1.10.2 - 2023-04-12(07:50:11 +0000)

### Fixes

- [dhcpv4-manager] changing the lease time break dnsmasq subscription

## Release v1.10.1 - 2023-03-21(14:11:32 +0000)

### Fixes

- [DHCPv4 Server] The DUT ignores the Static Leases records

### Other

- fix missing execvp null pointer array termination [EXTERNAL]

## Release v1.10.0 - 2023-03-13(15:51:56 +0000)

### Fixes

- [DHCPv4] Split client and server plugin

## Release v1.9.3 - 2023-03-09(12:03:29 +0000)

### Other

- tr181-components: missing explicit dependency on rpcd service providing ubus uci backend
- [Config] enable configurable coredump generation

## Release v1.9.2 - 2023-02-09(09:14:07 +0000)

### Fixes

- [dhcpv4-manager] lease is not added to DM if given between start and subscribe

## Release v1.9.1 - 2023-01-19(13:52:01 +0000)

### Other

- [CI] Add missing dependency on libdhcpoptions

## Release v1.9.0 - 2023-01-18(16:18:58 +0000)

### New

- update DNS data model with hosts from DHCP server pool

## Release v1.8.6 - 2023-01-09(10:16:08 +0000)

### Fixes

- avoidable copies of strings, htables and lists

## Release v1.8.5 - 2022-12-22(16:40:22 +0000)

### Fixes

- [dhcpv4-manager] prevent error in bit endianness

## Release v1.8.4 - 2022-12-09(09:20:21 +0000)

### Fixes

- [Config] coredump generation should be configurable

## Release v1.8.3 - 2022-11-29(15:03:50 +0000)

### Fixes

- [DHCPv4 Serever] Only check Enable for the open and close of the firewall

## Release v1.8.2 - 2022-11-23(11:02:37 +0000)

### Fixes

- [DHCPv4 server] unable to retrieve dhcp address on lla config

## Release v1.8.1 - 2022-11-07(12:28:34 +0000)

### Fixes

- [DHCPv4 manager] Unit test rework after modification

## Release v1.8.0 - 2022-10-20(13:30:20 +0000)

### New

- [DHCPv4s] creation of module to configure dnsmasq without uci for the lla config

## Release v1.7.0 - 2022-10-06(10:52:09 +0000)

### New

- [DHCPv4 Server] Use separtion of concern for uci and dnsmasq

## Release v1.6.15 - 2022-09-27(13:21:30 +0000)

### Fixes

- [DHCPv4-manager] uci config does not update if not already present in the dhcp config file

## Release v1.6.14 - 2022-09-14(15:03:15 +0000)

### Fixes

- [DHCPv4Server] use of query in priv data

## Release v1.6.13 - 2022-09-08(07:41:16 +0000)

### Fixes

- [DHCPv4 manager] Do not use uci file for default configuration

## Release v1.6.12 - 2022-07-29(07:37:45 +0000)

### Fixes

- dhcpv servers fails to create cpe-dhcpvXs-* firewall services on firstboot

## Release v1.6.11 - 2022-07-28(12:06:10 +0000)

### Fixes

- DHCP v4/v6 managers started alongside clients

## Release v1.6.10 - 2022-07-26(07:54:08 +0000)

### Fixes

- dhcp rule missing

## Release v1.6.9 - 2022-07-11(09:42:58 +0000)

### Fixes

- CI code check warning: use after free

## Release v1.6.8 - 2022-07-11(09:31:06 +0000)

### Fixes

- static ip address not working

## Release v1.6.7 - 2022-06-23(08:41:15 +0000)

### Fixes

- [tr181-dhcpv4client] fials to load datamodel at startup

## Release v1.6.6 - 2022-06-02(06:42:02 +0000)

### Fixes

- [DISH US HGW][DHCPv4] LAN Client does not get IPv4 address (after hard reset)

## Release v1.6.5 - 2022-05-23(19:51:05 +0000)

### Other

- [TR181-DHCPv4] Definition of Upgrade Persistent DHCPv4 Server Configuration

## Release v1.6.4 - 2022-05-17(13:54:27 +0000)

### Fixes

- Issue: hop-1435 [DHCPv4Server] Missing libnetmodel in BUILD_DEPS

## Release v1.6.3 - 2022-05-05(14:29:23 +0000)

### Other

- [TR181][DHCPv4S] Add NetModel Query + Unit Tests

## Release v1.6.2 - 2022-03-24(11:08:08 +0000)

### Changes

- [GetDebugInformation] Add data model debuginfo in component services

## Release v1.6.1 - 2022-03-15(18:32:26 +0000)

### Other

- [DHCPv4Server] Improve logging

## Release v1.6.0 - 2022-03-03(12:45:17 +0000)

### New

- [CONFIG] add lcm interface to config

## Release v1.5.7 - 2022-02-25(11:56:02 +0000)

### Other

- Enable core dumps by default

## Release v1.5.6 - 2022-02-24(15:29:43 +0000)

### Fixes

- - component crashes at startup

## Release v1.5.5 - 2021-12-13(09:54:32 +0000)

### Fixes

- notify status change before triggering dnsmasq

## Release v1.5.4 - 2021-12-01(21:18:43 +0000)

### Fixes

- Fix tests subscription handling

## Release v1.5.3 - 2021-11-25(11:47:59 +0000)

### Fixes

- increase dnsmasq timeout and make it configurable

## Release v1.5.2 - 2021-11-10(07:45:16 +0000)

### Fixes

- fix type of ChaddrExclude
- fix IP range check

## Release v1.5.1 - 2021-11-05(07:31:23 +0000)

### Fixes

- fix modifying Active after applying read-only

## Release v1.5.0 - 2021-11-04(11:47:54 +0000)

### New

- [tr181-dhcpv4] add guest config

## Release v1.4.6 - 2021-11-02(13:40:20 +0000)

### Fixes

- fix applying read only on Status

## Release v1.4.5 - 2021-11-02(13:05:24 +0000)

### Changes

- firewall: include in tests, limit max instances

## Release v1.4.4 - 2021-10-25(12:51:45 +0000)

### Fixes

-  use common datamodel with DHCPv4 client

## Release v1.4.3 - 2021-10-25(07:46:45 +0000)

### Fixes

- update objects with read-only

## Release v1.4.2 - 2021-10-07(17:10:26 +0000)

### Fixes

- emit events on Status change

## Release v1.4.1 - 2021-10-01(13:34:06 +0000)

### Changes

- modify Interface format, integrate with firewall

## Release v1.4.0 - 2021-09-10(11:15:34 +0000)

### New

- Do not exit the plugin if dnsmasq is not found

## Release v1.3.2 - 2021-09-09(06:31:27 +0000)

### Fixes

- fix segfault on non existing IP.Interface object

## Release v1.3.1 - 2021-09-07(14:25:58 +0000)

### Fixes

- fix protovalue being NULL

## Release v1.3.0 - 2021-09-07(13:45:19 +0000)

### New

- Implement handling of "Interface" parameter to integrate with other plugins

## Release v1.2.4 - 2021-08-26(14:52:49 +0000)

### Changes

- add defaults directory

## Release v1.2.3 - 2021-08-25(15:14:55 +0000)

### Fixes

- Avoid spurious lease events, keep only the most recent

## Release v1.2.2 - 2021-08-25(14:19:20 +0000)

### Fixes

- fix status displayed in Pool, LeaseTime

### Other

- update license

## Release v1.2.1 - 2021-08-20(08:23:08 +0000)

### Fixes

- fix amxd_object_for_each nesting shadowing
- fix amxd_object_for_each nesting shadowing

## Release v1.2.0 - 2021-08-12(09:52:55 +0000)

### New

- add debian package generation

### Fixes

- staticip: fix staticip_remove

## Release v1.1.1 - 2021-08-06(12:51:02 +0000)

### Changes

- [Startup Order] modify initialization priority

## Release v1.1.0 - 2021-08-06(07:46:03 +0000)

### New

- Server internal eventing

### Other

- Correct CHANGELOG.md formatting

## Release v1.0.7 - 2021-07-27(12:16:06 +0000)

### Changes

- implement client options
- Added support for client options in the datamodel

## Release v1.0.6 - 2021-07-26(07:57:39 +0000)

### Changes

- event fixes
- Do not look for 'options_ack' in dnsmasq events
- Release IPs without network pool label information.

## Release v1.0.5 - 2021-07-17

### Changes

- Fix to compile on WNC board

## Release v1.0.4 - 2021-07-01(09:35:09 +0000)

### Changes

- several basic functionality fixes
- Ability to start/stop dnsmasq from the datamodel.
- Report errors when configuring wrong DHCP Pools.
- Being able to add a lease without networkid label (stored in cache on dnsmasq restart).
- Fix unsynchronization with dnsmasq (amxb_ubus >= 2.0.22).

## Release v1.0.3 - 2021-06-28(15:36:31 +0000)

### New

- introduce Pool StaticAddress support
- Introduced Pool StaticAddress support
- This will make the dhcpserver reserve a static lease for a deisgned MAC address.
- When disabled, the IP address will still be asigned to a reserved MAC address, so it is not available in the Pool.
- Fix Pool deletion: generate an UCI compatible alias on the same conditions as UCI Pool export.

## Release v1.0.2 - 2021-06-21(09:01:14 +0000)

### Changes

- fix output not long enough
- Size of string destination of int32_t plus unit character could be truncated.

## Release v1.0.1 - 2021-06-17(11:14:59 +0000)

### New

- forward datamodel changes to UCI
- React to new Pool instances created, modified or removed.
- This should forward the necessary change to the ubus UCI service.
- Generation of the datamodel documentation.
- Alias names that contain characters not usable in UCI need to be translated into an auxiliary name to make a mapping.

## Release v1.0.0 - 2021-06-09(14:10:47 +0000)

### New

- Initial import
- DHCPv4.Server datamodel
- Basic configuration of DHCPv4.Server.Pool.\{i\} is imported from UCI service in ubus at startup.
- DHCPv4.Server.Pool.\{i\}.Client.\{i\}.IPv4Address.\{i\} are created both reading from ubus dnsmasq.leases and dnsmasq lease events.
- Changes to DHCPv4.Server.Pool.\{i\} that are understood are stored back to UCI.
- Automated tests to cover most of the code functionality.
