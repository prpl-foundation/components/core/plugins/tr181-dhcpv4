/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include <amxc/amxc_macros.h>

#include "mock_amxb_lib_dnsmasq.h"
#include "common_lib_dnsmasq.h"

// internal variable
static amxb_state_t amxb_obj;

// Function
// helper
static void dummy_fct(UNUSED const char* const sig_name,
                      UNUSED const amxc_var_t* const data,
                      UNUSED void* const priv) {
    return;
}

// wrapper
int __wrap_amxb_subscribe(UNUSED amxb_bus_ctx_t* const bus_ctx,
                          const char* object,
                          const char* expression,
                          amxp_slot_fn_t slot_cb,
                          UNUSED void* priv) {
    int retval = 1;
    assert_non_null(object);
    assert_non_null(expression);
    assert_non_null(slot_cb);

    if(amxb_obj.sub_cb == dummy_fct) {
        amxb_obj.sub_cb = slot_cb;
        retval = 0;
    }
    return retval;
}

int __wrap_amxb_unsubscribe(amxb_bus_ctx_t* const bus_ctx,
                            const char* object,
                            amxp_slot_fn_t slot_cb,
                            UNUSED void* priv) {
    int retval = 1;
    assert_non_null(bus_ctx);
    assert_false(bus_ctx != amxb_obj.bus);
    assert_non_null(object);
    assert_non_null(slot_cb);

    if(amxb_obj.sub_cb != dummy_fct) {
        amxb_obj.sub_cb = dummy_fct;
        retval = 0;
    }
    return retval;
}

amxb_bus_ctx_t* __wrap_amxb_be_who_has(char* component) {
    assert_non_null(component);
    if(amxb_obj.skip_pres_bus == 0) {
        return amxb_obj.bus;
    } else {
        amxb_obj.skip_pres_bus--;
        return NULL;
    }
}

int __wrap_amxb_call(amxb_bus_ctx_t* const bus_ctx,
                     const char* object,
                     const char* method,
                     amxc_var_t* args,
                     amxc_var_t* ret,
                     UNUSED int timeout) {
    int retval = 1;
    amxc_var_t* lease_list = NULL;
    amxc_var_t* leases = NULL;
    amxc_var_t* lease = NULL;

    assert_non_null(bus_ctx);
    assert_false(bus_ctx != amxb_obj.bus);
    assert_non_null(object);
    assert_non_null(method);
    assert_non_null(args);
    assert_non_null(ret);

    amxc_var_set_type(ret, AMXC_VAR_ID_LIST);
    lease_list = amxc_var_add(amxc_htable_t, ret, NULL);
    leases = amxc_var_add_key(amxc_llist_t, lease_list, "leases", NULL);
    lease = amxc_var_add(amxc_htable_t, leases, NULL);

    creat_dnsmasq_event(lease, true);
    retval = 0;
    return retval;
}

// mock specifique
void mock_amxb_init(void) {
    amxb_obj.sub_cb = dummy_fct;
    amxb_obj.skip_pres_bus = 0;
    amxb_obj.bus = (amxb_bus_ctx_t*) malloc(sizeof(amxb_bus_ctx_t));
    return;
}

void mock_amxb_clean(void) {
    amxb_obj.sub_cb = dummy_fct;
    free(amxb_obj.bus);
    amxb_obj.bus = NULL;
    amxb_obj.skip_pres_bus = 0;
    return;
}

void mock_amxb_skip_bus_pres(int nb) {
    amxb_obj.skip_pres_bus = nb;
}