/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <setjmp.h>
#include <stdarg.h>
#include <stddef.h>
#include <cmocka.h>
#include <stdlib.h>
#include <stdio.h>


#include <amxc/amxc_macros.h>

#include "common_lib_dnsmasq.h"
#include "../../include_priv/dnsmasq_utils.h"

static amxd_dm_t dm;
static amxo_parser_t parser;

int test_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    handle_events();
    return 0;
}

int test_teardown(UNUSED void** state) {
    amxm_close_all();

    amxo_resolver_import_close_all();
    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    return 0;
}

void handle_events(void) {
    while(amxp_signal_read() == 0) {
    }
}

void creat_dnsmasq_event(amxc_var_t* data, bool ack_event) {
    if(ack_event) {
        amxc_var_t* options_list = NULL;
        amxc_var_t* option = NULL;
        amxc_var_add_key(cstring_t, data, "notification", DHCPACK);
        amxc_var_add_key(cstring_t, data, "mac", "48:9e:bd:2d:66:93");
        amxc_var_add_key(cstring_t, data, "hwaddr", "489EBD2D6693");
        amxc_var_add_key(cstring_t, data, "ip", "192.168.1.140");
        amxc_var_add_key(cstring_t, data, "net", "dummy_master");
        amxc_var_add_key(cstring_t, data, "ipv4", "192.168.1.140");
        amxc_var_add_key(cstring_t, data, "FQDN", "test.dummy");
        amxc_var_add_key(cstring_t, data, "expires", "2022-10-19T00:02:58Z");
        options_list = amxc_var_add_key(amxc_llist_t, data, "options_request", NULL);
        option = amxc_var_add(amxc_htable_t, options_list, NULL);
        amxc_var_add_key(uint32_t, option, "tag", 42);
        amxc_var_add_key(cstring_t, option, "raw", "Hello 1");
        option = amxc_var_add(amxc_htable_t, options_list, NULL);
        amxc_var_add_key(uint32_t, option, "tag", 24);
        amxc_var_add_key(cstring_t, option, "raw", "Hello 2");
    } else {
        amxc_var_add_key(cstring_t, data, "notification", DHCPRELEASE);
        amxc_var_add_key(cstring_t, data, "mac", "48:9e:bd:2d:66:93");
        amxc_var_add_key(cstring_t, data, "ip", "192.168.1.140");
        amxc_var_add_key(cstring_t, data, "net", "dummy_master");
    }
    return;
}

void add_lease_cb(amxc_var_t* lease) {
    amxc_var_t* options_list = NULL;
    assert_non_null(lease);
    assert_string_equal(GET_CHAR(lease, "Alias"), "dummy");
    assert_string_equal(GET_CHAR(lease, "Chaddr"), "48:9e:bd:2d:66:93");
    assert_string_equal(GET_CHAR(lease, "IPv4Address"), "192.168.1.140");
    assert_non_null(GET_ARG(lease, "LeaseTimeRemaining"));
    options_list = GET_ARG(lease, "Option");
    amxc_var_for_each(option, options_list) {
        const char* raw = GET_CHAR(option, NULL);
        switch(atoi(amxc_var_key(option))) {
        case 42:
            assert_string_equal(raw, "Hello 1");
            break;
        case 24:
            assert_string_equal(raw, "Hello 2");
            break;
        default:
            assert_false(true);
        }
    }
    return;
}

void delete_lease_cb(amxc_var_t* lease) {
    assert_non_null(lease);
    assert_string_equal(GET_CHAR(lease, "Alias"), "dummy");
    assert_string_equal(GET_CHAR(lease, "Chaddr"), "48:9e:bd:2d:66:93");
    assert_string_equal(GET_CHAR(lease, "IPv4Address"), "192.168.1.140");
    return;
}

