/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>

#include <amxc/amxc_macros.h>

#include "../common_lib_dnsmasq/common_lib_dnsmasq.h"
#include "../common_lib_dnsmasq/mock_amxb_lib_dnsmasq.h"
#include "../common_lib_dnsmasq/mock_amxp_lib_dnsmasq.h"
#include "test_library.h"

#include "dnsmasq/common_api.h"
#include "dnsmasq_utils.h"

void test_subscribe(UNUSED void** state) {
    int ret = 1;

    dnsmasq_init(add_lease_cb, delete_lease_cb);
    mock_amxb_init();

    mock_amxb_skip_bus_pres(0);

    ret = dnsmasq_subscribe();

    assert_int_equal(ret, 0);

    ret = dnsmasq_unsubscribe();

    assert_int_equal(ret, 0);

    mock_amxb_skip_bus_pres(1);

    ret = dnsmasq_subscribe();

    assert_int_equal(ret, 0);

    ret = dnsmasq_unsubscribe();

    assert_int_equal(ret, 0);

    dnsmasq_clean();
    mock_amxb_clean();
}

void test_dnsmasq_event(UNUSED void** state) {
    amxc_var_t data;

    amxc_var_init(&data);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);

    dnsmasq_init(add_lease_cb, delete_lease_cb);
    mock_amxb_init();

    creat_dnsmasq_event(&data, true);
    dhcp_dnsmasq_lease_event(NULL, &data, NULL);

    amxc_var_clean(&data);
    amxc_var_init(&data);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);

    creat_dnsmasq_event(&data, false);
    dhcp_dnsmasq_lease_event(NULL, &data, NULL);

    amxc_var_clean(&data);
    dnsmasq_clean();
    mock_amxb_clean();
}

void test_leases(UNUSED void** state) {
    int retval = 1;
    dnsmasq_init(add_lease_cb, delete_lease_cb);
    mock_amxb_init();

    retval = dnsmasq_add_all_leases("lan");
    assert_int_equal(retval, 0);

    retval = dnsmasq_add_all_leases("dummy");
    assert_int_equal(retval, 0);

    mock_amxb_clean();
    dnsmasq_clean();
}
