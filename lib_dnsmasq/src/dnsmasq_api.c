/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>
#include <ctype.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_types.h>
#include <amxb/amxb.h>
#include <amxm/amxm.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "dnsmasq_utils.h"

struct _dnsmasq dnsmasq;
static const char* expression = "notification in ['dhcp.ack', 'dhcp.release']";

/**
 * @brief
 * Call back function to subscribe to ack and release event of dnsmasq on ubus.
 *
 * @param signame name of the signal received.
 * @param data data from the signal.
 * @param priv data passed by the creator of the call back.
 */
static void dnsmasq_subscribe_cb(UNUSED const char* signame,
                                 UNUSED const amxc_var_t* const data,
                                 UNUSED void* const priv) {
    SAH_TRACEZ_INFO(ME, "dnsmasq subscription call back called");

    if(!dnsmasq.sub_ok) {
        int r = AMXB_ERROR_UNKNOWN;
        amxb_bus_ctx_t* ctx = amxb_be_who_has("dnsmasq");
        when_null_trace(ctx, exit, ERROR, "No dnsmasq bus context found");

        r = amxb_subscribe(ctx, "dnsmasq.", expression, dhcp_dnsmasq_lease_event, NULL);
        when_failed_trace(r, exit, ERROR, "Problem subscribing to dnsmasq events %d", r);
        dnsmasq.sub_ok = true;
    }
exit:
    return;
}

/**
 * @brief
 * Initialise the variables use the dnsmasq library.
 *
 * @param handler_add The call back function to add a lease.
 * @param handler_delete The call back function to remove a lease.
 */
void dnsmasq_init(dnsmasq_callback_t handler_add, dnsmasq_callback_t handler_delete) {
    dnsmasq.add_lease = handler_add;
    dnsmasq.delete_lease = handler_delete;
    dnsmasq.add_ok = true;
    dnsmasq.del_ok = true;
    dnsmasq.sub_ok = false;
    dnsmasq.slot_ok = false;
}

/**
 * @brief
 * Clean all variables used by the dnsmasq library.
 */
void dnsmasq_clean(void) {
    dnsmasq.add_lease = NULL;
    dnsmasq.delete_lease = NULL;
    dnsmasq.add_ok = false;
    dnsmasq.del_ok = false;
    dnsmasq.sub_ok = false;
    dnsmasq.slot_ok = false;
    amxp_slot_disconnect_all(dnsmasq_subscribe_cb);
}

/**
 * @brief
 * Subscribe to dnsmasq ack and release events on the ubus or create a call back if dnsmasq is not present on ubus.
 *
 * @return int 0 on success
 */
int dnsmasq_subscribe(void) {
    int r = AMXB_ERROR_UNKNOWN;

    if(!dnsmasq.sub_ok) {
        amxb_bus_ctx_t* ctx = amxb_be_who_has("dnsmasq.");
        r = amxb_subscribe(ctx, "dnsmasq.", expression, dhcp_dnsmasq_lease_event, NULL);
        if(r != AMXB_STATUS_OK) {
            SAH_TRACEZ_INFO(ME, "Cannot yet subscribe to dnsmasq");
            if(!dnsmasq.slot_ok) {
                r = amxp_slot_connect_filtered(NULL, "^wait:dnsmasq\\.$", NULL, dnsmasq_subscribe_cb, NULL);
                when_failed_trace(r, exit, ERROR, "Failed to wait for dnsmasq to be available");
                dnsmasq.slot_ok = true;
            }
            amxb_wait_for_object("dnsmasq.");
        } else {
            dnsmasq.sub_ok = true;
        }
    }
exit:
    return r;
}

/**
 * @brief
 * Unsubcribe from dnsmasq on ubus.
 *
 * @return int 0 on success
 */
int dnsmasq_unsubscribe(void) {
    amxb_bus_ctx_t* ctx = amxb_be_who_has("dnsmasq.");
    int r = 1;

    when_null_trace(ctx, exit, WARNING, "dnsmasq not on the bus");
    r = amxb_unsubscribe(ctx, "dnsmasq.", dhcp_dnsmasq_lease_event, NULL);
    when_failed_trace(r, exit, WARNING, "Problem unsubscribing from dnsmasq events");
    dnsmasq.sub_ok = false;
exit:
    return r;
}

/**
 * @brief
 * Add all leases already given by dnsmasq, dnsmasq must be initialised and be register on ubus.
 *
 * @param alias The network alias to get the leases from.
 * @return int 0 on success.
 */
int dnsmasq_add_all_leases(char* alias) {
    int r = 1;
    amxb_bus_ctx_t* ctx = NULL;
    amxc_var_t* leases = NULL;
    amxc_var_t args;
    amxc_var_t rv;

    amxc_var_init(&rv);
    amxc_var_init(&args);

    when_null_trace(alias, exit, ERROR, "No alias given.");

    ctx = amxb_be_who_has("dnsmasq.");
    when_null_trace(ctx, exit, ERROR, "dnsmasq not on the bus.");

    r = amxb_call(ctx, "dnsmasq.", "leases", &args, &rv, 100);
    when_failed_trace(r, exit, ERROR, "Problem calling dnsmasq lease");

    leases = GETP_ARG(&rv, "0.leases");
    when_null_trace(leases, exit, INFO, "No dnsmasq leases found");
    amxc_var_for_each(lease, leases) {
        amxc_string_t fqdn;
        amxc_llist_t list_fqdn;
        amxc_string_t* str_part = NULL;
        const char* network = NULL;
        const char* hwaddr = NULL;
        const char* ipv4 = NULL;
        char mac_addr[18];

        amxc_string_init(&fqdn, 0);
        amxc_llist_init(&list_fqdn);

        amxc_string_set(&fqdn, GET_CHAR(lease, "FQDN"));
        amxc_string_split_to_llist(&fqdn, &list_fqdn, '.');

        str_part = amxc_string_from_llist_it(amxc_llist_get_last(&list_fqdn));
        when_null_trace(str_part, exit, ERROR, "No domain name given");
        network = amxc_string_get(str_part, 0);
        when_str_empty_trace(network, exit, ERROR, "Lease event is missing network");
        hwaddr = GET_CHAR(lease, "hwaddr");
        when_str_empty_trace(hwaddr, exit, ERROR, "Lease event is missing hwaddr");
        ipv4 = GET_CHAR(lease, "ipv4");
        when_str_empty_trace(ipv4, exit, ERROR, "Lease event is missing ipv4");

        sprintf(mac_addr, "%c%c:%c%c:%c%c:%c%c:%c%c:%c%c",
                tolower(hwaddr[0]), tolower(hwaddr[1]), tolower(hwaddr[2]), tolower(hwaddr[3]),
                tolower(hwaddr[4]), tolower(hwaddr[5]), tolower(hwaddr[6]), tolower(hwaddr[7]),
                tolower(hwaddr[8]), tolower(hwaddr[9]), tolower(hwaddr[10]), tolower(hwaddr[11]));


        if(strcmp(alias, network) == 0) {
            dnsmasq_send_lease(lease, network, mac_addr, ipv4);
        }
        amxc_string_clean(&fqdn);
        amxc_llist_clean(&list_fqdn, amxc_string_list_it_free);
    }
exit:
    amxc_var_clean(&args);
    amxc_var_clean(&rv);
    return r;
}
