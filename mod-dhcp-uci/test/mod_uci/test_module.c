/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <amxc/amxc_macros.h>

#include "../common_uci/common_uci.h"
#include "../common_uci/mock_amxb_uci.h"
#include "../common_uci/mock_amxm_uci.h"
#include "../common_uci/mock_amxp_uci.h"
#include "../common_uci/mock_lib_dnsmasq_uci.h"
#include "test_module.h"

#include "mod_dhcp_uci.h"

void test_can_load_module(UNUSED void** state) {
    amxm_shared_object_t* so = NULL;
    assert_int_equal(amxm_so_open(&so, "target_module", "../modules_under_test/target_module.so"), 0);
    handle_events();
}

void test_pool(UNUSED void** state) {
    int retval = 1;
    amxc_var_t data;
    amxc_var_t ret;

    amxc_var_init(&data);
    amxc_var_init(&ret);
    mock_amxb_init();

    creat_pool_data(&data, "192.168.1.100", "192.168.1.190");

    retval = enable_dhcp_pool_uci(NULL,
                                  &data,
                                  &ret);
    assert_int_equal(retval, 0);

    retval = disable_dhcp_pool_uci(NULL,
                                   &data,
                                   &ret);
    assert_int_equal(retval, 0);

    amxc_var_clean(&data);
    amxc_var_clean(&ret);
    mock_amxb_clean();
}

void test_static_ip(UNUSED void** state) {
    int retval = 1;
    amxc_var_t data;
    amxc_var_t ret;

    amxc_var_init(&data);
    amxc_var_init(&ret);
    mock_amxb_init();

    creat_static_ip_data(&data, "192.168.1.150", "192.168.1.100", "192.168.1.190");

    retval = enable_dhcp_static_ip_uci(NULL,
                                       &data,
                                       &ret);
    assert_int_equal(retval, 0);

    retval = disable_dhcp_static_ip_uci(NULL,
                                        &data,
                                        &ret);
    assert_int_equal(retval, 0);

    amxc_var_clean(&data);
    amxc_var_clean(&ret);
    mock_amxb_clean();
}

void test_callback(UNUSED void** state) {
    mock_lib_dnsmasq_send_lease();
    mock_lib_dnsmasq_remove_lease();
}
