/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <setjmp.h>
#include <stdarg.h>
#include <stddef.h>
#include <cmocka.h>
#include <stdlib.h>
#include <stdio.h>


#include <amxc/amxc_macros.h>

#include "common_uci.h"

static amxd_dm_t dm;
static amxo_parser_t parser;

int test_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    handle_events();
    return 0;
}

int test_teardown(UNUSED void** state) {
    amxm_close_all();

    amxo_resolver_import_close_all();
    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    return 0;
}

void handle_events(void) {
    while(amxp_signal_read() == 0) {
    }
}

void creat_pool_data(amxc_var_t* data, char* minaddr, char* maxaddr) {
    amxc_var_t* options = NULL;
    amxc_var_t* option = NULL;
    amxc_var_set_type(data, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, data, "Alias", "dummy");
    amxc_var_add_key(cstring_t, data, "IntfName", "dummy");
    amxc_var_add_key(cstring_t, data, "MinAddress", minaddr);
    amxc_var_add_key(cstring_t, data, "MaxAddress", maxaddr);
    amxc_var_add_key(cstring_t, data, "SubnetMask", "255.255.255.0");
    amxc_var_add_key(uint32_t, data, "LeaseTime", 360);
    options = amxc_var_add_key(amxc_llist_t, data, "Option", NULL);

    option = amxc_var_add(amxc_htable_t, options, NULL);
    amxc_var_add_key(cstring_t, option, "Alias", "option-12");
    amxc_var_add_key(bool, option, "Enable", true);
    amxc_var_add_key(uint8_t, option, "Tag", 12);
    amxc_var_add_key(cstring_t, option, "Value", "test-value");

    option = amxc_var_add(amxc_htable_t, options, NULL);
    amxc_var_add_key(cstring_t, option, "Alias", "option-13");
    amxc_var_add_key(bool, option, "Enable", false);
    amxc_var_add_key(uint8_t, option, "Tag", 13);
    amxc_var_add_key(cstring_t, option, "Value", "test-value");

    option = amxc_var_add(amxc_htable_t, options, NULL);
    amxc_var_add_key(cstring_t, option, "Alias", "option-125");
    amxc_var_add_key(bool, option, "Enable", true);
    amxc_var_add_key(uint8_t, option, "Tag", 125);
    amxc_var_add_key(cstring_t, option, "Value", "test-value");

    option = amxc_var_add(amxc_htable_t, options, NULL);
    amxc_var_add_key(cstring_t, option, "Alias", "option-119");
    amxc_var_add_key(bool, option, "Enable", true);
    amxc_var_add_key(uint8_t, option, "Tag", 119);
    amxc_var_add_key(cstring_t, option, "Value", "0262650A736F66746174686F6D6503636F6D000A736F66746174686F6D6503636F6D000272640A736F66746174686F6D6503636F6D00");

    return;
}

void creat_static_ip_data(amxc_var_t* data, char* ip, char* minaddr, char* maxaddr) {
    amxc_var_t* addr = NULL;
    amxc_var_t* pool = NULL;
    amxc_var_set_type(data, AMXC_VAR_ID_HTABLE);

    pool = amxc_var_add_key(amxc_htable_t, data, "pool", NULL);
    creat_pool_data(pool, minaddr, maxaddr);
    addr = amxc_var_add_key(amxc_htable_t, data, "addr", NULL);

    amxc_var_add_key(cstring_t, addr, "Alias", "dummy_ip");
    amxc_var_add_key(cstring_t, addr, "Chaddr", "ee:ee:ee:ee:ee:ee");
    amxc_var_add_key(cstring_t, addr, "Yiaddr", ip);
}