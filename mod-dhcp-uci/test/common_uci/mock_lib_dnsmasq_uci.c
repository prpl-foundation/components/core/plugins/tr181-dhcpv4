/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include <amxc/amxc_macros.h>

#include "mock_lib_dnsmasq_uci.h"

lib_dnsmasq_state_t lib_dnsmasq;

// Function
// wrapper
void __wrap_dnsmasq_init(dnsmasq_callback_t handler_add, dnsmasq_callback_t handler_delete) {
    lib_dnsmasq.add_cb = handler_add;
    lib_dnsmasq.rm_cb = handler_delete;
}

void __wrap_dnsmasq_clean(void) {
    lib_dnsmasq.add_cb = NULL;
    lib_dnsmasq.rm_cb = NULL;
}

int __wrap_dnsmasq_subscribe(void) {
    return 0;
}
int __wrap_dnsmasq_unsubscribe(void) {
    return 0;
}

int __wrap_dnsmasq_add_all_leases(char* alias) {
    assert_string_equal(alias, "dummy");
    return 0;
}

// mock function

void mock_lib_dnsmasq_send_lease() {
    amxc_var_t lease;
    amxc_var_t* options_list = NULL;
    amxc_var_t* option = NULL;
    assert_non_null(lib_dnsmasq.add_cb);

    amxc_var_init(&lease);
    amxc_var_set_type(&lease, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &lease, "Alias", "dummy");
    amxc_var_add_key(cstring_t, &lease, "Chaddr", "48:9e:bd:2d:66:93");
    amxc_var_add_key(cstring_t, &lease, "IPv4Address", "192.168.1.140");
    amxc_var_add_key(cstring_t, &lease, "LeaseTimeRemaining", "2022-10-19T00:02:58Z");
    options_list = amxc_var_add_key(amxc_llist_t, &lease, "Option", NULL);
    option = amxc_var_add(amxc_htable_t, options_list, NULL);
    amxc_var_add_key(uint32_t, option, "tag", 42);
    amxc_var_add_key(cstring_t, option, "raw", "Hello 1");
    option = amxc_var_add(amxc_htable_t, options_list, NULL);
    amxc_var_add_key(uint32_t, option, "tag", 24);
    amxc_var_add_key(cstring_t, option, "raw", "Hello 2");

    lib_dnsmasq.add_cb(&lease);
    amxc_var_clean(&lease);
}

void mock_lib_dnsmasq_remove_lease() {
    amxc_var_t lease;
    assert_non_null(lib_dnsmasq.rm_cb);

    amxc_var_init(&lease);
    amxc_var_set_type(&lease, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &lease, "Alias", "dummy");
    amxc_var_add_key(cstring_t, &lease, "Chaddr", "48:9e:bd:2d:66:93");
    amxc_var_add_key(cstring_t, &lease, "IPv4Address", "192.168.1.140");

    lib_dnsmasq.rm_cb(&lease);
    amxc_var_clean(&lease);
}