/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxm/amxm.h>
#include <amxp/amxp.h>
#include <v4v6option.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "dnsmasq/common_api.h"
#include "mod_dhcp_uci.h"
#include "ubus_uci.h"

#define ME "dhcp_uci"
#define DNSMASQ_FILE "/etc/init.d/dnsmasq"

struct _mod_uci {
    amxm_shared_object_t* so;
};

static struct _mod_uci uci;

struct _dhcp_options {
    const char* option_number;
    const char* option_field;
};

static struct _dhcp_options opts[] = {
    {"6", "DNSServers"}
};

/**
 * @brief
 * Send a service command the dnsmasq init script
 *
 * @param cmd the command to send.
 * @return 0 on success
 */
static int dnsmasq_cmd(const char* cmd) {
    const char* args[] = {NULL, NULL, NULL, NULL};
    int rc = 1;
    amxp_subproc_t* subproc = NULL;

    amxp_subproc_new(&subproc);

    args[0] = "service";
    args[1] = "dnsmasq";
    args[2] = cmd;

    SAH_TRACEZ_INFO(ME, "Calling %s %s %s", args[0], args[1], args[2]);
    rc = amxp_subproc_vstart_wait(subproc, 30000, (char**) args);
    switch(rc) {
    case -1:
        SAH_TRACEZ_ERROR(ME, "Error calling dnsmasq init.d script");
        break;
    case 1:
        SAH_TRACEZ_ERROR(ME, "Killing dnsmasq init.d script that timed out");
        amxp_subproc_kill(subproc, SIGKILL);
        break;
    }
    amxp_subproc_delete(&subproc);
    return rc;
}

static int dnsmasq_loadconfig(void) {
    int rc = -1;
    rc = dnsmasq_cmd("restart");
    when_failed_trace(rc, exit, ERROR, "Unable to restart dnsmasq");
exit:
    return rc;
}

/**
 * @brief
 * Config lease time for the uci config with the tr181 parameters.
 *
 * @param params The tr181 dhcpv4 pool parameters.
 * @param uci_config the uci config to fill.
 * @return 0 if success
 */
static int uci_pool_set_config_lease_time(amxc_var_t* params, amxc_var_t* uci_config) {
    int32_t leasetime = 0;
    amxc_string_t leasetime_uci;
    amxc_var_t* tmp = NULL;
    int ret = 1;

    amxc_string_init(&leasetime_uci, 0);

    when_null_trace(params, exit, ERROR, "No parameters given.");
    when_null_trace(uci_config, exit, ERROR, "Uci config not initialised.");

    leasetime = GET_UINT32(params, "LeaseTime");

    if(leasetime < 0) {
        amxc_string_set(&leasetime_uci, "infinite");
    } else {
        amxc_string_setf(&leasetime_uci, "%ds", leasetime);
    }

    tmp = amxc_var_add_new_key(uci_config, "leasetime");
    amxc_var_push(cstring_t, tmp, amxc_string_take_buffer(&leasetime_uci));
    ret = 0;
exit:
    amxc_string_clean(&leasetime_uci);
    return ret;
}

/**
 * @brief
 * Set the network range for the uci configuration.
 *
 * @param params The parameter from the dhcpv4 server plugin.
 * @param uci_config The uci configuration to fill.
 * @return 0 if success
 */
static int uci_pool_set_network_config(amxc_var_t* params, amxc_var_t* uci_config) {
    int rc = 1;
    const char* min_addr = NULL;
    const char* max_addr = NULL;
    const char* subnetmask = NULL;
    struct in_addr netmask;
    struct in_addr minip;
    struct in_addr maxip;
    int32_t nlimit = 0;
    amxc_string_t start;
    amxc_string_t limit;
    amxc_var_t* tmp = NULL;

    amxc_string_init(&start, 0);
    amxc_string_init(&limit, 0);

    when_null_trace(params, exit, ERROR, "No parameters given.");
    when_null_trace(uci_config, exit, ERROR, "Uci config not initialised.");

    min_addr = GET_CHAR(params, "MinAddress");
    max_addr = GET_CHAR(params, "MaxAddress");
    subnetmask = GET_CHAR(params, "SubnetMask");
    when_str_empty_trace(min_addr, exit, ERROR, "Pool is missing MinAddress parameter");
    when_str_empty_trace(subnetmask, exit, ERROR, "Pool is missing SubnetMask parameter");
    when_str_empty_trace(max_addr, exit, ERROR, "Pool is missing MaxAddress parameter");

    rc = inet_pton(AF_INET, min_addr, &minip);
    when_false_trace(rc == 1, exit, ERROR, "Bad MinAddress parameter '%s'", min_addr);
    rc = inet_pton(AF_INET, max_addr, &maxip);
    when_false_trace(rc == 1, exit, ERROR, "Bad MaxAddress parameter '%s'", max_addr);
    rc = inet_pton(AF_INET, subnetmask, &netmask);
    when_false_trace(rc == 1, exit, ERROR, "Bad SubnetMask parameter '%s'", subnetmask);

    rc = 1;
    nlimit = htonl(maxip.s_addr | netmask.s_addr) - htonl(minip.s_addr | netmask.s_addr);
    when_false_trace((nlimit >= 0), exit, ERROR, ".MinAddress '%s' is larger than .MaxAddress '%s'", min_addr, max_addr);

    amxc_string_setf(&start, "%d", htonl(minip.s_addr) - htonl(minip.s_addr & netmask.s_addr));
    amxc_string_setf(&limit, "%d", nlimit + 1);

    tmp = amxc_var_add_new_key(uci_config, "start");
    amxc_var_push(cstring_t, tmp, amxc_string_take_buffer(&start));
    tmp = amxc_var_add_new_key(uci_config, "limit");
    amxc_var_push(cstring_t, tmp, amxc_string_take_buffer(&limit));
    rc = 0;
exit:
    amxc_string_clean(&start);
    amxc_string_clean(&limit);
    return rc;
}

/**
 * @brief
 * Parse dhcpv4 option using libdhcpoptions
 *
 * @param option_parsed return variant that contains the parsed option
 * @param value the hex string value to parse by libdhcpoptions
 * @param tag the option tag
 */
static int parse_dhcp_option(amxc_var_t* option_parsed, const char* value, uint32_t tag) {
    unsigned char* option = NULL;
    uint32_t length = 0;
    int rv = -1;

    when_null_trace(option_parsed, exit, ERROR, "option_parsed variant is NULL");
    when_str_empty_trace(value, exit, ERROR, "Can't parse option %u, value is empty", tag);

    option = dhcpoption_option_convert2bin(value, &length);
    when_null_trace(option, exit, ERROR, "Unable to create the binary of the option");

    SAH_TRACEZ_INFO(ME, "option is %s", option);

    dhcpoption_v4parse(option_parsed, tag, length, option);

    rv = 0;
exit:
    free(option);
    return rv;
}

/**
 * @brief
 * Parse option 119 (domain search list) for uci config
 *
 * @param option_list the list of dhcp options that will be written to uci
 * @param value the hex string value to parse by libdhcp
 */
static int parse_option_119(amxc_var_t* option_list, const char* value) {
    amxc_var_t option_parsed;
    int ret = 1;
    amxc_var_t* tmp = NULL;
    amxc_string_t opt;

    amxc_var_init(&option_parsed);
    amxc_string_init(&opt, 0);

    when_failed_trace(parse_dhcp_option(&option_parsed, value, 119), exit, ERROR, "Failed to parse option 119");
    amxc_var_cast(&option_parsed, AMXC_VAR_ID_CSV_STRING);
    amxc_string_setf(&opt, "%d,%s", 119, GET_CHAR(&option_parsed, NULL));
    tmp = amxc_var_add_new(option_list);
    amxc_var_push(cstring_t, tmp, amxc_string_take_buffer(&opt));

    ret = 0;
exit:
    amxc_string_clean(&opt);
    amxc_var_clean(&option_parsed);
    return ret;
}

/**
 * @brief
 * Parse option 125 ( vendor specific option ) for uci config
 *
 * @param option_list the list of dhcp options that will be written to uci
 * @param value the hex string value to parse by libdhcp
 */
static int parse_option_125(amxc_var_t* option_list, const char* value) {
    amxc_var_t option_parsed;
    int ret = 1;
    uint32_t entreprise = 0;

    amxc_var_init(&option_parsed);

    when_failed_trace(parse_dhcp_option(&option_parsed, value, 125), exit, ERROR, "Failed to parse option 125");
    entreprise = GETP_UINT32(&option_parsed, "0.Enterprise");

    amxc_var_for_each(sub_option, GETP_ARG(&option_parsed, "0.Data")) {
        amxc_var_t* tmp = NULL;
        amxc_string_t opt;
        amxc_string_init(&opt, 0);
        amxc_string_setf(&opt, "vi-encap:%d,%d,%s", entreprise, GET_UINT32(sub_option, "Code"), GET_CHAR(sub_option, "Data"));
        tmp = amxc_var_add_new(option_list);
        amxc_var_push(cstring_t, tmp, amxc_string_take_buffer(&opt));
        amxc_string_clean(&opt);
        ret = 0;
    }
exit:
    amxc_var_clean(&option_parsed);
    return ret;
}

/**
 * @brief
 * set the list of dhcpv4 option to be uci compatible.
 *
 * @param params The params from the dhcpv4 server plugin.
 * @param options_list the list of option to be configured in uci.
 * @return 0 if success
 */
static int uci_pool_set_config_dhcp_options(amxc_var_t* params, amxc_var_t* options_list) {
    int ret = 1;
    amxc_var_t* options = NULL;
    when_null_trace(params, exit, ERROR, "No parameters given.");
    when_null_trace(options_list, exit, ERROR, "List of options not initialised.");

    // specific param value
    for(uint32_t i = 0; i < (uint32_t) sizeof(opts) / sizeof(opts[0]); i++) {
        amxc_string_t opt;
        const char* var = GET_CHAR(params, opts[i].option_field);
        if(var != NULL) {
            amxc_var_t* tmp = NULL;
            amxc_string_init(&opt, 0);
            amxc_string_setf(&opt, "%s,%s", opts[i].option_number, var);
            tmp = amxc_var_add_new(options_list);
            amxc_var_push(cstring_t, tmp, amxc_string_take_buffer(&opt));
            amxc_string_clean(&opt);
        }
    }

    options = GET_ARG(params, "Option");
    amxc_var_for_each(option, options) {
        const char* value = GET_CHAR(option, "Value");
        bool enable = GET_BOOL(option, "Enable");
        uint8_t tag = GET_UINT32(option, "Tag");

        if(enable && (value != NULL)) {
            switch(tag) {
            case 119:
                ret = parse_option_119(options_list, value);
                if(ret != 0) {
                    SAH_TRACEZ_ERROR(ME, "Unable to parse option 119");
                }
                break;
            // vendor specific information
            case 125:
                ret = parse_option_125(options_list, value);
                if(ret != 0) {
                    SAH_TRACEZ_ERROR(ME, "Unable to parse option 125");
                }
                break;
            default:
                SAH_TRACEZ_WARNING(ME, "Option not supported");
            }
        }
    }

    // in option list
    ret = 0;
exit:
    return ret;
}

/**
 * @brief
 * Translate the pool parameter from the dhcpv4 server plugin to be uci compatible.
 *
 * @param params The parameters from the dhcpv4 server plugin.
 * @param uci_config The config to be send via uci.
 * @param uci_alias The corresponding interface alias in uci.
 * @return 0 if success
 */
static int translate_to_uci_pool_config(amxc_var_t* params, amxc_var_t* uci_config, const char* uci_alias) {
    int ret = 1;
    when_null_trace(params, exit, ERROR, "No parameters given.");
    when_null_trace(uci_config, exit, ERROR, "Uci config not initialised.");
    amxc_var_add_key(cstring_t, uci_config, "interface", uci_alias);
    amxc_var_add_key(cstring_t, uci_config, "ignore", "0");

    ret = uci_pool_set_config_dhcp_options(params, amxc_var_add_key(amxc_llist_t, uci_config, "dhcp_option", NULL));
    when_failed_trace(ret, exit, ERROR, "Unable to set dhcp options.");
    ret = uci_pool_set_network_config(params, uci_config);
    when_failed_trace(ret, exit, ERROR, "Unable to set network config.");
    ret = uci_pool_set_config_lease_time(params, uci_config);
    when_failed_trace(ret, exit, ERROR, "Unable to set lease time config.");
exit:
    return ret;
}

/**
 * @brief
 * translate an alias to be uci compatible.
 *
 * @param alias The alias to translate.
 * @param uci_alias The alias translated to freed.
 * @return 0 if success
 */
static int translate_to_uci_alias(const char* alias, char** uci_alias) {
    amxc_string_t amxc_uci_alias;
    int ret = 1;

    amxc_string_init(&amxc_uci_alias, 0);

    when_null_trace(alias, exit, ERROR, "No alias given to check for uci interface.");
    amxc_string_set(&amxc_uci_alias, alias);
    amxc_string_replace(&amxc_uci_alias, "-", "_", UINT32_MAX);

    *uci_alias = amxc_string_take_buffer(&amxc_uci_alias);
    ret = 0;
exit:
    amxc_string_clean(&amxc_uci_alias);
    return ret;
}

/**
 * @brief
 * Enable the configuration of the tr181 pool interface.
 *
 * @param function_name Name of the function called.
 * @param args Pool variable structure to config in uci.
 * @param ret Return variant.
 * @return int 0 on success.
 */
int enable_dhcp_pool_uci(UNUSED const char* function_name,
                         amxc_var_t* args,
                         UNUSED amxc_var_t* ret) {
    int retval = 1;
    amxc_var_t uci_args;
    amxc_var_t ret_val;
    char* uci_alias = NULL;

    amxc_var_init(&ret_val);
    amxc_var_init(&uci_args);
    amxc_var_set_type(&uci_args, AMXC_VAR_ID_HTABLE);
    when_null_trace(args, exit, ERROR, "Unable to get list of listening interfaces.");
    retval = translate_to_uci_alias(GET_CHAR(args, "IntfName"), &uci_alias);
    when_failed_trace(retval, exit, ERROR, "Unable to translate alias.");
    retval = translate_to_uci_pool_config(args, &uci_args, uci_alias);
    when_failed_trace(retval, exit, ERROR, "Unable to config uci config.");
    retval = uci_call("set", "dhcp", uci_alias, "dhcp", &uci_args, &ret_val);
    if(retval != 0) {
        SAH_TRACEZ_WARNING(ME, "No previous configuration for pool interface, creating it.");
        retval = uci_call("add", "dhcp", uci_alias, "dhcp", &uci_args, &ret_val);
        when_failed_trace(retval, exit, ERROR, "Unable to set umdns entry in uci.");
    }
    retval = uci_call("commit", "dhcp", NULL, NULL, NULL, &ret_val);
    when_failed_trace(retval, exit, ERROR, "Unable to commit change to uci.");
    retval = dnsmasq_loadconfig();
    when_failed_trace(retval, exit, ERROR, "Unable to reload dnsmasq config.");
    retval = dnsmasq_add_all_leases(uci_alias);
    when_failed_trace(retval, exit, ERROR, "Unable to get already present leases.");
exit:
    free(uci_alias);
    amxc_var_clean(&ret_val);
    amxc_var_clean(&uci_args);
    return retval;
}

/**
 * @brief
 * Enable the configuration of the tr181 pool interface.
 *
 * @param function_name Name of the function called.
 * @param args Pool variable structure to config in uci.
 * @param ret Return variant.
 * @return int 0 on success.
 */
int disable_dhcp_pool_uci(UNUSED const char* function_name,
                          amxc_var_t* args,
                          UNUSED amxc_var_t* ret) {
    int retval = 1;
    amxc_var_t uci_args;
    amxc_var_t ret_val;
    char* uci_alias = NULL;

    amxc_var_init(&ret_val);
    amxc_var_init(&uci_args);
    amxc_var_set_type(&uci_args, AMXC_VAR_ID_HTABLE);

    when_null_trace(args, exit, ERROR, "Unable to get list of listening interfaces.");

    retval = translate_to_uci_alias(GET_CHAR(args, "IntfName"), &uci_alias);
    when_failed_trace(retval, exit, ERROR, "Unable to translate alias.");

    amxc_var_add(cstring_t, &uci_args, "start");
    amxc_var_add(cstring_t, &uci_args, "limit");

    retval = uci_call("delete", "dhcp", uci_alias, "dhcp", &uci_args, &ret_val);
    when_failed_trace(retval, exit, ERROR, "Unable to delete independant dhcpv4 configuration.");

    amxc_var_clean(&uci_args);
    amxc_var_init(&uci_args);
    amxc_var_set_type(&uci_args, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &uci_args, "ignore", "1");

    retval = uci_call("set", "dhcp", uci_alias, "dhcp", &uci_args, &ret_val);
    if(retval != 0) {
        SAH_TRACEZ_WARNING(ME, "No previous configuration for pool interface, creating it.");
        retval = uci_call("add", "dhcp", uci_alias, "dhcp", &uci_args, &ret_val);
        when_failed_trace(retval, exit, ERROR, "Unable to set umdns entry in uci.");
    }

    retval = uci_call("commit", "dhcp", NULL, NULL, NULL, &ret_val);
    when_failed_trace(retval, exit, ERROR, "Unable to commit change to uci.");

    retval = dnsmasq_loadconfig();
    when_failed_trace(retval, exit, ERROR, "Unable to reload dnsmasq config.");
exit:
    free(uci_alias);
    amxc_var_clean(&ret_val);
    amxc_var_clean(&uci_args);
    return retval;
}

/**
 * @brief
 * Fill the uci variant to configure a static host on the system. Check if the static ip given is valid.
 *
 * @param pool the pool information related to the static ip address.
 * @param addr the static ip address information.
 * @return 0 on success
 */
static int fill_static_ip(amxc_var_t* pool, amxc_var_t* addr, amxc_var_t* uci_config) {
    struct in_addr ip;
    const char* ip_addr = NULL;
    int rc = 1;

    when_null_trace(pool, exit, ERROR, "No pool parameters given.");
    when_null_trace(addr, exit, ERROR, "No addr parameters given.");
    when_null_trace(uci_config, exit, ERROR, "Uci config not initialised.");

    ip_addr = GET_CHAR(addr, "Yiaddr");
    when_str_empty_trace(ip_addr, exit, ERROR, "Static address is missing Yiaddr parameter");

    rc = inet_pton(AF_INET, ip_addr, &ip);
    when_false_trace(rc == 1, exit, ERROR, "Bad static address '%s'", ip_addr);

    rc = 0;

    amxc_var_add_key(cstring_t, uci_config, "ip", ip_addr);
exit:
    return rc;
}

/**
 * @brief
 * Config a static ip for a certain pool in uci config.
 *
 * @param function_name Name of the function called.
 * @param args Static ip variable structure to config in uci.
 * @param ret Return variant.
 * @return int 0 on success.
 */
int enable_dhcp_static_ip_uci(UNUSED const char* function_name,
                              amxc_var_t* args,
                              UNUSED amxc_var_t* ret) {
    int retval = 1;
    amxc_var_t uci_args;
    amxc_var_t ret_val;
    char* uci_alias = NULL;
    const char* mac_addr = NULL;
    amxc_var_t* pool = NULL;
    amxc_var_t* addr = NULL;

    amxc_var_init(&ret_val);
    amxc_var_init(&uci_args);
    amxc_var_set_type(&uci_args, AMXC_VAR_ID_HTABLE);

    when_null_trace(args, exit, ERROR, "Unable to get static ip parameters.");

    pool = GET_ARG(args, "pool");
    addr = GET_ARG(args, "addr");
    when_null_trace(pool, exit, ERROR, "Unable to get pool parameters.");
    when_null_trace(addr, exit, ERROR, "Unable to get addr parameters.");


    translate_to_uci_alias(GET_CHAR(addr, "IntfName"), &uci_alias);

    retval = fill_static_ip(pool, addr, &uci_args);
    when_failed_trace(retval, exit, ERROR, "Static ip cannot be configured.");

    mac_addr = GET_CHAR(addr, "Chaddr");
    when_null_trace(mac_addr, exit, ERROR, "No MAC address given.");
    amxc_var_add_key(cstring_t, &uci_args, "mac", mac_addr);

    retval = uci_call("set", "dhcp", uci_alias, "host", &uci_args, &ret_val);
    if(retval != 0) {
        SAH_TRACEZ_WARNING(ME, "No previous configuration for host, creating it.");
        retval = uci_call("add", "dhcp", uci_alias, "host", &uci_args, &ret_val);
        when_failed_trace(retval, exit, ERROR, "Unable to set host entry in uci.");
    }

    retval = uci_call("commit", "dhcp", NULL, NULL, NULL, &ret_val);
    when_failed_trace(retval, exit, ERROR, "Unable to commit change to uci.");

    retval = dnsmasq_loadconfig();
    when_failed_trace(retval, exit, ERROR, "Unable to reload dnsmasq config.");
exit:
    amxc_var_clean(&ret_val);
    amxc_var_clean(&uci_args);
    free(uci_alias);
    return retval;
}

/**
 * @brief
 * Delete a static ip for a certain pool in uci config.
 *
 * @param function_name Name of the function called.
 * @param args Static ip variable structure to config in uci.
 * @param ret Return variant.
 * @return int 0 on success.
 */
int disable_dhcp_static_ip_uci(UNUSED const char* function_name,
                               amxc_var_t* args,
                               UNUSED amxc_var_t* ret) {
    int retval = 1;
    amxc_var_t uci_args;
    amxc_var_t ret_val;
    char* uci_alias = NULL;

    amxc_var_init(&ret_val);
    amxc_var_init(&uci_args);
    amxc_var_set_type(&uci_args, AMXC_VAR_ID_HTABLE);

    when_null_trace(args, exit, ERROR, "Unable to get list of listening interfaces.");

    translate_to_uci_alias(GET_CHAR(args, "IntfName"), &uci_alias);

    amxc_var_add(cstring_t, &uci_args, "ip");
    amxc_var_add(cstring_t, &uci_args, "mac");

    retval = uci_call("delete", "dhcp", uci_alias, "host", &uci_args, &ret_val);
    when_failed_trace(retval, exit, ERROR, "Unable to delete host configuration.");

    retval = uci_call("commit", "dhcp", NULL, NULL, NULL, &ret_val);
    when_failed_trace(retval, exit, ERROR, "Unable to commit change to uci.");

    retval = dnsmasq_loadconfig();
    when_failed_trace(retval, exit, ERROR, "Unable to reload dnsmasq config.");
exit:
    amxc_var_clean(&ret_val);
    amxc_var_clean(&uci_args);
    free(uci_alias);
    return retval;
}

/**
 * @brief
 * Does nothing as the back-end is not handle by the module.
 *
 * @param function_name unused
 * @param args unused
 * @param ret unused
 * @return int always return 0
 */
int start_back_end_uci(UNUSED const char* function_name,
                       UNUSED amxc_var_t* args,
                       UNUSED amxc_var_t* ret) {
    SAH_TRACEZ_WARNING(ME, "back-end not handle by the module.");
    return 0;
}

/**
 * @brief
 * Does nothing as the back-end is not handle by the module.
 *
 * @param function_name unused
 * @param args unused
 * @param ret unused
 * @return int always return 0
 */
int stop_back_end_uci(UNUSED const char* function_name,
                      UNUSED amxc_var_t* args,
                      UNUSED amxc_var_t* ret) {
    SAH_TRACEZ_WARNING(ME, "back-end not handle by the module.");
    return 0;
}

/**
 * @brief
 * Send a lease to the DHCPv4 plugin.
 *
 * @param lease the lease to add.
 */
static void send_lease(amxc_var_t* lease) {
    amxc_var_t rv;
    int status = 1;

    amxc_var_init(&rv);

    when_null_trace(lease, exit, ERROR, "No lease to send to the plugin.");

    status = amxm_execute_function("self",
                                   MOD_CORE,
                                   "set-lease",
                                   lease,
                                   &rv);
    when_failed_trace(status, exit, ERROR, "Could not send the lease to the plugin %d.", status);
exit:
    amxc_var_clean(&rv);
    return;
}

/**
 * @brief
 * Remove a lease to the DHCPv4 plugin.
 *
 * @param lease the lease to remove.
 */
static void remove_lease(amxc_var_t* lease) {
    amxc_var_t rv;
    int status = 1;

    amxc_var_init(&rv);

    when_null_trace(lease, exit, ERROR, "No lease to send to the plugin.");

    status = amxm_execute_function("self",
                                   MOD_CORE,
                                   "remove-lease",
                                   lease,
                                   &rv);
    when_failed_trace(status, exit, ERROR, "Could not remove the lease on the plugin.");
exit:
    amxc_var_clean(&rv);
    return;
}


/**
 * @brief
 * Start and register functions for the uci module.
 *
 * @return AMXM_CONSTRUCTOR always return 0.
 */
static AMXM_CONSTRUCTOR dhcp_uci_start(void) {
    int r = 0;
    amxm_module_t* mod = NULL;
    uci.so = amxm_so_get_current();
    amxm_module_register(&mod, uci.so, MOD_UCI);
    amxm_module_add_function(mod, "enable-dhcp-pool", enable_dhcp_pool_uci);
    amxm_module_add_function(mod, "disable-dhcp-pool", disable_dhcp_pool_uci);
    amxm_module_add_function(mod, "enable-static-ip", enable_dhcp_static_ip_uci);
    amxm_module_add_function(mod, "disable-static-ip", disable_dhcp_static_ip_uci);
    amxm_module_add_function(mod, "start-back-end", start_back_end_uci);
    amxm_module_add_function(mod, "stop-back-end", stop_back_end_uci);
    dnsmasq_init(send_lease, remove_lease);
    r = dnsmasq_cmd("start");
    when_failed_trace(r, exit, ERROR, "Unable to get dnsmasq status");
    dnsmasq_subscribe();
exit:
    return r;
}

/**
 * @brief
 * Stop and clean the uci module.
 *
 * @return AMXM_DESTRUCTOR always return 0.
 */
static AMXM_DESTRUCTOR dhcp_uci_stop(void) {
    int r = 0;
    uci.so = NULL;
    dnsmasq_clean();
    dnsmasq_unsubscribe();
    return r;
}
