/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include <amxc/amxc_macros.h>

#include "mock_stdio_dnsmasq.h"

// local variables
static amxc_string_t* file_data = NULL;
static bool record = false;

// Function
// wrapper

int __wrap_fputs(const char* data, UNUSED FILE* fd) {
    assert_non_null(data);
    if(record) {
        if(file_data == NULL) {
            amxc_string_new(&file_data, 0);
            amxc_string_set(file_data, data);
        } else {
            amxc_string_appendf(file_data, data);
        }
    }
    return 1;
}

int __wrap_remove(const char* __filename) {
    assert_non_null(__filename);
    return 0;
}

int __wrap_mkdir(const char* __path, __mode_t __mode) {
    assert_non_null(__path);
    assert_non_null(__mode);
    return 0;
}

int __wrap_getline(char** line, UNUSED size_t* len, UNUSED FILE* fd) {
    static size_t lenght = 8;
    len = &lenght;
    lenght--;
    if(*line == NULL) {
        *line = strdup("test");
    }
    if(lenght != 0) {
        return lenght;
    } else {
        return -1;
    }
}

// mock function
void mock_stdio_register_inputs(bool enable) {
    record = enable;
}

void mock_stdio_check_inputs(const char* expected) {
    assert_non_null(strstr(amxc_string_get(file_data, 0), expected));
}

void mock_stdio_free_inputs(void) {
    amxc_string_delete(&file_data);
    file_data = NULL;
}