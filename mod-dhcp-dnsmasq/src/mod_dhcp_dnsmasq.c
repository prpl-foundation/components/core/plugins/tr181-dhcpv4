/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxm/amxm.h>
#include <amxp/amxp.h>
#include <amxd/amxd_types.h>
#include <amxb/amxb.h>
#include <v4v6option.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "dnsmasq/common_api.h"
#include "mod_dhcp_dnsmasq.h"

#define ME "dhcp_dnsmasq"
#define KILL_TIMEOUT_IN_MSEC 1000
#define DELAY_SUB_TIMER 20000
#define str_empty(x) ((x) == NULL || *(x) == 0)

static char main_confs[] = \
    "##################################################\n"
    "# Auto generated conf file from mod-dhcp-dnsmasq #\n"
    "##################################################\n"
    "conf-dir=/tmp/dnsmasq_conf,*.conf\n"
    "\n"
    "dhcp-authoritative\n"
    "domain-needed\n"
    "localise-queries\n"
    "read-ethers\n"
    "enable-ubus\n"
    "expand-hosts\n"
    "bind-dynamic\n"
    "local-service\n"
    "port=0\n"
    "edns-packet-max=4096\n"
    "server=/lan/\n"
    "dhcp-leasefile=/tmp/dhcp.leases\n"
    "resolv-file=/tmp/resolv.conf.d/resolv.conf.auto\n"
    "stop-dns-rebind\n"
    "rebind-localhost-ok\n"
    "dhcp-broadcast=tag:needs-broadcast\n"
    "addn-hosts=/tmp/hosts\n"
    "user=dnsmasq\n"
    "group=dnsmasq\n"
    "bogus-priv\n"
    "no-dhcp-interface=eth0\n"
    "dhcp-sequential-ip\n"
    "\n"
    "server=/bind/\n"
    "server=/invalid/\n"
    "server=/local/\n"
    "server=/localhost/\n"
    "server=/onion/\n"
    "server=/test/\n"
    "\n"
    "# Below are all configurable option from the DM\n"
    "\n";

typedef enum _dnsmasq_cmd {
    START_DNSMASQ_CMD,
    STOP_DNSMASQ_CMD,
    RESTART_DNSMASQ_CMD,
    NB_DNSMASQ_CMD
} dnsmasq_cmd_t;

struct _mod_dnsmasq {
    amxm_shared_object_t* so;
    amxp_subproc_t* subproc;
    amxc_var_t* config_htable;
    amxc_var_t* static_ht;
    amxp_timer_t* buffer_timer;
    dnsmasq_cmd_t last_cmd;
    bool launch;
    bool need_first_start;
    bool need_start;
    bool disabled;
};

static struct _mod_dnsmasq dnsmasq;

struct _dhcp_options {
    const char* option_number;
    const char* option_field;
};

struct _dhcp_config {
    const char* tr181_name;
    const char* dnsmasq_option;
};

static struct _dhcp_options opts[] = {
    {"6", "DNSServers"},
    {"15", "DomainName"}
};

static struct _dhcp_config configs[] = {
    {"IgnoreIDS", "dhcp-ignore-clid\n"},
    {"DDNSUpdate", "dhcp-client-update\n"},
    {"EnableFQDN", "dhcp-fqdn\n"}
};

static int dnsmasq_cmd(dnsmasq_cmd_t cmd);

/**
 * @brief
 * Callback when dnsmasq stop unexpectedly
 *
 * @param event_name Name of the event send.
 * @param event_data Date of the event.
 * @param priv NULL.
 */
static void dnsmasq_stopped(UNUSED const char* const event_name,
                            const amxc_var_t* const event_data,
                            UNUSED void* const priv) {
    uint32_t exit_code = GET_UINT32(event_data, "ExitCode");
    uint32_t sig_code = GET_UINT32(event_data, "Signal");
    // this event handler is always called, even on expected restart, only handle the error cases with this function
    if(exit_code != 0) {
        dnsmasq.launch = false;
        SAH_TRACEZ_ERROR(ME, "dnsmasq exit unexpected, exit code : %d", exit_code);
    }
    if(sig_code != 0) {
        SAH_TRACEZ_ERROR(ME, "dnsmasq crash unexpected, restarting it, signal : %d", sig_code);
        dnsmasq.launch = false;
        dnsmasq_cmd(RESTART_DNSMASQ_CMD);
    }
}

/**
 * @brief
 * Start, stop or restart the dnsmasq utility.
 *
 * @param cmd action to be done with the dnsmasq utility.
 * @return 0 on success
 */
static void dnsmasq_control(UNUSED amxp_timer_t* timer, UNUSED void* priv) {
    int retval = 1;
    dnsmasq_cmd_t cmd = dnsmasq.last_cmd;

    when_null_trace(dnsmasq.subproc, exit, ERROR, "Could not find the subproc");
    if((cmd != START_DNSMASQ_CMD) && dnsmasq.launch) {
        dnsmasq.launch = false;
        dnsmasq_unsubscribe();
        amxp_slot_disconnect(dnsmasq.subproc->sigmngr, "stop", dnsmasq_stopped);
        SAH_TRACEZ_INFO(ME, "Stopping dnsmasq with pid %d", amxp_subproc_get_pid(dnsmasq.subproc));
        retval = amxp_subproc_kill(dnsmasq.subproc, SIGTERM);
        if(retval != 0) {
            SAH_TRACEZ_WARNING(ME, "Failed to kill the subproc with SIGTERM %s (%d) %d", strerror(errno), errno, retval);
        }
        retval = amxp_subproc_wait(dnsmasq.subproc, KILL_TIMEOUT_IN_MSEC);
        if(retval != 0) {
            SAH_TRACEZ_WARNING(ME, "Time out to SIGTERM dnsmasq sending SIGKILL");
            retval = amxp_subproc_kill(dnsmasq.subproc, SIGKILL);
            when_failed_trace(retval, exit, ERROR, "Failed to kill the subproc with SIGKILL %s (%d) %d", strerror(errno), errno, retval);
            retval = amxp_subproc_wait(dnsmasq.subproc, KILL_TIMEOUT_IN_MSEC);
            when_failed_trace(retval, exit, ERROR, "SIGKILL timed out.");
        }
    }

    if((cmd != STOP_DNSMASQ_CMD) && !dnsmasq.launch) {
        dnsmasq_subscribe();
        const char* args[] = {"/usr/sbin/dnsmasq", "-k", NULL};
        retval = amxp_subproc_vstart(dnsmasq.subproc, (char**) args);
        switch(retval) {
        case -1:
            SAH_TRACEZ_ERROR(ME, "Error calling dnsmasq process");
            break;
        case 1:
            SAH_TRACEZ_ERROR(ME, "Killing dnsmasq process that timed out");
            amxp_subproc_kill(dnsmasq.subproc, SIGKILL);
            break;
        default:
            retval = amxp_slot_connect(dnsmasq.subproc->sigmngr, "stop", NULL, dnsmasq_stopped, NULL);
            if(retval != 0) {
                SAH_TRACEZ_ERROR(ME, "cannot connect slot to dnsmasq %d", retval);
            }
            SAH_TRACEZ_INFO(ME, "Started dnsmasq with pid %d", amxp_subproc_get_pid(dnsmasq.subproc));
            dnsmasq.launch = true;
        }
    }
exit:
    return;
}

/**
 * @brief
 * Aggregate dnsmasq command when multiple one arise at the same time
 *
 * @param cmd Command to pass to dnsmasq
 * @return int 0 if command known
 */
static int dnsmasq_cmd(dnsmasq_cmd_t cmd) {
    int rc = 0;
    amxp_timer_start(dnsmasq.buffer_timer, 1000);
    switch(cmd) {
    case START_DNSMASQ_CMD:
        if(dnsmasq.last_cmd == STOP_DNSMASQ_CMD) {
            dnsmasq.last_cmd = RESTART_DNSMASQ_CMD;
        }
        if(dnsmasq.last_cmd == NB_DNSMASQ_CMD) {
            dnsmasq.last_cmd = START_DNSMASQ_CMD;
        }
        break;
    case RESTART_DNSMASQ_CMD:
        dnsmasq.last_cmd = RESTART_DNSMASQ_CMD;
        break;
    case STOP_DNSMASQ_CMD:
        dnsmasq.last_cmd = STOP_DNSMASQ_CMD;
        break;
    default:
        rc = 1;
        amxp_timer_stop(dnsmasq.buffer_timer);
        SAH_TRACEZ_ERROR(ME, "Command not define");
    }
    return rc;
}

/**
 * @brief
 * Load the dnsmasq configuration. Cause a restart of dnsmasq.
 *
 * @return 0 on success
 */
static int dnsmasq_loadconfig(void) {
    int rc = 1;
    dnsmasq.need_start = true;
    if(dnsmasq.need_first_start == false) {
        rc = dnsmasq_cmd(RESTART_DNSMASQ_CMD);
    } else {
        rc = 0;
    }
    return rc;
}

/**
 * @brief
 * Parse dhcpv4 option using libdhcpoptions
 *
 * @param option_parsed return variant that contains the parsed option
 * @param value the hex string value to parse by libdhcpoptions
 * @param tag the option tag
 */
static int parse_dhcp_option(amxc_var_t* option_parsed, const char* value, uint32_t tag) {
    unsigned char* option = NULL;
    uint32_t length = 0;
    int rv = -1;

    when_null_trace(option_parsed, exit, ERROR, "option_parsed variant is NULL");
    when_str_empty_trace(value, exit, ERROR, "Can't parse option %u, value is empty", tag);

    option = dhcpoption_option_convert2bin(value, &length);
    when_null_trace(option, exit, ERROR, "Unable to create the binary of the option");

    dhcpoption_v4parse(option_parsed, tag, length, option);
    rv = 0;
exit:
    free(option);
    return rv;
}

/**
 * @brief
 * Parse option if no specific handler is needed for dnsmasq config
 *
 * @param config the config string to write to
 * @param alias the alias of the pool
 * @param pool_tag the pool tag
 * @param option_parsed the option parsed by libdhcp
 * @param tag the option number
 * @return int 0 if success
 */
static int parse_option_default(amxc_string_t* config, char* alias, const char* pool_tag, amxc_var_t* option_parsed, uint8_t tag, bool force) {
    if(amxc_var_type_of(option_parsed) == AMXC_VAR_ID_LIST) {
        amxc_var_cast(option_parsed, AMXC_VAR_ID_CSV_STRING);
    }

    amxc_string_appendf(config, "dhcp-option");
    if(force) {
        amxc_string_appendf(config, "-force");
    }
    return amxc_string_appendf(config, "=tag:%s_%s,%d,%s\n", alias, pool_tag, tag, GET_CHAR(option_parsed, NULL));
}

/**
 * @brief
 * Parse option 125 ( vendor specific option ) for dnsmasq config
 *
 * @param config the config string to write to
 * @param alias the alias of the pool
 * @param pool_tag The tag of the pool
 * @param option_parsed The option parsed by libdhcp
 * @return int 0 if success
 */
static int parse_option_125(amxc_string_t* config, char* alias, const char* pool_tag, amxc_var_t* option_parsed, bool force) {
    int ret = 1;
    uint32_t entreprise = 0;

    entreprise = GETP_UINT32(option_parsed, "0.Enterprise");

    amxc_var_for_each(sub_option, GETP_ARG(option_parsed, "0.Data")) {
        amxc_string_appendf(config, "dhcp-option");
        if(force) {
            amxc_string_appendf(config, "-force");
        }
        ret = amxc_string_appendf(config, "=tag:%s_%s,vi-encap:%d,%d,%s\n", alias, pool_tag, entreprise, GET_UINT32(sub_option, "Code"), GET_CHAR(sub_option, "Data"));
        when_failed_trace(ret, exit, ERROR, "Cannot parse part of option 125");
    }
exit:
    return ret;
}

/**
 * @brief
 * Parse option 43 (vendor specific option) for dnsmasq config
 *
 * @param config the config string to write to
 * @param alias the alias of the pool
 * @param pool_tag The tag of the pool
 * @param option_parsed The option parsed by libdhcp
 * @return int 0 if success
 */
static int parse_option_43(amxc_string_t* config, char* alias,
                           const char* pool_tag, amxc_var_t* option_parsed,
                           bool force) {
    int ret = 1;

    amxc_var_for_each(sub_option, option_parsed) {
        amxc_string_appendf(config, "dhcp-option%s", force ? "-force" : "");
        ret = amxc_string_appendf(config, "=tag:%s_%s,vendor:,%u,%s\n", alias,
                                  pool_tag, GET_UINT32(sub_option, "Code"),
                                  GET_CHAR(sub_option, "Data"));
        when_failed_trace(ret, exit, ERROR, "Cannot parse part of option 43");
    }

exit:
    return ret;
}

/**
 * @brief
 * set the list of dhcpv4 option to be dnsmasq compatible.
 *
 * @param params The params from the dhcpv4 server plugin.
 * @param config the configuration string to fill in.
 * @param alias the interface alias.
 * @param pool_tag the tag of the pool
 * @return 0 if success
 */
static int dnsmasq_pool_set_config_dhcp_options(amxc_var_t* params, amxc_string_t* config, char* alias, const char* pool_tag) {
    int ret = 1;
    amxc_var_t* options_list = NULL;

    when_null_trace(params, exit, ERROR, "No parameters given.");
    when_null_trace(config, exit, ERROR, "List of options not initialised.");
    when_null_trace(alias, exit, ERROR, "No interface given");

    // specific param value
    for(uint32_t i = 0; i < (uint32_t) sizeof(opts) / sizeof(opts[0]); i++) {
        const char* var = GET_CHAR(params, opts[i].option_field);
        if(var != NULL) {
            amxc_string_appendf(config, "dhcp-option=tag:%s_%s,%s,%s\n", alias, pool_tag, opts[i].option_number, var);
        }
    }

    // in option list
    options_list = GET_ARG(params, "Option");
    amxc_var_for_each(option, options_list) {
        const char* value = GET_CHAR(option, "Value");
        bool enable = GET_BOOL(option, "Enable");
        bool force = GET_BOOL(option, "Force");
        int tag = GET_UINT32(option, "Tag");
        amxc_var_t option_parsed;

        if(!enable || (value == NULL)) {
            SAH_TRACEZ_INFO(ME, "option %d not enabled or doesn't have a value.", tag);
            continue;
        }

        amxc_var_init(&option_parsed);

        if(parse_dhcp_option(&option_parsed, value, tag) != 0) {
            SAH_TRACEZ_ERROR(ME, "Failed to parse option %d", tag);
            amxc_var_clean(&option_parsed);
            continue;
        }

        switch(tag) {
        case 125:
            ret = parse_option_125(config, alias, pool_tag, &option_parsed, force);
            if(ret != 0) {
                SAH_TRACEZ_ERROR(ME, "Unable to parse option 125");
            }
            break;
        case 43:
            ret = parse_option_43(config, alias, pool_tag, &option_parsed, force);
            if(ret != 0) {
                SAH_TRACEZ_ERROR(ME, "Unable to parse option 43");
            }
            break;
        default:
            SAH_TRACEZ_INFO(ME, "No special cast for option %d needed so casting to string", tag);
            ret = parse_option_default(config, alias, pool_tag, &option_parsed, tag, force);
            if(ret != 0) {
                SAH_TRACEZ_ERROR(ME, "Unable to parse option %d", tag);
            }
        }
        amxc_var_clean(&option_parsed);
    }
    ret = 0;
exit:
    return ret;
}

/**
 * @brief
 * translate an alias to be dnsmasq compatible.
 *
 * @param alias The alias to translate.
 * @param dnsmasq_alias The alias translated to freed.
 * @return 0 if success
 */
static int translate_to_dnsmasq_alias(const char* alias, char** dnsmasq_alias) {
    amxc_string_t amxc_uci_alias;
    int ret = 1;

    amxc_string_init(&amxc_uci_alias, 0);

    when_null_trace(alias, exit, ERROR, "No alias given to check for interface.");
    amxc_string_set(&amxc_uci_alias, alias);
    amxc_string_replace(&amxc_uci_alias, "-", "_", UINT32_MAX);

    *dnsmasq_alias = amxc_string_take_buffer(&amxc_uci_alias);
    ret = 0;
exit:
    amxc_string_clean(&amxc_uci_alias);
    return ret;
}

/**
 * @brief
 * Set the network range for the dnsmasq configuration.
 *
 * @param params the parameter from the dhcpv4 server plugin.
 * @param config the configuration string to fill in.
 * @param intf_name the interface alias
 * @param flags_activated tag selection of activated filter
 * @param pool_tag the tag of the pool
 * @return 0 if success
 */
static int dnsmasq_pool_set_network_config(amxc_var_t* params, amxc_string_t* config, char* intf_name, int flag_activated, const char* pool_tag) {
    int rc = 1;
    const char* min_addr = NULL;
    const char* max_addr = NULL;
    const char* subnetmask = NULL;
    int32_t leasetime = 0;
    struct in_addr netmask;
    struct in_addr minip;
    struct in_addr maxip;
    int32_t nlimit = 0;
    bool exclude = false;

    when_null_trace(params, exit, ERROR, "No parameters given.");
    when_null_trace(config, exit, ERROR, "Config not initialised.");

    min_addr = GET_CHAR(params, "MinAddress");
    max_addr = GET_CHAR(params, "MaxAddress");
    subnetmask = GET_CHAR(params, "SubnetMask");
    leasetime = GET_UINT32(params, "LeaseTime");

    when_str_empty_trace(min_addr, exit, ERROR, "Pool is missing MinAddress parameter");
    when_str_empty_trace(subnetmask, exit, ERROR, "Pool is missing SubnetMask parameter");
    when_str_empty_trace(max_addr, exit, ERROR, "Pool is missing MaxAddress parameter");

    rc = inet_pton(AF_INET, min_addr, &minip);
    when_false_trace(rc == 1, exit, ERROR, "Bad MinAddress parameter '%s'", min_addr);
    rc = inet_pton(AF_INET, max_addr, &maxip);
    when_false_trace(rc == 1, exit, ERROR, "Bad MaxAddress parameter '%s'", max_addr);
    rc = inet_pton(AF_INET, subnetmask, &netmask);
    when_false_trace(rc == 1, exit, ERROR, "Bad SubnetMask parameter '%s'", subnetmask);

    rc = 1;
    nlimit = htonl(maxip.s_addr | netmask.s_addr) - htonl(minip.s_addr | netmask.s_addr);
    when_false_trace((nlimit >= 0), exit, ERROR, ".MinAddress '%s' is larger than .MaxAddress '%s'", min_addr, max_addr);

    amxc_string_appendf(config, "dhcp-range=");

    if(flag_activated & MATCH_VENDOR) {
        exclude = GET_BOOL(params, "VendorClassIDExclude");
        amxc_string_appendf(config, "tag:%s%s_vendor,", exclude ? "!" : "", intf_name);
    }
    if(flag_activated & MATCH_MAC) {
        exclude = GET_BOOL(params, "ChaddrExclude");
        amxc_string_appendf(config, "tag:%s%s_mac,", exclude ? "!" : "", intf_name);
    }
    if(flag_activated & MATCH_DEVICE) {
        const char* allow = GET_CHAR(params, "AllowedDevices");
        exclude = strcmp(allow, "Unknown") == 0;
        amxc_string_appendf(config, "tag:%s%s_devices,", exclude ? "!" : "", intf_name);
    }

    amxc_string_appendf(config, "set:%s_%s,%s,%s,%s", intf_name, pool_tag, min_addr, max_addr, subnetmask);
    if(leasetime > 0) {
        amxc_string_appendf(config, ",%ds\n", leasetime);
    } else {
        amxc_string_appendf(config, "\n");
    }
    rc = 0;
exit:
    return rc;
}

/**
 * @brief
 * Set the filter for devices mac filtering.
 *
 * @param params the parameter from the dhcpv4 server plugin.
 * @param config the configuration string to fill in.
 * @param flags_activated switch device bit to true if a filter is activated
 * @param pool_tag the tag of the pool
 * @return 0 if success
 */
static int dnsmasq_pool_set_devices_matching(amxc_var_t* params, amxc_string_t* config, int* flags_activated, const char* pool_tag) {
    int rc = 1;
    const char* allow = NULL;
    amxc_var_t* devices_ht = NULL;

    when_null_trace(params, exit, ERROR, "No parameters given.");
    when_null_trace(config, exit, ERROR, "Config not initialised.");

    allow = GET_CHAR(params, "AllowedDevices");

    when_str_empty_trace(allow, exit, ERROR, "No allow device filter given");
    when_true_status(strcmp(allow, "All") == 0, exit, rc = 0);

    *flags_activated |= MATCH_DEVICE;
    devices_ht = GET_ARG(params, "MACs");
    when_null_trace(devices_ht, exit, ERROR, "No devices given for mac filtering");

    amxc_var_for_each(mac_var, devices_ht) {
        const char* mac = amxc_var_key(mac_var);
        amxc_string_appendf(config, "dhcp-mac=set:%s_devices,%s,ff:ff:ff:ff:ff:ff\n", pool_tag, mac);
    }
    rc = 0;
exit:
    return rc;
}

/**
 * @brief
 * Set the filter for vendor class ID matching.
 *
 * @param params the parameter from the dhcpv4 server plugin.
 * @param config the configuration string to fill in.
 * @param flags_activated switch vendor bit to true if a match is activated
 * @param pool_tag the tag of the pool
 * @return 0 if success
 */
static int dnsmasq_pool_set_vendor_class_matching(amxc_var_t* params, amxc_string_t* config, int* flags_activated, const char* pool_tag) {
    int rc = 1;
    const char* id = NULL;
    const char* mode = NULL;

    when_null_trace(params, exit, ERROR, "No parameters given.");
    when_null_trace(config, exit, ERROR, "Config not initialised.");

    id = GET_CHAR(params, "VendorClassID");
    mode = GET_CHAR(params, "VendorClassIDMode");

    when_str_empty_trace(mode, exit, ERROR, "No mode given for Vendor class filtering");
    rc = 0;
    when_str_empty_trace(id, exit, INFO, "No VendorClassID given");

    *flags_activated |= MATCH_VENDOR;

    amxc_string_appendf(config, "dhcp-vendorclass=set:%s_vendor,", pool_tag);
    if((strcmp("Suffix", mode) == 0) || (strcmp("Substring", mode) == 0)) {
        amxc_string_appendf(config, "*");
    }
    amxc_string_appendf(config, "%s", id);
    if((strcmp("Prefix", mode) == 0) || (strcmp("Substring", mode) == 0)) {
        amxc_string_appendf(config, "*");
    }
    amxc_string_appendf(config, "\n");
exit:
    return rc;
}

/**
 * @brief
 * Set the filter for MAC matching.
 *
 * @param params the parameter from the dhcpv4 server plugin.
 * @param config the configuration string to fill in.
 * @param flags_activated switch to true if a match is configured
 * @param pool_tag the tag of the pool
 * @return 0 if success
 */
static int dnsmasq_pool_set_mac_matching(amxc_var_t* params, amxc_string_t* config, int* flags_activated, const char* pool_tag) {
    int rc = 1;
    const char* chaddr = NULL;
    const char* mask = NULL;

    when_null_trace(params, exit, ERROR, "No parameters given.");
    when_null_trace(config, exit, ERROR, "Config not initialised.");

    rc = 0;
    chaddr = GET_CHAR(params, "Chaddr");
    when_str_empty_trace(chaddr, exit, INFO, "No chaddr given");
    rc = 1;
    mask = GET_CHAR(params, "ChaddrMask");
    when_null_trace(chaddr, exit, ERROR, "No chaddr mask given");

    *flags_activated |= MATCH_MAC;

    amxc_string_appendf(config, "dhcp-mac=set:%s_mac,%s,%s\n", pool_tag, chaddr, mask[0] == '\0' ? "ff:ff:ff:ff:ff:ff" : mask);
    rc = 0;
exit:
    return rc;
}

/**
 * @brief
 * Translate the pool parameter from the dhcpv4 server plugin to be dnsmasq compatible.
 *
 * @param params The parameters from the dhcpv4 server plugin.
 * @param config the configuration string to fill in.
 * @param intf_name The corresponding interface alias.
 * @return 0 if success
 */
static int translate_to_dnsmasq_pool_config(amxc_var_t* params, amxc_string_t* config, char* intf_name) {
    int ret = 1;
    int match_enable = 0;
    const char* pool_tag = NULL;
    when_null_trace(params, exit, ERROR, "No parameters given.");
    when_null_trace(config, exit, ERROR, "Config not initialised.");

    pool_tag = GET_CHAR(params, "Tag");
    when_str_empty_trace(pool_tag, exit, ERROR, "Unable to translate pool tag.");

    ret = dnsmasq_pool_set_devices_matching(params, config, &match_enable, intf_name);
    when_failed_trace(ret, exit, ERROR, "Unable to set devices matching pool rule.");
    ret = dnsmasq_pool_set_vendor_class_matching(params, config, &match_enable, intf_name);
    when_failed_trace(ret, exit, ERROR, "Unable to set vendor matching pool rule.");
    ret = dnsmasq_pool_set_mac_matching(params, config, &match_enable, intf_name);
    when_failed_trace(ret, exit, ERROR, "Unable to set mac matching pool rule.");
    ret = dnsmasq_pool_set_network_config(params, config, intf_name, match_enable, pool_tag);
    when_failed_trace(ret, exit, ERROR, "Unable to set network config.");
    ret = dnsmasq_pool_set_config_dhcp_options(params, config, intf_name, pool_tag);
    when_failed_trace(ret, exit, ERROR, "Unable to set dhcp options.");
exit:
    return ret;
}

/**
 * @brief
 *
 * Check if the config file for the pool need to be updated
 * @param bufer string to be put in the config file.
 * @param alias alias of the pool.
 * @param order order of the pool ( low value is highest prio )
 * @param tag the tag of the pool
 * @return true if the content of the file is different than the buffer.
 */
static bool need_update(amxc_string_t* buffer, char* alias, uint32_t order, const char* tag) {
    bool ret = true;
    amxc_var_t* config = NULL;

    when_null_trace(buffer, exit, WARNING, "No buffer given for comparaison.");
    when_null_trace(alias, exit, WARNING, "No alias given for the pool to configure.");
    when_null_trace(tag, exit, WARNING, "No tag given for the pool");

    config = GET_ARG(dnsmasq.config_htable, alias);
    when_null_trace(config, exit, WARNING, "No config found");

    ret = GET_UINT32(config, "Order") != order;
    if(ret) {
        int retval = 1;
        amxc_string_t file_name;
        amxc_string_init(&file_name, 0);
        amxc_string_setf(&file_name, "%s/%s%d_%s_pool.conf", DNSMASQ_CONF_DIR, strstr(tag, "extender") != NULL ? "!" : "", GET_UINT32(config, "Order"), alias);
        retval = remove(amxc_string_get(&file_name, 0));
        if(retval != 0) {
            SAH_TRACEZ_ERROR(ME, "Unable to remove previous config file");
        }
        amxc_var_t* temp = amxc_var_take_key(dnsmasq.config_htable, alias);
        amxc_var_delete(&temp);
        amxc_string_clean(&file_name);
        goto exit;
    }

    ret = strcmp(amxc_string_get(buffer, 0), GET_CHAR(config, "Config")) != 0;
    if(ret) {
        amxc_var_t* temp = amxc_var_take_key(dnsmasq.config_htable, alias);
        amxc_var_delete(&temp);
    }
exit:
    return ret;
}

/**
 * @brief
 * Enable the configuration of the tr181 pool interface.
 *
 * @param function_name Name of the function called.
 * @param args Pool variable structure to config in dnsmasq.
 * @param ret Return variant.
 * @return int 0 on success.
 */
int enable_dhcp_pool_dnsmasq(UNUSED const char* function_name,
                             amxc_var_t* args,
                             UNUSED amxc_var_t* ret) {
    int retval = 1;
    amxc_string_t config;
    amxc_string_t file_name;
    char* alias = NULL;
    const char* tag = NULL;
    FILE* fd = NULL;
    int order = 0;
    amxc_var_t* table = NULL;
    amxc_string_init(&config, 0);
    amxc_string_init(&file_name, 0);

    when_null_trace(args, exit, ERROR, "Unable to get list of listening interfaces.");

    order = GET_INT32(args, "Order");
    tag = GET_CHAR(args, "Tag");
    when_null_trace(tag, exit, ERROR, "No tag given for the pool");
    retval = translate_to_dnsmasq_alias(GET_CHAR(args, "Alias"), &alias);
    when_failed_trace(retval, exit, ERROR, "Unable to translate alias.");
    retval = translate_to_dnsmasq_pool_config(args, &config, alias);
    when_failed_trace(retval, exit, ERROR, "Unable to config dnsmasq config.");
    amxc_string_setf(&file_name, "%s/%s%d_%s_pool.conf", DNSMASQ_CONF_DIR, strstr(tag, "extender") != NULL ? "!" : "", order, alias);
    when_false_trace(need_update(&config, alias, order, tag), exit, INFO, "config already upto date");

    retval = 1;
    fd = fopen(amxc_string_get(&file_name, 0), "w+");
    when_null_trace(fd, exit, ERROR, "Unable to open conf file %s", amxc_string_get(&file_name, 0));


    retval = fputs(amxc_string_get(&config, 0), fd);
    fclose(fd);

    when_false_trace(retval != EOF, exit, ERROR, "Unable to write file %s", amxc_string_get(&file_name, 0));
    table = amxc_var_add_key(amxc_htable_t, dnsmasq.config_htable, alias, NULL);
    amxc_var_add_key(uint32_t, table, "Order", order);
    amxc_var_add_key(cstring_t, table, "Config", amxc_string_get(&config, 0));

    retval = dnsmasq_loadconfig();
    when_failed_trace(retval, exit, ERROR, "Unable to reload dnsmasq config.");
exit:
    amxc_string_clean(&config);
    amxc_string_clean(&file_name);
    free(alias);
    return retval;
}

/**
 * @brief
 * Enable the configuration of the tr181 pool interface.
 *
 * @param function_name Name of the function called.
 * @param args Pool variable structure to config in dnsmasq.
 * @param ret Return variant.
 * @return int 0 on success.
 */
int disable_dhcp_pool_dnsmasq(UNUSED const char* function_name,
                              amxc_var_t* args,
                              UNUSED amxc_var_t* ret) {
    int retval = 1;
    char* alias = NULL;
    const char* tag = NULL;
    amxc_var_t* entry = NULL;
    amxc_string_t file_name;
    int order = 0;

    amxc_string_init(&file_name, 0);

    when_null_trace(args, exit, ERROR, "Unable to get list of listening interfaces.");

    order = GET_INT32(args, "Order");
    tag = GET_CHAR(args, "Tag");
    when_null_trace(tag, exit, ERROR, "No tag given for the pool");
    retval = translate_to_dnsmasq_alias(GET_CHAR(args, "Alias"), &alias);
    when_failed_trace(retval, exit, ERROR, "Unable to translate alias.");

    amxc_string_setf(&file_name, "%s/%s%d_%s_pool.conf", DNSMASQ_CONF_DIR, strstr(tag, "extender") != NULL ? "!" : "", order, alias);

    retval = remove(amxc_string_get(&file_name, 0));
    when_failed_trace(retval, exit, ERROR, "Unable to delete pool conf file %s.", amxc_string_get(&file_name, 0));

    entry = amxc_var_get_key(dnsmasq.config_htable, alias, AMXC_VAR_FLAG_DEFAULT);
    when_null_trace(entry, exit, ERROR, "No config found for removing the pool.");
    amxc_var_delete(&entry);

    retval = dnsmasq_loadconfig();
    when_failed_trace(retval, exit, ERROR, "Unable to reload dnsmasq config.");
exit:
    free(alias);
    amxc_string_clean(&file_name);
    return retval;
}

/**
 * @brief
 * Fill the uci variant to configure a static host on the system. Check if the static ip given is valid.
 *
 * @param pool the pool information related to the static ip address.
 * @param addr the static ip address information.
 * @param config the configuration string to fill in.
 * @return 0 on success
 */
static int fill_static_ip(amxc_var_t* pool, amxc_var_t* addr, amxc_string_t* config) {
    struct in_addr ip;
    const char* ip_addr = NULL;
    const char* mac_addr = NULL;
    const char* tag = NULL;
    char* intf_name = NULL;
    int rc = 1;

    when_null_trace(pool, exit, ERROR, "No pool parameters given.");
    when_null_trace(addr, exit, ERROR, "No addr parameters given.");
    when_null_trace(config, exit, ERROR, "Uci config not initialised.");

    rc = translate_to_dnsmasq_alias(GET_CHAR(pool, "Alias"), &intf_name);
    when_failed_trace(rc, exit, ERROR, "Unable to translate interface alias.");

    tag = GET_CHAR(pool, "Tag");
    when_str_empty_trace(tag, exit, ERROR, "Unable to get pool tag.");

    ip_addr = GET_CHAR(addr, "Yiaddr");
    when_str_empty_trace(ip_addr, exit, ERROR, "Static address is missing Yiaddr parameter");

    rc = inet_pton(AF_INET, ip_addr, &ip);
    when_false_trace(rc == 1, exit, ERROR, "Bad static address '%s'", ip_addr);

    mac_addr = GET_CHAR(addr, "Chaddr");
    when_null_trace(mac_addr, exit, ERROR, "No MAC address given.");

    rc = 0;

    amxc_string_appendf(config, "dhcp-host=%s,tag:%s_%s,%s\n", mac_addr, intf_name, tag, ip_addr);
exit:
    free(intf_name);
    return rc;
}

/**
 * @brief
 * Config a static ip for a certain pool in dnsmasq config.
 *
 * @param function_name Name of the function called.
 * @param args Static ip variable structure to config in dnsmasq.
 * @param ret Return variant.
 * @return int 0 on success.
 */
int enable_dhcp_static_ip_dnsmasq(UNUSED const char* function_name,
                                  amxc_var_t* args,
                                  UNUSED amxc_var_t* ret) {
    int retval = 1;
    uint32_t order = 0;
    char* alias = NULL;
    char* pool_alias = NULL;
    const char* tag = NULL;
    amxc_string_t config;
    amxc_string_t file_name;
    amxc_string_t key;
    FILE* fd = NULL;
    amxc_var_t* pool = NULL;
    amxc_var_t* addr = NULL;
    amxc_var_t* entry = NULL;

    amxc_string_init(&config, 0);
    amxc_string_init(&file_name, 0);
    amxc_string_init(&key, 0);

    when_null_trace(args, exit, ERROR, "Unable to get list of listening interfaces.");

    pool = GET_ARG(args, "pool");
    addr = GET_ARG(args, "addr");
    when_null_trace(pool, exit, ERROR, "Unable to get pool parameters.");
    when_null_trace(addr, exit, ERROR, "Unable to get addr parameters.");

    tag = GET_CHAR(pool, "Tag");
    when_null_trace(tag, exit, ERROR, "No tag given for the pool");
    order = GET_INT32(pool, "Order");
    retval = translate_to_dnsmasq_alias(GET_CHAR(addr, "Alias"), &alias);
    when_failed_trace(retval, exit, ERROR, "Unable to translate alias.");
    retval = translate_to_dnsmasq_alias(GET_CHAR(pool, "Alias"), &pool_alias);
    when_failed_trace(retval, exit, ERROR, "Unable to translate alias.");

    retval = fill_static_ip(pool, addr, &config);
    when_failed_trace(retval, exit, ERROR, "Static ip cannot be configured.");

    amxc_string_setf(&key, "%s_%s", pool_alias, alias);
    amxc_string_setf(&file_name, "%s/%s%d_%s_host.conf", DNSMASQ_CONF_DIR, strstr(tag, "extender") != NULL ? "!" : "", order, amxc_string_get(&key, 0));

    fd = fopen(amxc_string_get(&file_name, 0), "w+");
    when_null_trace(fd, exit, ERROR, "Unable to open conf file %s", amxc_string_get(&file_name, 0));


    retval = fputs(amxc_string_get(&config, 0), fd);
    fclose(fd);

    when_false_trace(retval != EOF, exit, ERROR, "Unable to write file %s", amxc_string_get(&file_name, 0));

    entry = amxc_var_get_key(dnsmasq.static_ht, amxc_string_get(&key, 0), AMXC_VAR_FLAG_DEFAULT);
    if(entry != NULL) {
        uint32_t prev_order = GET_UINT32(entry, NULL);
        if(order != prev_order) {
            amxc_string_setf(&file_name, "%s/%d_%s_host.conf", DNSMASQ_CONF_DIR, prev_order, amxc_string_get(&key, 0));
            SAH_TRACEZ_INFO(ME, "Deleting old host conf file with previous order %s", amxc_string_get(&file_name, 0));
            retval = remove(amxc_string_get(&file_name, 0));
            amxc_var_delete(&entry);
            when_failed_trace(retval, exit, ERROR, "Unable to delete pool conf file %s.", amxc_string_get(&file_name, 0));
        }
    }

    if(entry == NULL) {
        amxc_var_add_key(uint32_t, dnsmasq.static_ht, amxc_string_get(&key, 0), order);
    }

    retval = dnsmasq_loadconfig();
    when_failed_trace(retval, exit, ERROR, "Unable to reload dnsmasq config.");
exit:
    free(alias);
    free(pool_alias);
    amxc_string_clean(&config);
    amxc_string_clean(&file_name);
    amxc_string_clean(&key);
    return retval;
}

/**
 * @brief
 * Delete a static ip for a certain pool in dnsmasq config.
 *
 * @param function_name Name of the function called.
 * @param args Static ip variable structure to config in dnsmasq.
 * @param ret Return variant.
 * @return int 0 on success.
 */
int disable_dhcp_static_ip_dnsmasq(UNUSED const char* function_name,
                                   amxc_var_t* args,
                                   UNUSED amxc_var_t* ret) {
    int retval = 1;
    char* alias = NULL;
    char* pool_alias = NULL;
    const char* tag = NULL;
    uint32_t order = 0;
    amxc_string_t file_name;
    amxc_string_t key;
    amxc_var_t* entry = NULL;
    amxc_var_t* pool = NULL;
    amxc_var_t* addr = NULL;

    amxc_string_init(&file_name, 0);
    amxc_string_init(&key, 0);

    when_null_trace(args, exit, ERROR, "Unable to get list of listening interfaces.");

    pool = GET_ARG(args, "pool");
    addr = GET_ARG(args, "addr");
    when_null_trace(pool, exit, ERROR, "Unable to get pool parameters.");
    when_null_trace(addr, exit, ERROR, "Unable to get addr parameters.");

    tag = GET_CHAR(pool, "Tag");
    when_null_trace(tag, exit, ERROR, "No tag given for the pool");
    order = GET_INT32(pool, "Order");
    retval = translate_to_dnsmasq_alias(GET_CHAR(addr, "Alias"), &alias);
    when_failed_trace(retval, exit, ERROR, "Unable to translate alias.");
    retval = translate_to_dnsmasq_alias(GET_CHAR(pool, "Alias"), &pool_alias);
    when_failed_trace(retval, exit, ERROR, "Unable to translate alias.");

    amxc_string_setf(&key, "%s_%s", pool_alias, alias);
    amxc_string_setf(&file_name, "%s/%s%d_%s_host.conf", DNSMASQ_CONF_DIR, strstr(tag, "extender") != NULL ? "!" : "", order, amxc_string_get(&key, 0));

    retval = remove(amxc_string_get(&file_name, 0));
    when_failed_trace(retval, exit, ERROR, "Unable to delete pool conf file %s.", amxc_string_get(&file_name, 0));

    entry = amxc_var_get_key(dnsmasq.static_ht, amxc_string_get(&key, 0), AMXC_VAR_FLAG_DEFAULT);
    when_null_trace(entry, exit, ERROR, "No static entry found for removing the static address.");
    amxc_var_delete(&entry);

    retval = dnsmasq_loadconfig();
    when_failed_trace(retval, exit, ERROR, "Unable to reload dnsmasq config.");
exit:
    free(alias);
    free(pool_alias);
    amxc_string_clean(&file_name);
    amxc_string_clean(&key);
    return retval;
}

/**
 * @brief
 * Create the basic configuration file for dnsmasq.
 *
 * @return 0 on success
 */
static int init_dnsmasq_files(amxc_var_t* config) {
    int ret = 1;
    FILE* fd = NULL;

    fd = fopen(DNSMASQ_CONF_FILE, "w+");
    when_null_trace(fd, exit, ERROR, "Unable to open conf file %s", DNSMASQ_CONF_FILE);

    ret = fputs(main_confs, fd);
    if(ret == EOF) {
        SAH_TRACEZ_ERROR(ME, "Unable to write base config : %s", strerror(errno));
        fclose(fd);
        goto exit;
    }
    for(uint32_t i = 0; i < (uint32_t) sizeof(configs) / sizeof(configs[0]); i++) {
        ret = fputs(GET_BOOL(config, configs[i].tr181_name) ? configs[i].dnsmasq_option : "", fd);
        if(ret == EOF) {
            SAH_TRACEZ_ERROR(ME, "Unable to write %s config : %s", configs[i].tr181_name, strerror(errno));
            fclose(fd);
            goto exit;
        }
    }

    fclose(fd);
    ret = 0;
exit:
    return ret;
}

/**
 * @brief
 * Create the directory for dnsmasq configuration files.
 *
 * @return 0 on success
 */
static int init_dnsmasq_dir(void) {
    int ret = 1;
    struct stat st = {0};

    if(stat(DNSMASQ_CONF_DIR, &st) == -1) {
        ret = mkdir(DNSMASQ_CONF_DIR, 0777);
        when_failed_trace(ret, exit, ERROR, "Unable to create the config directory %s.", DNSMASQ_CONF_DIR);
    }

    ret = 0;

exit:
    return ret;
}

/**
 * @brief
 * Start dnsmasq and subscribe to it for the first time, allow to avoid multiple restart of dnsmasq at boot.
 *
 * @param function_name The name of the function call
 * @param args argument passed to the function
 * @param ret return argument ( not used )
 * @return int 0 when dnsmasq was successfully launch.
 */
int start_dnsmasq(UNUSED const char* function_name,
                  amxc_var_t* args,
                  UNUSED amxc_var_t* ret) {
    int r = 1;
    dnsmasq.need_first_start = false;
    r = init_dnsmasq_files(args);
    when_failed_trace(r, exit, ERROR, "Unable to create basic configuration.");
    SAH_TRACEZ_INFO(ME, "dnsmasq waiting for config");
    if(dnsmasq.need_start) {
        r = dnsmasq_loadconfig();
        when_failed_trace(r, exit, ERROR, "Cannot start dnsmasq");
    } else {
        r = 0;
    }
exit:
    return r;
}

/**
 * @brief
 * Start dnsmasq and subscribe to it for the first time, allow to avoid multiple restart of dnsmasq at boot.
 *
 * @param function_name The name of the function call
 * @param args argument passed to the function
 * @param ret return argument ( not used )
 * @return int 0 when dnsmasq was successfully launch.
 */
int stop_dnsmasq(UNUSED const char* function_name,
                 UNUSED amxc_var_t* args,
                 UNUSED amxc_var_t* ret) {
    dnsmasq.need_first_start = true;
    dnsmasq_cmd(STOP_DNSMASQ_CMD);
    return 0;
}

/**
 * @brief
 * Clean all leases which mac address is given in the arguments
 *
 * @param function_name The name of the function call
 * @param args list of mac address to remove the leases of dnsmasq
 * @param ret return argument ( not used )
 * @return int 0 when dnsmasq was successfully launch.
 */
int clean_leases(UNUSED const char* function_name,
                 amxc_var_t* args,
                 UNUSED amxc_var_t* ret) {
    int r = 1;
    FILE* fptr = NULL;
    char* line = NULL;
    size_t len = 0;
    bool mac_in_leases = false;
    amxc_string_t buffer;

    amxc_string_init(&buffer, 0);

    when_null_trace(args, exit, ERROR, "No mac addresses given");

    fptr = fopen(DNSMASQ_LEASE_FILE, "r");
    when_null_trace(fptr, exit, ERROR, "Cannot open lease file");

    while(getline(&line, &len, fptr) != -1) {
        mac_in_leases = false;
        amxc_var_for_each(mac, args) {
            const char* mac_str = GET_CHAR(mac, NULL);
            if(strstr(line, mac_str) != NULL) {
                mac_in_leases = true;
                break;
            }
        }
        if(!mac_in_leases) {
            amxc_string_appendf(&buffer, "%s", line);
        }
    }

    fclose(fptr);
    free(line);

    fptr = fopen(DNSMASQ_LEASE_FILE, "w");
    when_null_trace(fptr, exit, ERROR, "Cannot open lease file");

    r = fputs(amxc_string_get(&buffer, 0), fptr);
    if(r == EOF) {
        SAH_TRACEZ_ERROR(ME, "Unable to write leases file : %s", strerror(errno));
        fclose(fptr);
        goto exit;
    }
    fclose(fptr);

    r = dnsmasq_loadconfig();
    when_failed_trace(r, exit, ERROR, "Unable to reload dnsmasq config.");
exit:
    amxc_string_clean(&buffer);
    return r;
}

static int write_leases_to_file(amxc_llist_t* leases, const char* file) {
    int ret = -1;
    FILE* f = NULL;

    when_str_empty_trace(file, exit, ERROR, "File name is empty");
    when_null_trace(leases, exit, ERROR, "leases list is empty");

    f = fopen(file, "w");
    when_null_trace(f, exit, ERROR, "Unable to open the file: %s", file);

    if(fgetc(f) != EOF) {
        SAH_TRACEZ_ERROR(ME, "The %s contains an entry, we cannot restore", file);
        fclose(f);
        goto exit;
    }

    amxc_llist_for_each(it, leases) {
        amxc_string_t* lease = amxc_container_of(it, amxc_string_t, it);
        fputs(amxc_string_get(lease, 0), f);
        fputs("\n", f);
    }
    ret = 0;
    fclose(f);
exit:
    return ret;
}

static int set_last_ipv4(amxc_var_t* ipv4_list, amxc_string_t* lease) {
    int rc = -1;
    amxc_ts_t time_remaining;
    const char* ipv4_addr = NULL;
    const char* time_remaining_str = "0001-01-01T00:00:00Z";

    when_null_trace(ipv4_list, exit, WARNING, "The IPv4Address list is NULL");

    amxc_ts_parse(&time_remaining, time_remaining_str, strlen(time_remaining_str));
    amxc_var_for_each(ipv4, ipv4_list) {
        amxc_ts_t tmp_time;
        const char* tmp_time_str = GETP_CHAR(ipv4, "0.LeaseTimeRemaining");
        when_str_empty_trace(tmp_time_str, exit, ERROR, "Cannot restore leases as time remaining not given");
        amxc_ts_parse(&tmp_time, tmp_time_str, strlen(tmp_time_str));
        if(amxc_ts_compare(&tmp_time, &time_remaining) == 1) {
            time_remaining_str = tmp_time_str;
            amxc_ts_parse(&time_remaining, time_remaining_str, strlen(time_remaining_str));
            ipv4_addr = GETP_CHAR(ipv4, "0.IPAddress");
        }
    }

    if(!str_empty(ipv4_addr) && (strcmp(time_remaining_str, "0001-01-01T00:00:00Z") != 0)) {
        amxc_string_prependf(lease, "%" PRId64 " ", time_remaining.sec);
        amxc_string_appendf(lease, " %s", ipv4_addr);
        rc = 0;
    }

exit:
    return rc;
}

static int set_lease_options(amxc_var_t* options, amxc_string_t* lease) {
    int rc = -1;
    amxc_var_t options_value;
    amxc_var_t* host_name = NULL;
    const char* client_id = NULL;
    uint32_t length = 0;
    unsigned char* value_bin = NULL;
    char lease_client_id[21];

    amxc_var_init(&options_value);
    amxc_var_set_type(&options_value, AMXC_VAR_ID_HTABLE);

    when_null_trace(options, exit, WARNING, "The Option list is NULL");

    amxc_var_for_each(opt, options) {
        int tag = GETP_INT32(opt, "0.Tag");

        switch(tag) {
        case 12:
            value_bin = dhcpoption_option_convert2bin(GETP_CHAR(opt, "0.Value"), &length);
            host_name = amxc_var_add_new_key(&options_value, "Name");
            dhcpoption_v4parse(host_name, tag, length, value_bin);
            break;
        case 61:
            client_id = GETP_CHAR(opt, "0.Value");
            if(!str_empty(client_id) && (strlen(client_id) == 14)) {
                sprintf(lease_client_id, "%c%c:%c%c:%c%c:%c%c:%c%c:%c%c:%c%c",
                        client_id[0], client_id[1], client_id[2], client_id[3], client_id[4], client_id[5],
                        client_id[6], client_id[7], client_id[8], client_id[9], client_id[10], client_id[11],
                        client_id[12], client_id[13]);
            }
            break;
        default:
            break;
        }
    }

    if(!str_empty(GET_CHAR(host_name, NULL)) &&
       (!str_empty(client_id) && (strlen(client_id) == 14))) {
        amxc_string_appendf(lease, " %s %s", GET_CHAR(host_name, NULL), lease_client_id);
        rc = 0;
    }
exit:
    free(value_bin);
    amxc_var_clean(&options_value);
    return rc;
}

/**
 * @brief
 * Updates the dhcp.leases file with the given leases. Restarts the dnsmasq backend
 *
 * @param function_name The name of the function call
 * @param args argument passed to the function
 * @param ret return argument ( not used )
 * @return int 0 when dnsmasq updated successfully.
 */
int update_dhcp_leases_dnsmasq(UNUSED const char* function_name,
                               amxc_var_t* args,
                               UNUSED amxc_var_t* ret) {
    int rc = -1;
    const amxc_llist_t* clients = NULL;
    amxc_llist_t leases;

    amxc_llist_init(&leases);
    when_null_trace(args, exit, ERROR, "Invalid arguments");

    clients = amxc_var_constcast(amxc_llist_t, GET_ARG(args, "Clients"));
    amxc_llist_for_each(client, clients) {
        amxc_var_t* client_data = amxc_var_from_llist_it(client);
        amxc_string_t lease;
        amxc_var_t* options = NULL;
        amxc_var_t* ipv4_list = NULL;
        const char* lease_chaddr = NULL;

        amxc_string_init(&lease, 0);

        lease_chaddr = GETP_CHAR(client_data, "0.Chaddr");
        when_str_empty_trace(lease_chaddr, fail, ERROR, "Could not retrieve the Chaddr");
        amxc_string_set(&lease, lease_chaddr);

        ipv4_list = GETP_ARG(client_data, "0.IPv4Address");
        rc = set_last_ipv4(ipv4_list, &lease);
        when_true_trace(rc != 0, fail, ERROR, "Could not set the lease IPAddress, LeaseTimeRemaining");

        options = GETP_ARG(client_data, "0.Option");
        rc = set_lease_options(options, &lease);
        when_true_trace(rc != 0, fail, ERROR, "Could not set the lease options 12, 61");

        amxc_llist_add_string(&leases, amxc_string_get(&lease, 0));
fail:
        amxc_string_clean(&lease);
        continue;
    }

    rc = write_leases_to_file(&leases, DNSMASQ_LEASE_FILE);
    when_true_trace(rc != 0, exit, ERROR, "Couln't write the leases to %s", DNSMASQ_LEASE_FILE);

    rc = dnsmasq_loadconfig();
    when_failed_trace(rc, exit, ERROR, "Unable to reload dnsmasq config.");

exit:
    amxc_llist_clean(&leases, amxc_string_list_it_free);
    return rc;
}

/**
 * @brief
 * Send a lease to the DHCPv4 plugin.
 *
 * @param lease the lease to add.
 */
static void send_lease(amxc_var_t* lease) {
    amxc_var_t rv;
    int status = 1;

    amxc_var_init(&rv);

    when_null_trace(lease, exit, ERROR, "No lease to send to the plugin.");

    status = amxm_execute_function("self",
                                   MOD_CORE,
                                   "set-lease",
                                   lease,
                                   &rv);
    when_failed_trace(status, exit, ERROR, "Could not send the lease to the plugin %d.", status);
exit:
    amxc_var_clean(&rv);
    return;
}

/**
 * @brief
 * Remove a lease to the DHCPv4 plugin.
 *
 * @param lease the lease to remove.
 */
static void remove_lease(amxc_var_t* lease) {
    amxc_var_t rv;
    int status = 1;

    amxc_var_init(&rv);

    when_null_trace(lease, exit, ERROR, "No lease to send to the plugin.");

    status = amxm_execute_function("self",
                                   MOD_CORE,
                                   "remove-lease",
                                   lease,
                                   &rv);
    when_failed_trace(status, exit, ERROR, "Could not remove the lease on the plugin.");
exit:
    amxc_var_clean(&rv);
    return;
}

/**
 * @brief
 * Start and register functions for the uci module.
 *
 * @return AMXM_CONSTRUCTOR always return 0.
 */
static AMXM_CONSTRUCTOR dhcp_dnsmasq_start(void) {
    int ret = 0;
    amxm_module_t* mod = NULL;

    dnsmasq.so = amxm_so_get_current();
    dnsmasq.launch = false;
    dnsmasq.need_first_start = true;
    dnsmasq.need_start = false;
    amxp_timer_new(&dnsmasq.buffer_timer, dnsmasq_control, NULL);
    amxp_subproc_new(&dnsmasq.subproc);
    amxc_var_new(&dnsmasq.config_htable);
    amxc_var_set_type(dnsmasq.config_htable, AMXC_VAR_ID_HTABLE);
    amxc_var_new(&dnsmasq.static_ht);
    amxc_var_set_type(dnsmasq.static_ht, AMXC_VAR_ID_HTABLE);

    amxm_module_register(&mod, dnsmasq.so, MOD_DNSMASQ);
    amxm_module_add_function(mod, "enable-dhcp-pool", enable_dhcp_pool_dnsmasq);
    amxm_module_add_function(mod, "disable-dhcp-pool", disable_dhcp_pool_dnsmasq);
    amxm_module_add_function(mod, "enable-static-ip", enable_dhcp_static_ip_dnsmasq);
    amxm_module_add_function(mod, "disable-static-ip", disable_dhcp_static_ip_dnsmasq);
    amxm_module_add_function(mod, "start-back-end", start_dnsmasq);
    amxm_module_add_function(mod, "stop-back-end", stop_dnsmasq);
    amxm_module_add_function(mod, "clean-leases", clean_leases);
    amxm_module_add_function(mod, "update-dhcp-leases", update_dhcp_leases_dnsmasq);

    dnsmasq_init(send_lease, remove_lease);
    ret = init_dnsmasq_dir();
    when_failed_trace(ret, exit, ERROR, "Failed to create dnsmasq config dir");

exit:
    return ret;
}

/**
 * @brief
 * Stop and clean the uci module.
 *
 * @return AMXM_DESTRUCTOR always return 0.
 */
static AMXM_DESTRUCTOR dhcp_dnsmasq_stop(void) {
    dnsmasq.so = NULL;
    FILE* fd = NULL;
    if(amxp_timer_get_state(dnsmasq.buffer_timer) == amxp_timer_started) {
        amxp_timer_stop(dnsmasq.buffer_timer);
    }
    dnsmasq.last_cmd = STOP_DNSMASQ_CMD;
    dnsmasq_control(NULL, NULL);
    fd = fopen(DNSMASQ_CONF_FILE, "w+");
    if(fd != NULL) {
        fclose(fd);
    }
    remove(DNSMASQ_CONF_DIR);
    dnsmasq_clean();
    amxp_slot_disconnect_all(dnsmasq_stopped);
    amxc_var_delete(&dnsmasq.config_htable);
    amxc_var_delete(&dnsmasq.static_ht);
    dnsmasq.launch = false;
    dnsmasq.need_first_start = true;
    dnsmasq.need_start = false;
    amxp_subproc_delete(&dnsmasq.subproc);
    amxp_timer_delete(&dnsmasq.buffer_timer);
    return 0;
}
