/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_transaction.h>

#include <amxut/amxut_bus.h>

#include "../common/common.h"
#include "../common/dm_utils.h"
#include "../mocks/mock_amxm.h"
#include "test_methods.h"

#include "../../include_priv/dhcp_method.h"
#include "../../include_priv/dm_dhcp.h"

#define SERVER_PATH "DHCPv4Server."

/**
 * @brief
 * Test done :
 * t-1 :
 *  test : call _setStaticClients() method with list of MACs addresses.
 *  expected : all the static addresses are created with a default Yiaddr.
 * t-2 :
 *  test : call _rmStaticClients() method with the same list of MACs addresses.
 *  expected : all the static addresses in the list given are removed.
 * t-3 :
 *  test : call _setStaticClients() method with wrong argument
 *  expected : call fail
 * t-4 :
 *  test : call _rmStaticClient() methode with wrong argument
 */
void test_static_clients(UNUSED void** state) {
    amxc_var_t data;
    amxc_var_t ret;
    amxc_var_t* mac_list = NULL;
    amxd_object_t* obj = NULL;

    amxc_var_init(&data);
    amxc_var_init(&ret);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);

    mac_list = amxc_var_add_key(amxc_llist_t, &data, "MACs", NULL);
    amxc_var_add(cstring_t, mac_list, "11:22:33:44:55:66");
    amxc_var_add(cstring_t, mac_list, "77:88:99:11:22:33");

    obj = amxd_dm_findf(amxut_bus_dm(), SERVER_PATH "Pool.dummy-1.");
    assert_non_null(obj);

    // t-1
    assert_int_equal(_setStaticClients(obj, NULL, &data, &ret), amxd_status_ok);
    expect_amxm_execute("mod-dhcp-dnsmasq", "enable-static-ip", NULL, 2);
    amxut_bus_handle_events();

    assert_dm_check_param(cstring_t, SERVER_PATH "Pool.dummy-1.StaticAddress.1.", "Chaddr", "11:22:33:44:55:66");
    assert_dm_check_param(cstring_t, SERVER_PATH "Pool.dummy-1.StaticAddress.2.", "Chaddr", "77:88:99:11:22:33");

    // t-2
    assert_int_equal(_rmStaticClients(obj, NULL, &data, &ret), amxd_status_ok);
    expect_amxm_execute("mod-dhcp-dnsmasq", "disable-static-ip", NULL, 2);
    amxut_bus_handle_events();

    obj = amxd_dm_findf(amxut_bus_dm(), SERVER_PATH "Pool.dummy-1.StaticAddress.1.");
    assert_null(obj);
    obj = amxd_dm_findf(amxut_bus_dm(), SERVER_PATH "Pool.dummy-1.StaticAddress.2.");
    assert_null(obj);

    // t-3
    amxc_var_clean(&data);
    amxc_var_init(&data);
    amxc_var_set_type(&data, AMXC_VAR_ID_CSTRING);
    amxc_var_set(cstring_t, &data, "11:22:33:44:55:66");
    assert_int_equal(_setStaticClients(obj, NULL, &data, &ret), amxd_status_unknown_error);

    // t-4
    assert_int_equal(_rmStaticClients(obj, NULL, &data, &ret), amxd_status_unknown_error);

    amxc_var_clean(&data);
    amxc_var_clean(&ret);
}

/**
 * @brief
 * Test done :
 * t-1 :
 *  test : send a list of mac to be clean when there is a client with a corresponding MAC address
 *  expected : the client is removed and the back end leases are cleaned
 * t-2 :
 *  test : send an empty list of MAC address to be cleaned
 *  expected : all the clients are removed and the clients are cleaned in the back-end
 */
void test_clients_leases_clean(UNUSED void** state) {
    amxc_var_t lease_data;
    amxc_var_t data;
    amxc_var_t ret;
    amxc_var_t* mac_list = NULL;
    amxd_object_t* obj = NULL;

    amxc_var_init(&data);
    amxc_var_init(&ret);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);

    creat_lease_arg(&lease_data, "dummy-1", "11:22:33:44:55:66", "192.168.1.1", false);
    set_dhcp_lease(&lease_data);

    creat_lease_arg(&lease_data, "dummy-1", "77:88:99:11:22:33", "192.168.1.2", false);
    set_dhcp_lease(&lease_data);

    creat_lease_arg(&lease_data, "dummy-1", "44:55:66:77:88:99", "192.168.1.3", false);
    set_dhcp_lease(&lease_data);

    obj = amxd_dm_findf(amxut_bus_dm(), SERVER_PATH "Pool.dummy-1.Client.1.");
    assert_non_null(obj);
    assert_dm_check_param(cstring_t, SERVER_PATH "Pool.dummy-1.Client.1.", "Chaddr", "11:22:33:44:55:66");
    obj = amxd_dm_findf(amxut_bus_dm(), SERVER_PATH "Pool.dummy-1.Client.2.");
    assert_non_null(obj);
    assert_dm_check_param(cstring_t, SERVER_PATH "Pool.dummy-1.Client.2.", "Chaddr", "77:88:99:11:22:33");
    obj = amxd_dm_findf(amxut_bus_dm(), SERVER_PATH "Pool.dummy-1.Client.3.");
    assert_non_null(obj);
    assert_dm_check_param(cstring_t, SERVER_PATH "Pool.dummy-1.Client.3.", "Chaddr", "44:55:66:77:88:99");

    obj = amxd_dm_findf(amxut_bus_dm(), SERVER_PATH "Pool.dummy-1.");
    assert_non_null(obj);

    // t-1
    mac_list = amxc_var_add_key(amxc_llist_t, &data, "MACs", NULL);
    amxc_var_add(cstring_t, mac_list, "11:22:33:44:55:66");

    expect_amxm_execute("mod-dhcp-dnsmasq", "clean-leases", NULL, 1);
    _cleanClientLeases(obj, NULL, &data, &ret);
    amxut_bus_handle_events();

    obj = amxd_dm_findf(amxut_bus_dm(), SERVER_PATH "Pool.dummy-1.Client.1.");
    assert_null(obj);
    obj = amxd_dm_findf(amxut_bus_dm(), SERVER_PATH "Pool.dummy-1.Client.2.");
    assert_non_null(obj);
    assert_dm_check_param(cstring_t, SERVER_PATH "Pool.dummy-1.Client.2.", "Chaddr", "77:88:99:11:22:33");
    obj = amxd_dm_findf(amxut_bus_dm(), SERVER_PATH "Pool.dummy-1.Client.3.");
    assert_non_null(obj);
    assert_dm_check_param(cstring_t, SERVER_PATH "Pool.dummy-1.Client.3.", "Chaddr", "44:55:66:77:88:99");

    // t-2
    amxc_var_clean(&data);
    amxc_var_init(&data);

    obj = amxd_dm_findf(amxut_bus_dm(), SERVER_PATH "Pool.dummy-1.");
    assert_non_null(obj);

    expect_amxm_execute("mod-dhcp-dnsmasq", "clean-leases", NULL, 1);
    _cleanClientLeases(obj, NULL, &data, &ret);
    amxut_bus_handle_events();

    obj = amxd_dm_findf(amxut_bus_dm(), SERVER_PATH "Pool.dummy-1.Client.2.");
    assert_null(obj);
    obj = amxd_dm_findf(amxut_bus_dm(), SERVER_PATH "Pool.dummy-1.Client.3.");
    assert_null(obj);

    amxc_var_clean(&data);
    amxc_var_clean(&ret);
}