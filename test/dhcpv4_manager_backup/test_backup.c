/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxc/amxc_macros.h>
#include <amxd/amxd_transaction.h>

#include <amxut/amxut_bus.h>

#include "../common/common.h"
#include "../common/dm_utils.h"
#include "../mocks/mock_netmodel.h"
#include "../mocks/mock_amxm.h"

#include "test_backup.h"
#include "dm_dhcp.h"
#include "dhcp_backup.h"

#define SERVER_PATH "DHCPv4Server."

static void add_static_address(amxc_var_t* lan, const char* inst_name, const char* yiaddr, const char* chaddr) {
    amxc_var_t* inst;
    amxc_var_t* static_addr;
    inst = amxc_var_add(amxc_htable_t, lan, NULL);
    static_addr = amxc_var_add_key(amxc_htable_t, inst, inst_name, NULL);
    amxc_var_add_key(cstring_t, static_addr, "Yiaddr", yiaddr);
    amxc_var_add_key(cstring_t, static_addr, "Chaddr", chaddr);
    amxc_var_add_key(bool, static_addr, "Enable", true);
}

static void add_ipv4address(amxc_var_t* ipv4, const char* inst_name, const char* address, const char* lease_time) {
    amxc_var_t* inst;
    amxc_var_t* ipv4_data;
    inst = amxc_var_add(amxc_htable_t, ipv4, NULL);
    ipv4_data = amxc_var_add_key(amxc_htable_t, inst, inst_name, NULL);
    amxc_var_add_key(cstring_t, ipv4_data, "IPAddress", address);
    amxc_var_add_key(cstring_t, ipv4_data, "LeaseTimeRemaining", lease_time);
}

static void add_new_option(amxc_var_t* option, const char* option_inst, uint8_t tag, const char* value) {
    amxc_var_t* inst;
    amxc_var_t* option_data;
    inst = amxc_var_add(amxc_htable_t, option, NULL);
    option_data = amxc_var_add_key(amxc_htable_t, inst, option_inst, NULL);
    amxc_var_add_key(uint8_t, option_data, "Tag", tag);
    amxc_var_add_key(cstring_t, option_data, "Value", value);
}

static void create_dummy_backup_data(amxc_var_t* data) {
    amxc_var_t* set_data = NULL;
    amxc_var_t* hgwconfig = NULL;
    amxc_var_t* dhcpv4_server = NULL;
    amxc_var_t* pool = NULL;
    amxc_var_t* pool_inst_1 = NULL;
    amxc_var_t* lan = NULL;
    amxc_var_t* static_addr = NULL;
    amxc_var_t* client = NULL;
    amxc_var_t* client_inst_1 = NULL;
    amxc_var_t* lease = NULL;
    amxc_var_t* ipv4 = NULL;
    amxc_var_t* option = NULL;

    assert_non_null(data);

    amxc_var_set_type(data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, data, "type", "upc");
    amxc_var_add_key(cstring_t, data, "version", "1.0");

    set_data = amxc_var_add_key(amxc_htable_t, data, "set", NULL);
    hgwconfig = amxc_var_add_key(amxc_htable_t, set_data, "hgwconfig", NULL);
    dhcpv4_server = amxc_var_add_key(amxc_htable_t, hgwconfig, "DHCPv4Server", NULL);

    pool = amxc_var_add_key(amxc_llist_t, dhcpv4_server, "Pool", NULL);
    pool_inst_1 = amxc_var_add(amxc_htable_t, pool, NULL);

    lan = amxc_var_add_key(amxc_htable_t, pool_inst_1, "dummy-1", NULL);
    amxc_var_add_key(int32_t, lan, "LeaseTime", 22222);

    static_addr = amxc_var_add_key(amxc_llist_t, lan, "StaticAddress", NULL);
    add_static_address(static_addr, "test_1", "192.168.1.103", "00:00:00:00:00:03");
    add_static_address(static_addr, "test_2", "192.168.1.104", "00:00:00:00:00:04");


    client = amxc_var_add_key(amxc_llist_t, lan, "Client", NULL);
    client_inst_1 = amxc_var_add(amxc_htable_t, client, NULL);
    lease = amxc_var_add_key(amxc_htable_t, client_inst_1, "client-00:00:00:00:00:01", NULL);

    ipv4 = amxc_var_add_key(amxc_llist_t, lease, "IPv4Address", NULL);
    add_ipv4address(ipv4, "1", "192.168.1.3", "2023-11-20T23:48:03.714926513Z");
    add_ipv4address(ipv4, "2", "192.168.1.45", "2023-11-23T23:48:03.714926513Z");

    option = amxc_var_add_key(amxc_llist_t, lease, "Option", NULL);
    add_new_option(option, "1", 50, "c0a8012d");
    add_new_option(option, "2", 61, "01000000000001");
    add_new_option(option, "3", 53, "03");
    add_new_option(option, "4", 54, "c0a80101");
    add_new_option(option, "5", 55, "0102060c0f1a1c79032128292a77f9fc11");
    add_new_option(option, "6", 57, "0240");
    add_new_option(option, "7", 12, "746573745f686f73746e616d65");

}

int test_backup_teardown(void** state) {
    // all the pool configuration will be removed ( 2 dummy pools )
    expect_amxm_execute("fw", "delete_service", NULL, 1);
    expect_amxm_execute("mod-dhcp-dnsmasq", "disable-dhcp-pool", NULL, 1);
    assert_dm_rm_inst("DHCPv4Server.Pool.", "dummy-1", 0);
    assert_dm_rm_inst("DHCPv4Server.Pool.", "dummy-2", 0);
    amxut_bus_handle_events();

    return test_common_teardown(state);
}

void test_import_func_available(UNUSED void** state) {
    fprintf(stderr, "TEST-ENTER: %s\n", __func__);
    amxd_function_t* func = NULL;
    amxd_object_t* dhcpv4s_client = amxd_dm_findf(amxut_bus_dm(), SERVER_PATH);
    assert_non_null(dhcpv4s_client);

    amxc_llist_for_each(it, (&dhcpv4s_client->functions)) {
        func = amxc_llist_it_get_data(it, amxd_function_t, it);
        const char* func_name = amxd_function_get_name(func);
        fprintf(stderr, "TEST-DEBUG: func:[%s]\n", func_name);
    }

    assert_non_null(amxd_object_get_function(dhcpv4s_client, "Import"));
    fprintf(stderr, "TEST-EXIT: %s\n", __func__);
}

void test_import_dummy_data(UNUSED void** state) {
    fprintf(stderr, "TEST-ENTER: %s\n", __func__);
    int lease_time = 0;
    char* str_tmp = NULL;
    amxc_var_t backup;
    amxc_var_t data;
    amxc_var_t ret;
    amxd_object_t* dhcpv4s = amxd_dm_findf(amxut_bus_dm(), SERVER_PATH);
    amxd_object_t* inst = NULL;
    amxd_object_t* obj = NULL;

    assert_non_null(dhcpv4s);

    amxc_var_init(&backup);
    amxc_var_init(&data);
    amxc_var_init(&ret);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);

    create_dummy_backup_data(&backup);
    amxc_var_add_key(amxc_htable_t, &data, "data", amxc_var_constcast(amxc_htable_t, &backup));

    expect_amxm_execute("mod-dhcp-dnsmasq", "update-dhcp-leases", NULL, 1);
    expect_amxm_execute("mod-dhcp-dnsmasq", "enable-static-ip", NULL, 6);

    assert_int_equal(_Import(dhcpv4s, NULL, &data, &ret), 0);
    amxut_bus_handle_events();

    obj = amxd_dm_findf(amxut_bus_dm(), "DHCPv4Server.Pool.dummy-1");
    lease_time = amxd_object_get_value(int32_t, obj, "LeaseTime", NULL);
    assert_int_equal(lease_time, 22222);


    obj = amxd_dm_findf(amxut_bus_dm(), "DHCPv4Server.Pool.dummy-1.StaticAddress.");
    inst = amxd_object_get_instance(obj, "test_1", 0);
    str_tmp = amxd_object_get_value(cstring_t, inst, "Yiaddr", NULL);
    assert_string_equal(str_tmp, "192.168.1.103");
    free(str_tmp);
    str_tmp = amxd_object_get_value(cstring_t, inst, "Chaddr", NULL);
    assert_string_equal(str_tmp, "00:00:00:00:00:03");
    free(str_tmp);

    inst = amxd_object_get_instance(obj, "test_2", 0);
    str_tmp = amxd_object_get_value(cstring_t, inst, "Yiaddr", NULL);
    assert_string_equal(str_tmp, "192.168.1.104");
    free(str_tmp);
    str_tmp = amxd_object_get_value(cstring_t, inst, "Chaddr", NULL);
    assert_string_equal(str_tmp, "00:00:00:00:00:04");
    free(str_tmp);

    expect_amxm_execute("mod-dhcp-dnsmasq", "disable-static-ip", NULL, 2);
    assert_dm_rm_inst(SERVER_PATH "Pool.dummy-1.StaticAddress", "test_1", 0);
    assert_dm_rm_inst(SERVER_PATH "Pool.dummy-1.StaticAddress", "test_2", 0);
    amxut_bus_handle_events();

    amxc_var_clean(&backup);
    amxc_var_clean(&data);
    amxc_var_clean(&ret);
    fprintf(stderr, "TEST-EXIT: %s\n", __func__);
}