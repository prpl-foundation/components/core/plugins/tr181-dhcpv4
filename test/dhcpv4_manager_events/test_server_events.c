/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_transaction.h>

#include <amxut/amxut_bus.h>
#include <amxut/amxut_timer.h>

#include "../common/common.h"
#include "../common/dm_utils.h"
#include "../mocks/mock_amxm.h"
#include "../mocks/mock_netmodel.h"
#include "test_events.h"

#include "../../include_priv/dm_dhcp.h"
#include "../../include_priv/dhcp_event.h"

#define SERVER_PATH "DHCPv4Server."

/* Not tested :
 *  - check installation of files
 *  - change of back-end module
 */

/**
 * @brief
 * Tests done :
 * t-1 :
 *  test : disable of the whole server
 *  expected : signal send to stop the back-end and pools are disabled
 * t-2 :
 *  test : enable of the whole server
 *  expected : signal send to start the back-end and pools are enabled
 */
void test_enable(UNUSED void** state) {

    // t-1
    assert_dm_set_param(bool, SERVER_PATH, "Enable", false);

    expect_amxm_execute("mod-dhcp-dnsmasq", "stop-back-end", NULL, 1);
    expect_amxm_execute("mod-dhcp-dnsmasq", "disable-dhcp-pool", NULL, 1);
    expect_amxm_execute("fw", "delete_service", NULL, 1);
    expect_amxm_execute("mod-dhcp-dnsmasq", "disable-dhcp-pool", NULL, 1);
    amxut_bus_handle_events();

    // t-2
    assert_dm_set_param(bool, SERVER_PATH, "Enable", true);

    expect_amxm_execute("mod-dhcp-dnsmasq", "start-back-end", NULL, 1);
    amxut_bus_handle_events();

    expect_amxm_execute("fw", "set_service", NULL, 1);
    expect_amxm_execute("mod-dhcp-dnsmasq", "enable-dhcp-pool", NULL, 2);

    mock_netmodel_trigger_call_backs();
    amxut_bus_handle_events();
}

/**
 * @brief
 * Tests done :
 * test : update config of the whole server
 * expected : changing a config trigger a restart of the back-end only if the server is enable
 */
void test_config_changed(UNUSED void** state) {
    amxc_var_t args_ht;

    amxc_var_init(&args_ht);
    amxc_var_set_type(&args_ht, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(bool, &args_ht, "IgnoreIDS", true);
    assert_dm_set_param(bool, SERVER_PATH, "IgnoreIDS", true);

    expect_amxm_execute("mod-dhcp-dnsmasq", "stop-back-end", NULL, 1);
    expect_amxm_execute("mod-dhcp-dnsmasq", "start-back-end", &args_ht, 1);
    amxut_bus_handle_events();

    assert_dm_set_param(bool, SERVER_PATH, "Enable", false);

    expect_amxm_execute("mod-dhcp-dnsmasq", "stop-back-end", NULL, 1);
    expect_amxm_execute("mod-dhcp-dnsmasq", "disable-dhcp-pool", NULL, 1);
    expect_amxm_execute("fw", "delete_service", NULL, 1);
    expect_amxm_execute("mod-dhcp-dnsmasq", "disable-dhcp-pool", NULL, 1);
    amxut_bus_handle_events();

    amxc_var_set(bool, amxc_var_get_key(&args_ht, "IgnoreIDS", AMXC_VAR_FLAG_DEFAULT), false);
    assert_dm_set_param(bool, SERVER_PATH, "IgnoreIDS", false);
    amxut_bus_handle_events();

    assert_dm_set_param(bool, SERVER_PATH, "Enable", true);

    expect_amxm_execute("mod-dhcp-dnsmasq", "start-back-end", &args_ht, 1);
    amxut_bus_handle_events();

    expect_amxm_execute("fw", "set_service", NULL, 1);
    expect_amxm_execute("mod-dhcp-dnsmasq", "enable-dhcp-pool", NULL, 2);

    mock_netmodel_trigger_call_backs();
    amxut_bus_handle_events();

    amxc_var_clean(&args_ht);
}

/**
 * @brief
 * Tests done :
 * test : update all the lease time of the Pools with the new synced time
 * expected : Lease time are updated with the new system time
 */
void test_sys_time_updated(UNUSED void** state) {
    amxc_var_t lease_data;
    amxc_ts_t now;
    amxc_var_t data;
    amxc_var_t* params = NULL;
    amxc_var_t* status = NULL;

    amxc_ts_now(&now);
    amxc_var_init(&data);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);

    creat_lease_arg(&lease_data, "dummy-1", "11:11:11:11:11:11", "192.168.1.2", false);
    set_dhcp_lease(&lease_data);

    // check timing
    assert_dm_check_param(bool, SERVER_PATH "Pool.dummy-1.Client.1.", "Active", true);
    assert_dm_check_param(uint32_t, SERVER_PATH "Pool.dummy-1.", "LeaseTime", 43200);
    assert_dm_check_param(cstring_t, SERVER_PATH "Pool.dummy-1.Client.1.", "Chaddr", "11:11:11:11:11:11");
    assert_dm_check_param(cstring_t, SERVER_PATH "Pool.dummy-1.Client.1.IPv4Address.1.", "IPAddress", "192.168.1.2");
    assert_dm_check_param(amxc_ts_t, SERVER_PATH "Pool.dummy-1.Client.1.IPv4Address.1.", "LastUpdate", &now);
    now.sec += 43200;
    assert_dm_check_param(amxc_ts_t, SERVER_PATH "Pool.dummy-1.Client.1.IPv4Address.1.", "LeaseTimeRemaining", &now);

    // go further in time
    amxut_timer_go_to_future_ms(60 * 1000);
    amxc_ts_now(&now);

    params = amxc_var_add_key(amxc_htable_t, &data, "parameters", NULL);
    status = amxc_var_add_key(amxc_htable_t, params, "Status", NULL);
    amxc_var_add_key(cstring_t, status, "to", "Synchronized");

    _dhcp_server_sys_time_updated(NULL, &data, NULL);
    amxut_bus_handle_events();

    assert_dm_check_param(amxc_ts_t, SERVER_PATH "Pool.dummy-1.Client.1.IPv4Address.1.", "LastUpdate", &now);
    now.sec += 43200;
    assert_dm_check_param(amxc_ts_t, SERVER_PATH "Pool.dummy-1.Client.1.IPv4Address.1.", "LeaseTimeRemaining", &now);

    amxc_var_clean(&data);
}