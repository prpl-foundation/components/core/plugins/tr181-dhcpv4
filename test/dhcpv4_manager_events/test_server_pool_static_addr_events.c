/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_transaction.h>

#include <amxut/amxut_bus.h>
#include <amxut/amxut_timer.h>


#include "../common/common.h"
#include "../common/dm_utils.h"
#include "../mocks/mock_amxm.h"
#include "../mocks/mock_netmodel.h"
#include "../mocks/mock_amxb.h"
#include "test_events.h"

#include "../../include_priv/dm_dhcp.h"

#define SERVER_PATH "DHCPv4Server."

/**
 * @brief
 * Test done :
 *  test : change a parameter of the static ip
 *  expected : the back-end is notified of the change
 */
void test_static_ip_changed(UNUSED void** state) {
    amxc_var_t ip_addr;
    amxc_var_t* data = NULL;

    amxc_var_init(&ip_addr);
    amxc_var_set_type(&ip_addr, AMXC_VAR_ID_HTABLE);

    // static ip from known client change test
    assert_dm_check_param(bool, SERVER_PATH "Pool.dummy-1.StaticAddress.1.", "Enable", true);
    assert_dm_check_param(cstring_t, SERVER_PATH "Pool.dummy-1.StaticAddress.1.", "Yiaddr", "192.168.1.10");
    assert_dm_check_param(cstring_t, SERVER_PATH "Pool.dummy-1.StaticAddress.1.", "Chaddr", "11:22:33:44:55:66");

    assert_dm_set_param(cstring_t, SERVER_PATH "Pool.dummy-1.StaticAddress.1.", "Yiaddr", "192.168.1.11");

    data = amxc_var_add_key(amxc_htable_t, &ip_addr, "addr", NULL);
    amxc_var_add_key(cstring_t, data, "Chaddr", "11:22:33:44:55:66");
    amxc_var_add_key(cstring_t, data, "Yiaddr", "192.168.1.11");
    amxc_var_add_key(cstring_t, data, "Alias", "cpe-StaticAddress-1");
    amxc_var_add_key(bool, data, "Enable", 1);

    expect_amxm_execute("mod-dhcp-dnsmasq", "enable-static-ip", &ip_addr, 1);
    amxut_bus_handle_events();

    assert_dm_check_param(cstring_t, SERVER_PATH "Pool.dummy-1.StaticAddress.1.", "Yiaddr", "192.168.1.11");

    amxc_var_clean(&ip_addr);
}

/**
 * @brief
 * Test done :
 * t-1 :
 *   test : add a static address that is already present in one static lease of the pools
 *   expected : static lease is not created
 * t-2 :
 *   test : add a static lease that is out of the pool range
 *   expected : static lease is not created
 * t-3 :
 *   test : add a static lease that is already given to a client
 *   expected : static lease is not created
 * t-4 :
 *    test ; add a static lease that have the same ip than the router
 *    expected : static lease is not created
 * t-5 :
 *   test : add a static lease that is already given to a client but with the same MAC
 *   expected : static lease is created and IsStatic should be true
 */
void test_overlap_static(UNUSED void** state) {
    // t-1
    assert_dm_check_param(cstring_t, SERVER_PATH "Pool.dummy-1.StaticAddress.1.", "Yiaddr", "192.168.1.11");
    add_static_ip(SERVER_PATH "Pool.dummy-1.StaticAddress.", "192.168.1.11", "12:23:34:45:56:67", amxd_status_invalid_value);
    amxut_bus_handle_events();
    assert_dm_check_param(cstring_t, SERVER_PATH "Pool.dummy-1.StaticAddress.1.", "Yiaddr", "192.168.1.11");
    add_static_ip(SERVER_PATH "Pool.dummy-2.StaticAddress.", "192.168.1.11", "12:23:34:45:56:67", amxd_status_invalid_value);
    amxut_bus_handle_events();

    // t-2
    assert_dm_check_param(cstring_t, SERVER_PATH "Pool.dummy-1.StaticAddress.1.", "Yiaddr", "192.168.1.11");
    assert_dm_check_param(cstring_t, SERVER_PATH "Pool.dummy-1.", "MaxAddress", "192.168.1.100");
    add_static_ip(SERVER_PATH "Pool.dummy-1.StaticAddress.", "192.168.1.250", "12:23:34:45:56:67", amxd_status_invalid_value);
    amxut_bus_handle_events();

    // t-3
    assert_dm_check_param(cstring_t, SERVER_PATH "Pool.dummy-1.Client.3.IPv4Address.1.", "IPAddress", "192.168.1.4");
    add_static_ip(SERVER_PATH "Pool.dummy-1.StaticAddress.", "192.168.1.4", "12:23:34:45:56:67", amxd_status_invalid_value);
    amxut_bus_handle_events();
    assert_dm_check_param(cstring_t, SERVER_PATH "Pool.dummy-1.Client.3.IPv4Address.1.", "IPAddress", "192.168.1.4");
    add_static_ip(SERVER_PATH "Pool.dummy-2.StaticAddress.", "192.168.1.4", "12:23:34:45:56:67", amxd_status_invalid_value);
    amxut_bus_handle_events();

    // t-4
    // netmodel get addr query should return assigned address (hard coded to 192.168.1.1 in the mock)
    add_static_ip(SERVER_PATH "Pool.dummy-1.StaticAddress.", "192.168.1.1", "12:23:34:45:56:67", amxd_status_invalid_value);
    amxut_bus_handle_events();

    // t-5
    assert_dm_check_param(cstring_t, SERVER_PATH "Pool.dummy-1.Client.3.IPv4Address.1.", "IPAddress", "192.168.1.4");
    assert_dm_check_param(cstring_t, SERVER_PATH "Pool.dummy-1.Client.3.", "Chaddr", "77:88:99:11:22:33");
    assert_dm_check_param(bool, SERVER_PATH "Pool.dummy-1.Client.3.IPv4Address.1.", "IsStatic", false);
    add_static_ip(SERVER_PATH "Pool.dummy-1.StaticAddress.", "192.168.1.4", "77:88:99:11:22:33", amxd_status_ok);
    expect_amxm_execute("mod-dhcp-dnsmasq", "enable-static-ip", NULL, 1);
    amxut_bus_handle_events();
    assert_dm_check_param(bool, SERVER_PATH "Pool.dummy-1.Client.3.IPv4Address.1.", "IsStatic", true);
}

/**
 * @brief
 * Test done :
 * t-1 :
 *    test : add a lease whit the same address ip as a static address
 *    expected : the parameter IsStatic of the lease is set to true
 * t-2 :
 *     test : change the lease to a static address ip should also update th IsStatic parameter
 *     done : the parameter IsStatic of the lease is set to true
 */
void test_is_static(UNUSED void** state) {
    amxc_var_t lease_data;

    // t-1
    assert_dm_check_param(cstring_t, SERVER_PATH "Pool.dummy-1.StaticAddress.1.", "Yiaddr", "192.168.1.11");
    assert_dm_check_param(cstring_t, SERVER_PATH "Pool.dummy-1.StaticAddress.1.", "Chaddr", "11:22:33:44:55:66");

    creat_lease_arg(&lease_data, "dummy-1", "11:22:33:44:55:66", "192.168.1.11", false);
    set_dhcp_lease(&lease_data);

    assert_dm_check_param(bool, SERVER_PATH "Pool.dummy-1.Client.4.IPv4Address.1.", "IsStatic", true);

    // t-2
    assert_dm_set_param(cstring_t, SERVER_PATH "Pool.dummy-1.StaticAddress.1.", "Yiaddr", "192.168.1.12");
    expect_amxm_execute("mod-dhcp-dnsmasq", "enable-static-ip", NULL, 1);
    amxut_bus_handle_events();

    assert_dm_check_param(bool, SERVER_PATH "Pool.dummy-1.Client.4.IPv4Address.1.", "IsStatic", false);

    amxc_var_set(cstring_t, GET_ARG(&lease_data, "IPv4Address"), "192.168.1.12");
    creat_lease_arg(&lease_data, "dummy-1", "11:22:33:44:55:66", "192.168.1.12", false);
    set_dhcp_lease(&lease_data);

    assert_dm_check_param(bool, SERVER_PATH "Pool.dummy-1.Client.4.IPv4Address.1.", "IsStatic", true);
}