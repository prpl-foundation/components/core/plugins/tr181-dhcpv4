/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_transaction.h>

#include <amxut/amxut_bus.h>
#include <amxut/amxut_dm.h>
#include <amxut/amxut_timer.h>


#include "../common/common.h"
#include "../common/dm_utils.h"
#include "../mocks/mock_amxm.h"
#include "../mocks/mock_netmodel.h"
#include "../mocks/mock_amxb.h"
#include "test_events.h"

#include "../../include_priv/dm_dhcp.h"

#define SERVER_PATH "DHCPv4Server."

static const bool test_true = true;
static const bool test_false = false;

/* Not tested :
 *  - Change the IPAddress of the client. Expect the DNS host is synced
 *  - Change the Active parameter of the client. Expect the DNS host is synced
 */

/**
 * @brief
 * Tests done :
 *  test : add a client with the send_lease method of the plugin
 *  expected : a client is added to the datamodel
 */
void test_send_lease(UNUSED void** state) {
    amxc_var_t lease_data;

    amxut_dm_param_equals(bool, SERVER_PATH "Pool.dummy-1.", "Enable", true);

    creat_lease_arg(&lease_data, "dummy-1", "11:22:33:44:55:66", "192.168.1.2", false);
    add_lease_option(GET_ARG(&lease_data, "Option"), "42", "74657374");
    set_dhcp_lease(&lease_data);

    amxut_dm_param_equals(bool, SERVER_PATH "Pool.dummy-1.Client.2.", "Active", true);
    amxut_dm_param_equals(cstring_t, SERVER_PATH "Pool.dummy-1.Client.2.", "Chaddr", "11:22:33:44:55:66");
    amxut_dm_param_equals(cstring_t, SERVER_PATH "Pool.dummy-1.Client.2.IPv4Address.1.", "IPAddress", "192.168.1.2");
    amxut_dm_param_equals(uint8_t, SERVER_PATH "Pool.dummy-1.Client.2.Option.1.", "Tag", 42);
    amxut_dm_param_equals(cstring_t, SERVER_PATH "Pool.dummy-1.Client.2.Option.1.", "Value", "74657374");
}

/**
 * @brief
 * Tests done :
 * t-1 :
 *  test : remove a lease of the client with the remove_lease method.
 *      expected : client is still there and inactive but there is no more lease attached to that client
 * t-2 :
 *  test : increase the time to make the client expire ( 2 minutes )
 *  expected : the client is removed
 */
void test_client_lease_expired(UNUSED void** state) {
    amxd_object_t* obj = NULL;
    amxc_var_t lease_data;

    creat_lease_arg(&lease_data, "dummy-1", "11:22:33:44:55:66", "192.168.1.2", false);

    // t-1
    amxut_dm_param_equals(bool, SERVER_PATH "Pool.dummy-1.Client.2.", "Active", true);
    amxut_dm_param_equals(cstring_t, SERVER_PATH "Pool.dummy-1.Client.2.IPv4Address.1.", "IPAddress", "192.168.1.2");
    remove_dhcp_lease(&lease_data);

    amxut_dm_param_equals(bool, SERVER_PATH "Pool.dummy-1.Client.2.", "Active", false);
    obj = amxd_dm_findf(amxut_bus_dm(), SERVER_PATH "Pool.dummy-1.Client.2.IPv4Address.1.");
    assert_null(obj);

    // t-2
    amxut_timer_go_to_future_ms(121 * 1000);
    amxut_bus_handle_events();
    obj = amxd_dm_findf(amxut_bus_dm(), SERVER_PATH "Pool.dummy-1.Client.2.");
    assert_null(obj);
}

/**
 * @brief
 * Tests done :
 * t-1 :
 *  test : add a client with option 12
 *  expected : a SetHost call to DNS is done
 * t-2 :
 *  test : renew client with a different option 12
 *  expect :
 *      client's tag 12 is updated
 *      a SetHost call to DNS is done
 * t-3 :
 *  test : renew client with an empty hostname
 *  expected : the DNS host is removed
 */
void test_client_hostname(UNUSED void** state) {
    amxc_var_t lease_data;
    mock_amxb_call_check_enable();

    amxut_dm_param_equals(bool, SERVER_PATH "Pool.dummy-1.", "Enable", true);

    // t-1: new lease with option 12
    creat_lease_arg(&lease_data, "dummy-1", "77:88:99:11:22:33", "192.168.1.4", false);
    add_lease_option(GET_ARG(&lease_data, "Option"), "12", "746573742d686f7374");

    expect_amxb_call_dns_set_host(&(const amxb_call_dns_set_host_args_t) {
        .ip_address = "192.168.1.4",
        .interface = "Device.IP.Interface.1.",
        .name = "test-host",
        .origin = "DHCPv4",
        .enable = &test_true,
        .exclusive = &test_true,
    });
    set_dhcp_lease(&lease_data);

    amxut_dm_param_equals(uint32_t, SERVER_PATH "Pool.dummy-1.Client.3", "OptionNumberOfEntries", 1);
    amxut_dm_param_equals(cstring_t, SERVER_PATH "Pool.dummy-1.Client.3.Option.[Tag == 12].", "Value", "746573742d686f7374");

    // t-2: renew lease with different option 12
    creat_lease_arg(&lease_data, "dummy-1", "77:88:99:11:22:33", "192.168.1.4", false);
    add_lease_option(GET_ARG(&lease_data, "Option"), "12", "64756d6d792d686f7374");

    expect_amxb_call_dns_set_host(&(const amxb_call_dns_set_host_args_t) {
        .alias = "cpe-X_PRPL-COM_Host-1",
        .name = "dummy-host",
    });
    set_dhcp_lease(&lease_data);

    amxut_dm_param_equals(uint32_t, SERVER_PATH "Pool.dummy-1.Client.3", "OptionNumberOfEntries", 1);
    amxut_dm_param_equals(cstring_t, SERVER_PATH "Pool.dummy-1.Client.3.Option.[Tag == 12].", "Value", "64756d6d792d686f7374");

    // t-3: renew lease with empty option 12
    creat_lease_arg(&lease_data, "dummy-1", "77:88:99:11:22:33", "192.168.1.4", false);
    add_lease_option(GET_ARG(&lease_data, "Option"), "12", "");

    expect_amxb_call_dns_remove_host("cpe-X_PRPL-COM_Host-1");
    set_dhcp_lease(&lease_data);

    amxut_dm_param_equals(uint32_t, SERVER_PATH "Pool.dummy-1.Client.3", "OptionNumberOfEntries", 1);
    amxut_dm_param_equals(cstring_t, SERVER_PATH "Pool.dummy-1.Client.3.Option.[Tag == 12].", "Value", "");

    mock_amxb_call_check_disable();
}

/**
 * @brief
 * Tests done :
 * t-1 :
 *  test : create a client lease with 2 meaningless(/unassigned) options (110 & 111)
 *  expected : the client to have the 2 DM options with the right values
 * t-2 :
 *  test : renew a client with one option(110) value changed and one option replaced(111 => 115).
 *  expected : the client to have the 2 DM options with the right values (index of option 110 unchanged)
 */
void test_client_options_can_change(UNUSED void** state) {
    amxc_var_t lease_data;
    uint32_t index = 0;

    amxut_dm_param_equals(bool, SERVER_PATH "Pool.dummy-1.", "Enable", true);

    // t-1
    creat_lease_arg(&lease_data, "dummy-1", "77:88:99:11:22:33", "192.168.1.4", false);
    add_lease_option(GET_ARG(&lease_data, "Option"), "110", "736f6d657468696e67"); // 110
    add_lease_option(GET_ARG(&lease_data, "Option"), "111", "6f74686572");         // 111
    set_dhcp_lease(&lease_data);

    amxut_dm_param_equals(uint32_t, SERVER_PATH "Pool.dummy-1.Client.3", "OptionNumberOfEntries", 2);
    amxut_dm_param_equals(cstring_t, SERVER_PATH "Pool.dummy-1.Client.3.Option.[Tag == 110].", "Value", "736f6d657468696e67");
    amxut_dm_param_equals(cstring_t, SERVER_PATH "Pool.dummy-1.Client.3.Option.[Tag == 111].", "Value", "6f74686572");
    index = amxd_object_get_index(amxd_dm_findf(amxut_bus_dm(), SERVER_PATH "Pool.dummy-1.Client.3.Option.[Tag == 110]."));

    // t-2
    creat_lease_arg(&lease_data, "dummy-1", "77:88:99:11:22:33", "192.168.1.4", false);
    add_lease_option(GET_ARG(&lease_data, "Option"), "110", "76616c75652d6368616e676564");   // 110
    add_lease_option(GET_ARG(&lease_data, "Option"), "115", "6f7074696f6e2d6368616e676564"); // 115
    set_dhcp_lease(&lease_data);

    amxut_dm_param_equals(uint32_t, SERVER_PATH "Pool.dummy-1.Client.3", "OptionNumberOfEntries", 2);
    amxut_dm_param_equals(cstring_t, SERVER_PATH "Pool.dummy-1.Client.3.Option.[Tag == 110].", "Value", "76616c75652d6368616e676564");
    amxut_dm_param_equals(cstring_t, SERVER_PATH "Pool.dummy-1.Client.3.Option.[Tag == 115].", "Value", "6f7074696f6e2d6368616e676564");
    assert_int_equal(index, amxd_object_get_index(amxd_dm_findf(amxut_bus_dm(), SERVER_PATH "Pool.dummy-1.Client.3.Option.[Tag == 110].")));
}

/**
 * @brief
 * Tests done :
 * t-1 :
 *  test : add a client with option 12
 *  expected : a SetHost call to DNS is done
 * t-2 :
 *  test : renew it without option 12
 *  expected : the DNS host is removed
 */
void test_client_hostname_option_can_be_removed(UNUSED void** state) {
    amxc_var_t lease_data;
    mock_amxb_call_check_enable();

    // t-1: add lease with option 12
    creat_lease_arg(&lease_data, "dummy-1", "77:88:99:11:22:33", "192.168.1.4", false);
    add_lease_option(GET_ARG(&lease_data, "Option"), "12", "64756d6d792d686f7374");

    expect_amxb_call_dns_set_host(&(const amxb_call_dns_set_host_args_t) {
        .ip_address = "192.168.1.4",
        .interface = "Device.IP.Interface.1.",
        .name = "dummy-host",
        .origin = "DHCPv4",
        .enable = &test_true,
        .exclusive = &test_true,
    });
    set_dhcp_lease(&lease_data);

    amxut_dm_param_equals(uint32_t, SERVER_PATH "Pool.dummy-1.Client.3", "OptionNumberOfEntries", 1);
    amxut_dm_param_equals(cstring_t, SERVER_PATH "Pool.dummy-1.Client.3.Option.[Tag == 12].", "Value", "64756d6d792d686f7374");

    // t-2: Renew without option 12
    creat_lease_arg(&lease_data, "dummy-1", "77:88:99:11:22:33", "192.168.1.4", false);
    // No option 12 added

    expect_amxb_call_dns_remove_host("cpe-X_PRPL-COM_Host-2");
    set_dhcp_lease(&lease_data);

    amxut_dm_param_equals(uint32_t, SERVER_PATH "Pool.dummy-1.Client.3", "OptionNumberOfEntries", 0);

    mock_amxb_call_check_disable();
}

/**
 * @brief
 * Tests done :
 * t-1 :
 *  test : set a lease A with option 12
 *  expected : a SetHost call to DNS is done with IP A
 * t-2 :
 *  test : set a lease B for the same client with a different IP
 *  expected : a RemoveIPFromHost call to DNS is done with both IP A
 *             a SetHost call to DNS is done with IP B
 * t-3 :
 *  test : remove lease B
 *  expected : dns host is disabled
 */
void test_client_hostname_sync_ip_with_dns_host(UNUSED void** state) {
    amxc_var_t lease_data;
    mock_amxb_call_check_enable();

    // t-1
    creat_lease_arg(&lease_data, "dummy-1", "77:88:99:11:22:33", "192.168.1.4", false);
    add_lease_option(GET_ARG(&lease_data, "Option"), "12", "64756d6d792d686f7374");

    expect_amxb_call_dns_set_host(&(const amxb_call_dns_set_host_args_t) {
        .ip_address = "192.168.1.4",
        .interface = "Device.IP.Interface.1.",
        .name = "dummy-host",
        .origin = "DHCPv4",
        .enable = &test_true,
        .exclusive = &test_true,
    });
    set_dhcp_lease(&lease_data);

    amxut_dm_param_equals(cstring_t, SERVER_PATH "Pool.dummy-1.Client.3.IPv4Address.1.", "IPAddress", "192.168.1.4");
    amxut_dm_param_equals(uint32_t, SERVER_PATH "Pool.dummy-1.Client.3", "OptionNumberOfEntries", 1);
    amxut_dm_param_equals(cstring_t, SERVER_PATH "Pool.dummy-1.Client.3.Option.[Tag == 12].", "Value", "64756d6d792d686f7374");

    // t-2
    creat_lease_arg(&lease_data, "dummy-1", "77:88:99:11:22:33", "192.168.1.5", false);
    add_lease_option(GET_ARG(&lease_data, "Option"), "12", "64756d6d792d686f7374");

    expect_amxb_call_dns_rm_host_ip(&(const amxb_call_dns_set_host_args_t) {
        .alias = "cpe-X_PRPL-COM_Host-3",
        .ip_address = "192.168.1.4",
    });
    expect_amxb_call_dns_set_host(&(const amxb_call_dns_set_host_args_t) {
        .alias = "cpe-X_PRPL-COM_Host-3",
        .ip_address = "192.168.1.5",
    });
    set_dhcp_lease(&lease_data);

    amxut_dm_param_equals(uint32_t, SERVER_PATH "Pool.dummy-1.Client.3", "OptionNumberOfEntries", 1);
    amxut_dm_param_equals(cstring_t, SERVER_PATH "Pool.dummy-1.Client.3.Option.[Tag == 12].", "Value", "64756d6d792d686f7374");
    amxut_dm_param_equals(cstring_t, SERVER_PATH "Pool.dummy-1.Client.3.IPv4Address.1.", "IPAddress", "192.168.1.5");

    // t-3
    creat_lease_arg(&lease_data, "dummy-1", "77:88:99:11:22:33", "192.168.1.5", false);
    add_lease_option(GET_ARG(&lease_data, "Option"), "12", "64756d6d792d686f7374");

    expect_amxb_call_dns_rm_host_ip(&(const amxb_call_dns_set_host_args_t) {
        .alias = "cpe-X_PRPL-COM_Host-3",
        .ip_address = "192.168.1.5",
    });
    expect_amxb_call_dns_set_host(&(const amxb_call_dns_set_host_args_t) {
        .alias = "cpe-X_PRPL-COM_Host-3",
        .enable = &test_false,
        .ip_address = "",
    });
    remove_dhcp_lease(&lease_data);

    amxut_dm_param_equals(uint32_t, SERVER_PATH "Pool.dummy-1.Client.3", "OptionNumberOfEntries", 1);
    amxut_dm_param_equals(cstring_t, SERVER_PATH "Pool.dummy-1.Client.3.Option.[Tag == 12].", "Value", "64756d6d792d686f7374");
    assert_null(amxd_dm_findf(amxut_bus_dm(), SERVER_PATH "Pool.dummy-1.Client.3.IPv4Address.1."));

    mock_amxb_call_check_disable();

    // recreate client with IP for future tests
    creat_lease_arg(&lease_data, "dummy-1", "77:88:99:11:22:33", "192.168.1.4", false);
    set_dhcp_lease(&lease_data);
    amxut_dm_param_equals(uint32_t, SERVER_PATH "Pool.dummy-1.Client.3", "OptionNumberOfEntries", 0);
    amxut_dm_param_equals(cstring_t, SERVER_PATH "Pool.dummy-1.Client.3.IPv4Address.1.", "IPAddress", "192.168.1.4");
}

/**
 * @brief
 * Test done :
 * test : add a new static ip when the parameter AllowedDevices == Known
 * expected : the new mac address is forwarded to the back-end
 */
void test_known_client_changed(UNUSED void** state) {
    amxc_var_t pool_param;
    amxc_var_t* mac_table;

    amxc_var_init(&pool_param);
    amxc_var_set_type(&pool_param, AMXC_VAR_ID_HTABLE);

    amxut_dm_param_equals(cstring_t, SERVER_PATH "Pool.dummy-1.", "AllowedDevices", "Known");
    amxut_dm_param_equals(bool, SERVER_PATH "Pool.dummy-1.Client.3.", "Active", true);
    amxut_dm_param_equals(cstring_t, SERVER_PATH "Pool.dummy-1.Client.3.", "Chaddr", "77:88:99:11:22:33");

    add_static_ip(SERVER_PATH "Pool.dummy-1.StaticAddress.", "192.168.1.10", "11:22:33:44:55:66", 0);
    expect_amxm_execute("mod-dhcp-dnsmasq", "enable-static-ip", NULL, 1);
    amxut_bus_handle_events();

    amxut_dm_param_equals(bool, SERVER_PATH "Pool.dummy-1.StaticAddress.1.", "Enable", true);
    amxut_dm_param_equals(cstring_t, SERVER_PATH "Pool.dummy-1.StaticAddress.1.", "Chaddr", "11:22:33:44:55:66");

    mac_table = amxc_var_add_key(amxc_htable_t, &pool_param, "MACs", NULL);
    amxc_var_add_key(bool, mac_table, "77:88:99:11:22:33", true);
    amxc_var_add_key(bool, mac_table, "11:22:33:44:55:66", true);
    amxc_var_add_key(bool, mac_table, "11:11:11:11:11:11", true);

    expect_amxm_execute("mod-dhcp-dnsmasq", "enable-dhcp-pool", NULL, 1);        // dummy-2
    expect_amxm_execute("mod-dhcp-dnsmasq", "enable-dhcp-pool", &pool_param, 1); // dummy-1 + Macs
    mock_netmodel_trigger_call_backs();
    amxut_bus_handle_events();

    // put back to "All" for future test
    expect_amxm_execute("mod-dhcp-dnsmasq", "enable-dhcp-pool", NULL, 1); // dummy-1 - Macs
    assert_dm_set_param(cstring_t, SERVER_PATH "Pool.dummy-1.", "AllowedDevices", "All");
    amxut_bus_handle_events();
    amxc_var_clean(&pool_param);
}