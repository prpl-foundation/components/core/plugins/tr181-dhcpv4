/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <setjmp.h>
#include <stdarg.h>
#include <stddef.h>
#include <cmocka.h>
#include <stdlib.h>
#include <stdio.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>

#include <amxp/amxp.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_dm.h>

#include <amxut/amxut_bus.h>

#include "dm_utils.h"

static int test_dm_set_param(const char* path, const char* param, amxc_var_t* val) {
    amxd_trans_t trans;
    int rv = -1;

    when_failed(rv = amxd_trans_init(&trans), exit);
    when_failed(rv = amxd_trans_select_pathf(&trans, path), exit);
    when_failed(rv = amxd_trans_set_param(&trans, param, val), exit);
    when_failed(rv = amxd_trans_apply(&trans, amxut_bus_dm()), exit);
exit:
    amxd_trans_clean(&trans);
    return rv;
}

void test_dm_set_param_bool(const char* path, const char* param, bool value) {
    amxc_var_t val;
    amxc_var_init(&val);
    amxc_var_set(bool, &val, value);

    assert_int_equal(test_dm_set_param(path, param, &val), 0);

    amxc_var_clean(&val);
}

void test_dm_set_param_uint32_t(const char* path, const char* param, uint32_t value) {
    amxc_var_t val;
    amxc_var_init(&val);
    amxc_var_set(uint32_t, &val, value);

    assert_int_equal(test_dm_set_param(path, param, &val), 0);

    amxc_var_clean(&val);
}

void test_dm_set_param_cstring_t(const char* path, const char* param, const char* value) {
    amxc_var_t val;
    amxc_var_init(&val);
    amxc_var_set(cstring_t, &val, value);

    assert_int_equal(test_dm_set_param(path, param, &val), 0);

    amxc_var_clean(&val);
}

void test_dm_check_param_bool(const char* path, const char* param, bool value) {
    amxd_object_t* object = amxd_dm_findf(amxut_bus_dm(), path);
    const amxc_var_t* val = amxd_object_get_param_value(object, param);

    assert_non_null(object);
    assert_non_null(val);

    assert_int_equal(GET_BOOL(val, NULL), value);
}

void test_dm_check_param_uint32_t(const char* path, const char* param, uint32_t value) {
    amxd_object_t* object = amxd_dm_findf(amxut_bus_dm(), path);
    const amxc_var_t* val = amxd_object_get_param_value(object, param);

    assert_non_null(object);
    assert_non_null(val);

    assert_int_equal(GET_UINT32(val, NULL), value);
}

void test_dm_check_param_cstring_t(const char* path, const char* param, const char* value) {
    amxd_object_t* object = amxd_dm_findf(amxut_bus_dm(), path);
    const amxc_var_t* val = amxd_object_get_param_value(object, param);

    assert_non_null(object);
    assert_non_null(val);

    assert_string_equal(GET_CHAR(val, NULL), value);
}

void test_dm_check_param_amxc_ts_t(const char* path, const char* param, amxc_ts_t* value) {
    amxd_object_t* object = amxd_dm_findf(amxut_bus_dm(), path);
    const amxc_var_t* val = amxd_object_get_param_value(object, param);
    amxc_ts_t* time = NULL;

    assert_non_null(object);
    assert_non_null(val);

    time = amxc_var_dyncast(amxc_ts_t, val);
    // accept 2 second of difference as same time
    assert_true(((value->sec - 2) <= time->sec) && (time->sec <= (value->sec + 2)));
    free(time);
}

void assert_dm_rm_inst(const char* path, const char* name, int index) {
    amxd_trans_t trans;
    int rv = -1;

    when_failed(rv = amxd_trans_init(&trans), exit);
    when_failed(rv = amxd_trans_select_pathf(&trans, path), exit);
    when_failed(rv = amxd_trans_del_inst(&trans, index, name), exit);
    when_failed(rv = amxd_trans_apply(&trans, amxut_bus_dm()), exit);
exit:
    amxd_trans_clean(&trans);
    assert_int_equal(rv, 0);
}

void assert_dm_check_inst(const char* path, bool exist) {
    amxd_object_t* object = amxd_dm_findf(amxut_bus_dm(), path);
    if(exist) {
        assert_non_null(object);
    } else {
        assert_null(object);
    }
}