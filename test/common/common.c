/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <setjmp.h>
#include <stdarg.h>
#include <stddef.h>
#include <cmocka.h>
#include <stdlib.h>
#include <stdio.h>


#include <amxc/amxc_macros.h>

#include <amxut/amxut_bus.h>
#include <amxut/amxut_dm.h>
#include <amxut/amxut_macros.h>

#include <debug/sahtrace.h>

#include "common.h"
#include "dm_utils.h"
#include "../../include_priv/dhcp_event.h"
#include "../../include_priv/dhcp_action.h"
#include "../../include_priv/dhcp_method.h"
#include "../../include_priv/dhcp_backup.h"
#include "../../include_priv/dm_dhcp.h"

#include "../mocks/mock_amxm.h"
#include "../mocks/mock_netmodel.h"

static void resolver_add_all_functions(amxo_parser_t* parser) {

    // events
    // server
    assert_int_equal(amxo_resolver_ftab_add(parser, "dhcp_server_enable",
                                            AMXO_FUNC(_dhcp_server_enable)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "dhcp_server_config_changed",
                                            AMXO_FUNC(_dhcp_server_config_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "dhcp_server_back_end_start",
                                            AMXO_FUNC(_dhcp_server_back_end_start)), 0);
    // pool
    assert_int_equal(amxo_resolver_ftab_add(parser, "dhcp_server_pool_added",
                                            AMXO_FUNC(_dhcp_server_pool_added)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "dhcp_server_pool_changed",
                                            AMXO_FUNC(_dhcp_server_pool_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "dhcp_server_pool_filter_changed",
                                            AMXO_FUNC(_dhcp_server_pool_filter_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "dhcp_server_pool_known_client_changed",
                                            AMXO_FUNC(_dhcp_server_pool_known_client_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "dhcp_server_pool_winsserver_changed",
                                            AMXO_FUNC(_dhcp_server_pool_winsserver_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "dhcp_server_pools_order_rearranged",
                                            AMXO_FUNC(_dhcp_server_pools_order_rearranged)), 0);
    // client
    assert_int_equal(amxo_resolver_ftab_add(parser, "dhcp_server_client_active",
                                            AMXO_FUNC(_dhcp_server_client_active)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "dhcps_client_option_hostname_active_changed",
                                            AMXO_FUNC(_dhcps_client_option_hostname_active_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "dhcp_client_check_if_static",
                                            AMXO_FUNC(_dhcp_client_check_if_static)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "dhcps_client_option_hostname_ipaddress_added",
                                            AMXO_FUNC(_dhcps_client_option_hostname_ipaddress_added)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "dhcps_client_option_hostname_ipaddress_removed",
                                            AMXO_FUNC(_dhcps_client_option_hostname_ipaddress_removed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "dhcps_client_option_hostname_ipaddress_changed",
                                            AMXO_FUNC(_dhcps_client_option_hostname_ipaddress_changed)), 0);
    // static address
    assert_int_equal(amxo_resolver_ftab_add(parser, "dhcp_server_static_address_added",
                                            AMXO_FUNC(_dhcp_server_static_address_added)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "dhcps_client_ipaddress_changed",
                                            AMXO_FUNC(_dhcps_client_ipaddress_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "dhcp_server_static_address_removed",
                                            AMXO_FUNC(_dhcp_server_static_address_removed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "dhcp_change_static",
                                            AMXO_FUNC(_dhcp_change_static)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "dhcp_server_static_address_changed",
                                            AMXO_FUNC(_dhcp_server_static_address_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "dhcps_client_option_hostname_added",
                                            AMXO_FUNC(_dhcps_client_option_hostname_added)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "dhcps_client_option_changed",
                                            AMXO_FUNC(_dhcps_client_option_changed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "dhcp_removed_static",
                                            AMXO_FUNC(_dhcp_removed_static)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "dhcp_option_add_or_remove",
                                            AMXO_FUNC(_dhcp_option_add_or_remove)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "dhcp_option_changed",
                                            AMXO_FUNC(_dhcp_option_changed)), 0);

    // action
    assert_int_equal(amxo_resolver_ftab_add(parser, "dhcp_server_pool_removed",
                                            AMXO_FUNC(_dhcp_server_pool_removed)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "dhcps_client_option_destroy",
                                            AMXO_FUNC(_dhcps_client_option_destroy)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "is_not_reserved_addr",
                                            AMXO_FUNC(_is_not_reserved_addr)), 0);

    // method
    assert_int_equal(amxo_resolver_ftab_add(parser, "setStaticClients",
                                            AMXO_FUNC(_setStaticClients)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "rmStaticClients",
                                            AMXO_FUNC(_rmStaticClients)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "cleanClientLeases",
                                            AMXO_FUNC(_cleanClientLeases)), 0);
    assert_int_equal(amxo_resolver_ftab_add(parser, "Import",
                                            AMXO_FUNC(_Import)), 0);
}

int test_setup(void** state) {
    amxo_parser_t* parser = NULL;

    mock_amxm_init();
    mock_netmodel_init();

    amxut_bus_setup(state);

    parser = amxut_bus_parser();

    resolver_add_all_functions(parser);

    amxut_dm_load_odl("../common/odl/dhcpv4-manager.odl");
    amxo_parser_invoke_entry_points(amxut_bus_parser(), amxut_bus_dm(), AMXO_START);

    amxut_dm_load_odl("../../odl/dhcpv4-manager_definition.odl");

    assert_int_equal(_dhcp_main(0, amxut_bus_dm(), parser), 0);

    amxut_dm_load_odl("../../odl/defaults.d/00_defaults_main.odl");
    amxut_dm_load_odl("../common/odl/dummy_pools_defaults.odl");

    expect_amxm_execute("mod-dhcp-dnsmasq", "start-back-end", NULL, 1);
    amxut_bus_handle_events();

    expect_amxm_execute("fw", "set_service", NULL, 1);
    expect_amxm_execute("mod-dhcp-dnsmasq", "enable-dhcp-pool", NULL, 2);
    mock_netmodel_trigger_call_backs();
    amxut_bus_handle_events();
    return 0;
}

int test_common_teardown(void** state) {
    assert_int_equal(_dhcp_main(1, amxut_bus_dm(), amxut_bus_parser()), 0);
    amxut_bus_handle_events();

    amxut_bus_teardown(state);
    mock_netmodel_teardown();
    mock_amxm_teardown();
    return 0;
}

int test_teardown(void** state) {
    // all the pool configuration will be removed ( 2 dummy pools )
    expect_amxm_execute("mod-dhcp-dnsmasq", "disable-dhcp-pool", NULL, 1);
    expect_amxm_execute("fw", "delete_service", NULL, 1);
    expect_amxm_execute("mod-dhcp-dnsmasq", "disable-dhcp-pool", NULL, 1);
    assert_dm_rm_inst("DHCPv4Server.Pool.", "dummy-1", 0);
    assert_dm_rm_inst("DHCPv4Server.Pool.", "dummy-2", 0);
    amxut_bus_handle_events();

    return test_common_teardown(state);
}

void creat_lease_arg(amxc_var_t* lease,
                     const char* pool_alias,
                     const char* mac_addr,
                     const char* ip,
                     bool add_time) {
    amxc_var_init(lease);
    amxc_var_set_type(lease, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, lease, "Alias", pool_alias);
    amxc_var_add_key(cstring_t, lease, "Chaddr", mac_addr);
    amxc_var_add_key(cstring_t, lease, "IPv4Address", ip);
    if(add_time) {
        amxc_ts_t now;
        amxc_ts_now(&now);
        amxc_var_add_key(amxc_ts_t, lease, "LeaseTimeRemaining", &now);
    }
    amxc_var_add_key(amxc_htable_t, lease, "Option", NULL);
}

void add_lease_option(amxc_var_t* option_htable, const char* tag, const char* value) {
    amxc_var_add_key(cstring_t, option_htable, tag, value);
}

void set_dhcp_lease(amxc_var_t* lease_data) {
    dm_dhcp_set_lease(NULL, lease_data, NULL);
    amxut_bus_handle_events();
    amxc_var_clean(lease_data);
}

void remove_dhcp_lease(amxc_var_t* lease_data) {
    dm_dhcp_remove_lease(NULL, lease_data, NULL);
    amxut_bus_handle_events();
    amxc_var_clean(lease_data);
}

void add_static_ip(const char* path, const char* ip, const char* chaddr, int ret_apply) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);

    amxd_trans_select_pathf(&trans, path);
    amxd_trans_add_inst(&trans, 0, NULL);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(cstring_t, &trans, "Yiaddr", ip);
    amxd_trans_set_value(cstring_t, &trans, "Chaddr", chaddr);

    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), ret_apply);
    amxd_trans_clean(&trans);
}

void add_pool_option(const char* path, int tag, const char* value, bool force) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    amxd_trans_select_pathf(&trans, path);
    amxd_trans_add_inst(&trans, 0, NULL);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(uint32_t, &trans, "Tag", tag);
    amxd_trans_set_value(cstring_t, &trans, "Value", value);
    amxd_trans_set_value(bool, &trans, "Force", force);

    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxd_trans_clean(&trans);
}