/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include <amxc/amxc_macros.h>

#include "mock_amxm.h"

// internal variable
static amxc_var_t* mock_amxm_expect = NULL;

// Function
// helper
static supported_modules_t get_supported_module(const char* const shared_object_name,
                                                const char* const module_name) {
    if((strcmp(shared_object_name, "mod-fw-amx") == 0) && (strcmp(module_name, "fw") == 0)) {
        return FW_MODULE;
    }
    if((strcmp(shared_object_name, "mod-dhcp-uci") == 0) && (strcmp(module_name, "mod-dhcp-uci") == 0)) {
        return UCI_MODULE;
    }
    if((strcmp(shared_object_name, "mod-dhcp-dnsmasq") == 0) && (strcmp(module_name, "mod-dhcp-dnsmasq") == 0)) {
        return UCI_MODULE;
    }
    return NB_MODULE;
}

static amxc_var_t* get_or_create_var_key(amxc_var_t* var, const char* key, amxc_var_type_id_t type) {
    assert_non_null(var);
    amxc_var_t* value = GET_ARG(var, key);
    if(value == NULL) {
        value = amxc_var_add_new_key(var, key);
        assert_non_null(value);
        amxc_var_set_type(value, type);
    }
    return value;
}

static amxc_var_t* get_or_create_expect_module_function_var(const char* const module_name,
                                                            const char* const func_name) {
    assert_non_null(mock_amxm_expect);
    amxc_var_t* module_var = get_or_create_var_key(mock_amxm_expect, module_name, AMXC_VAR_ID_HTABLE);
    return get_or_create_var_key(module_var, func_name, AMXC_VAR_ID_LIST);
}

// wrapper
int __wrap_amxm_execute_function(const char* const shared_object_name,
                                 const char* const module_name,
                                 const char* const func_name,
                                 amxc_var_t* args,
                                 UNUSED amxc_var_t* ret) {
    supported_modules_t mod = NB_MODULE;
    amxc_var_t* expected_args = NULL;
    amxc_var_t* module_func_var = NULL;

    assert_non_null(shared_object_name);
    assert_non_null(module_name);
    assert_non_null(func_name);
    assert_non_null(args);

    check_expected(func_name);

    mod = get_supported_module(shared_object_name, module_name);
    assert_int_not_equal(mod, NB_MODULE);

    module_func_var = GET_ARG(GET_ARG(mock_amxm_expect, module_name), func_name);
    assert_non_null(module_func_var);
    expected_args = amxc_var_take_index(module_func_var, 0);
    assert_non_null(expected_args);
    when_false(amxc_var_type_of(expected_args) == AMXC_VAR_ID_HTABLE, exit);

    amxc_var_for_each(elem, expected_args) {
        const char* key = amxc_var_key(elem);
        int result = -1;
        if((amxc_var_compare(elem, GET_ARG(args, key), &result) != 0) || (result != 0)) {
            print_error("function \"%s\" args don't match expected\n", func_name);
            amxc_var_dump(expected_args, STDERR_FILENO);
            amxc_var_dump(args, STDERR_FILENO);
            fail();
        }
    }
exit:
    if(expected_args != NULL) {
        amxc_var_delete(&expected_args);
    }
    return 0;
}

int __wrap_amxm_so_open(UNUSED amxm_shared_object_t** so,
                        UNUSED const char* shared_object_name,
                        UNUSED const char* const path_to_so) {
    return 0;
}

void mock_amxm_expect_execute(const char* const module_name,
                              const char* const func_name,
                              amxc_var_t* args, size_t count) {
    amxc_var_t* func_var = get_or_create_expect_module_function_var(module_name, func_name);

    for(size_t i = 0; i < count; i++) {
        amxc_var_t* expect_var = amxc_var_add_new(func_var);
        if(args != NULL) {
            amxc_var_copy(expect_var, args);
        } else {
            amxc_var_set_type(expect_var, AMXC_VAR_ID_HTABLE);
        }
    }
}

void mock_amxm_init(void) {
    assert_null(mock_amxm_expect);
    assert_int_equal(amxc_var_new(&mock_amxm_expect), 0);
    amxc_var_set_type(mock_amxm_expect, AMXC_VAR_ID_HTABLE);
}

void mock_amxm_teardown(void) {
    assert_non_null(mock_amxm_expect);
    amxc_var_delete(&mock_amxm_expect);
    assert_null(mock_amxm_expect);
}