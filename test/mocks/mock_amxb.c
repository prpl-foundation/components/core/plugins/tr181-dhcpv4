/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include <amxc/amxc_macros.h>

#include "mock_amxb.h"

// internal variable
static bool check_amxb_call = false;
static amxb_bus_ctx_t bus = {0};
static uint32_t dns_host_index = 0;

static void add_new_dns_host_alias(amxc_var_t* ret) {
    amxc_string_t alias;
    amxc_string_init(&alias, 0);
    amxc_string_setf(&alias, "cpe-X_PRPL-COM_Host-%d", ++dns_host_index);
    amxc_var_add(cstring_t, ret, amxc_string_get(&alias, 0));
    amxc_string_clean(&alias);
}

/*
 * @brief
 * cmocka's expect_string can't gracefully handle NULL references
 *
 * @param str: nullable string
 * @return str or "(NULL)" if str is NULL
 */
static const cstring_t safe_test_str(const cstring_t str) {
    return str ? str : "(NULL)";
}

static void amxb_call_dns_set_host(amxc_var_t* args,
                                   amxc_var_t* ret) {
    if(check_amxb_call) {
        const char* alias = safe_test_str(GET_CHAR(args, "Alias"));
        const char* ip_address = safe_test_str(GET_CHAR(args, "IPAddresses"));
        const char* interface = safe_test_str(GET_CHAR(args, "Interface"));
        const char* name = safe_test_str(GET_CHAR(args, "Name"));
        const char* origin = safe_test_str(GET_CHAR(args, "Origin"));
        bool has_enable = GET_ARG(args, "Enable") != NULL;
        bool enable = GET_BOOL(args, "Enable");
        bool has_exclusive = GET_ARG(args, "Exclusive") != NULL;
        bool exclusive = GET_BOOL(args, "Exclusive");

        function_called();
        check_expected(alias);
        check_expected(ip_address);
        check_expected(interface);
        check_expected(name);
        check_expected(origin);
        check_expected(has_enable);
        check_expected(enable);
        check_expected(has_exclusive);
        check_expected(exclusive);
    }

    amxc_var_set_type(ret, AMXC_VAR_ID_LIST);
    if(GET_CHAR(args, "Alias")) {
        amxc_var_add(cstring_t, ret, GET_CHAR(args, "Alias"));
    } else {
        add_new_dns_host_alias(ret);
    }
}

void expect_amxb_call_dns_set_host(const amxb_call_dns_set_host_args_t* host_args) {
    const cstring_t alias = host_args->alias;
    const cstring_t ip_address = host_args->ip_address;
    const cstring_t interface = host_args->interface;
    const cstring_t name = host_args->name;
    const cstring_t origin = host_args->origin;
    bool has_enable = host_args->enable != NULL;
    bool enable = host_args->enable != NULL ? *host_args->enable : false;
    bool has_exclusive = host_args->exclusive != NULL;
    bool exclusive = host_args->exclusive != NULL ? *host_args->exclusive : false;

    check_amxb_call = true;
    expect_function_call(amxb_call_dns_set_host);
    expect_string(amxb_call_dns_set_host, alias, safe_test_str(alias));
    expect_string(amxb_call_dns_set_host, ip_address, safe_test_str(ip_address));
    expect_string(amxb_call_dns_set_host, interface, safe_test_str(interface));
    expect_string(amxb_call_dns_set_host, name, safe_test_str(name));
    expect_string(amxb_call_dns_set_host, origin, safe_test_str(origin));
    expect_value(amxb_call_dns_set_host, has_enable, has_enable);
    expect_value(amxb_call_dns_set_host, enable, enable);
    expect_value(amxb_call_dns_set_host, has_exclusive, has_exclusive);
    expect_value(amxb_call_dns_set_host, exclusive, exclusive);
}

static void amxb_call_dns_remove_host(amxc_var_t* args,
                                      UNUSED amxc_var_t* ret) {
    if(check_amxb_call) {
        const char* alias = safe_test_str(GET_CHAR(args, "Alias"));
        function_called();
        check_expected(alias);
    }
}

void expect_amxb_call_dns_remove_host(const cstring_t alias) {
    check_amxb_call = true;
    expect_function_call(amxb_call_dns_remove_host);
    expect_string(amxb_call_dns_remove_host, alias, safe_test_str(alias));
}

static void amxb_call_dns_remove_host_ip(amxc_var_t* args,
                                         UNUSED amxc_var_t* ret) {
    if(check_amxb_call) {
        const char* alias = safe_test_str(GET_CHAR(args, "Alias"));
        const char* ip_address = safe_test_str(GET_CHAR(args, "IPAddresses"));

        function_called();
        check_expected(alias);
        check_expected(ip_address);
    }
}

void expect_amxb_call_dns_rm_host_ip(const amxb_call_dns_set_host_args_t* host_args) {
    const cstring_t alias = host_args->alias;
    const cstring_t ip_address = host_args->ip_address;

    check_amxb_call = true;
    expect_function_call(amxb_call_dns_remove_host_ip);
    expect_string(amxb_call_dns_remove_host_ip, alias, safe_test_str(alias));
    expect_string(amxb_call_dns_remove_host_ip, ip_address, safe_test_str(ip_address));
}

static void amxb_call_dns(const char* const func_name,
                          amxc_var_t* args,
                          amxc_var_t* ret) {

    if(strcmp(func_name, "SetHost") == 0) {
        amxb_call_dns_set_host(args, ret);
    } else if(strcmp(func_name, "RemoveIPFromHost") == 0) {
        amxb_call_dns_remove_host_ip(args, ret);
    } else if(strcmp(func_name, "RemoveHost") == 0) {
        amxb_call_dns_remove_host(args, ret);
    } else {
        fail_msg("Call to unexpected function DNS.%s", func_name);
    }
}

// Function
// wrapper
int __wrap_amxb_call(amxb_bus_ctx_t* bus,
                     const char* const path,
                     const char* const func_name,
                     amxc_var_t* args,
                     amxc_var_t* ret) {
    assert_non_null(bus);
    assert_non_null(path);
    assert_non_null(func_name);
    assert_non_null(args);
    assert_non_null(ret);

    if(strcmp(path, "DNS.") == 0) {
        amxb_call_dns(func_name, args, ret);
    } else if(check_amxb_call) {
        check_expected(path);
        check_expected(func_name);
    }
    return AMXB_STATUS_OK;
}

amxb_bus_ctx_t* __wrap_amxb_be_who_has(const char* path) {
    assert_non_null(path);
    return &bus;
}


// mock functions
void mock_amxb_call_check(const char* path, const char* func_name) {
    check_amxb_call = true;
    expect_string_count(__wrap_amxb_call, path, path, 1);
    expect_string_count(__wrap_amxb_call, func_name, func_name, 1);
}

void mock_amxb_call_check_enable(void) {
    check_amxb_call = true;
}

void mock_amxb_call_check_disable(void) {
    check_amxb_call = false;
}
