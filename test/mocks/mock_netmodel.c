/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>
#include <amxc/amxc_lqueue.h>
#include <amxc/amxc_macros.h>

#include "mock_netmodel.h"

typedef struct _handler_obj_t {
    amxp_slot_fn_t handler;
    amxc_var_t* data;
    amxc_var_t* user_data;
    netmodel_query_t* query;
    struct _handler_obj_t* next;
} handler_obj_t;

struct _netmodel_query {
    amxp_slot_fn_t handler;
    amxc_var_t data;
    void* priv;
    bool handled;
    amxc_llist_it_t it;
};

// private datas
static amxc_llist_t* mock_netmodel_queries = NULL;

// helper function
static netmodel_query_t* mock_netmodel_query_new(amxp_slot_fn_t handler, void* priv) {
    netmodel_query_t* q = calloc(sizeof(netmodel_query_t), 1);
    q->handler = handler;
    q->priv = priv;
    amxc_var_init(&q->data);

    amxc_llist_append(mock_netmodel_queries, &q->it);
    return q;
}

static void mock_netmodel_query_delete(netmodel_query_t* q) {
    amxc_llist_it_take(&q->it);
    amxc_var_clean(&q->data);
    free(q);
}

// wrapper functions
netmodel_query_t* __wrap_netmodel_openQuery_getAddrs(const char* intf,
                                                     const char* subscriber,
                                                     UNUSED const char* flag,
                                                     const char* traverse,
                                                     amxp_slot_fn_t handler,
                                                     void* userdata) {
    netmodel_query_t* q = NULL;

    assert_non_null(traverse);
    assert_non_null(handler);
    assert_string_equal(subscriber, "DHCPv4s");

    when_false(strncmp(intf, "Device.IP.Interface.", 20) == 0, exit);

    q = mock_netmodel_query_new(handler, userdata);
    amxc_var_set_type(&q->data, AMXC_VAR_ID_LIST);
    amxc_var_add(cstring_t, &q->data, "192.168.1.1");
exit:
    return q;
}

netmodel_query_t* __wrap_netmodel_openQuery_getFirstParameter(const char* intf,
                                                              const char* subscriber,
                                                              const char* param_name,
                                                              UNUSED const char* flag,
                                                              const char* traverse,
                                                              amxp_slot_fn_t handler,
                                                              void* userdata) {
    netmodel_query_t* q = NULL;

    assert_non_null(traverse);
    assert_non_null(handler);
    assert_non_null(param_name);
    assert_string_equal(subscriber, "DHCPv4s");

    when_false(strncmp(intf, "Device.IP.Interface.", 20) == 0, exit);

    q = mock_netmodel_query_new(handler, userdata);
    amxc_var_set(cstring_t, &q->data, "dummyinterface");
exit:
    return q;
}

bool __wrap_netmodel_initialize(void) {
    return true;
}

void __wrap_netmodel_closeQuery(netmodel_query_t* query) {
    mock_netmodel_query_delete(query);
}

amxc_var_t* __wrap_netmodel_getAddrs(const char* path, const char* flag, const char* traverse) {
    amxc_var_t* list = NULL;
    amxc_var_t* data = NULL;
    when_false(strncmp(path, "Device.IP.Interface.", 20) == 0, exit);
    assert_non_null(flag);
    assert_non_null(traverse);

    amxc_var_new(&list);
    amxc_var_set_type(list, AMXC_VAR_ID_LIST);
    data = amxc_var_add(amxc_htable_t, list, NULL);
    amxc_var_add_key(cstring_t, data, "Address", "192.168.1.1");
exit:
    return list;
}

// mock functions

void mock_netmodel_init(void) {
    assert_null(mock_netmodel_queries);
    assert_int_equal(amxc_llist_new(&mock_netmodel_queries), 0);
}

void mock_netmodel_teardown(void) {
    assert_non_null(mock_netmodel_queries);
    amxc_llist_delete(&mock_netmodel_queries, NULL);
}

void mock_netmodel_trigger_call_backs(void) {
    printf("Call back trigger: ");
    assert_non_null(mock_netmodel_queries);
    amxc_llist_for_each(it, mock_netmodel_queries) {
        netmodel_query_t* query = amxc_llist_it_get_data(it, netmodel_query_t, it);
        if(!query->handled) {
            printf(".");
            query->handler(NULL, &query->data, query->priv);
            query->handled = true;
        }
    }
    printf("\n");
}